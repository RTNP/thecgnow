package com.newsapp.app.thecgnow2.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.newsapp.app.thecgnow2.R;
import com.github.pavlospt.CircleView;

import java.util.ArrayList;

public class CustomGridAdapter extends BaseAdapter {

    ArrayList<Integer> numbers;
    Context mCtx;
    private static LayoutInflater inflater=null;
   /* private CGNowDB mDB;
    private TheCGNowApp publicAds;*/

    public CustomGridAdapter(ArrayList<Integer> numbers, Context mCtx) {
        this.numbers = numbers;
        this.mCtx = mCtx;

        /*publicAds=(TheCGNowApp)mCtx.getApplicationContext();
        publicAds.initDB(mCtx.getApplicationContext());
        mDB=publicAds.getDB(mCtx.getApplicationContext());*/

        /*inflater = ( LayoutInflater )mCtx.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);*/
    }

    @Override
    public int getCount() {
        return numbers.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    public class Holder {
        CircleView circleView;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        Holder holder=new Holder();

        /*View rowView;
        rowView = inflater.inflate(R.layout.bubble_grid_item, null);*/
        View itemView = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.bubble_grid_item, viewGroup, false);

        holder.circleView = itemView.findViewById(R.id.bubble_item);
        holder.circleView.setTitleText(numbers.get(i).toString());

        return itemView;
    }


}
