package com.newsapp.app.thecgnow2.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.newsapp.app.thecgnow2.R;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.newsapp.app.thecgnow2.TheCGNowApp;
import com.newsapp.app.thecgnow2.model.NewsCategory;
import com.newsapp.app.thecgnow2.roomDB.CGNowDB;
import com.newsapp.app.thecgnow2.roomDB.category.CategoryRoom;

import java.util.ArrayList;

public class AddRegionAdapter extends RecyclerView.Adapter {
    Context mCtx;
    ArrayList<NewsCategory> regions;
    CGNowDB mDB;
    TheCGNowApp cgNowApp;
    CategoryRoom data;
    String TAG="AddRegionAdapter";

    public AddRegionAdapter(ArrayList<NewsCategory> regions, Context mCtx) {
        this.regions = regions;
        this.mCtx = mCtx;

        cgNowApp=(TheCGNowApp)mCtx.getApplicationContext();
        cgNowApp.initDB(mCtx);
        mDB=cgNowApp.getDB(mCtx);

        data=new CategoryRoom();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView;

        itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.region_list_item, parent, false);

        return new viewHolderOne(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        final viewHolderOne vHolder = (viewHolderOne) holder;
        final NewsCategory region = regions.get(position);

        ((viewHolderOne)holder).setViews(region);
    }

    @Override
    public int getItemCount() {

        if (regions.size()>22)
        Log.d(TAG, "itemsize: "+regions.size());
        return regions.size();
    }

    public class viewHolderOne extends RecyclerView.ViewHolder {
        ImageView addCategory;
        TextView regionName;

        public viewHolderOne(@NonNull View itemView) {
            super(itemView);
            regionName=itemView.findViewById(R.id.region_name);
            addCategory=itemView.findViewById(R.id.add);
        }

        public void setViews(final NewsCategory region) {

            regionName.setText(region.toString());

            if (mDB.categoryDao().getCategoryWithCatId(region.getId())!=null) {
                Log.d(TAG, "catid: "+region.getId()+" region name: "+mDB.categoryDao().getCategoryWithCatId(region.getId()).getCatName());
                addCategory.setTag(1);
                addCategory.setImageResource(R.drawable.ic_add_active_icon);
//                mDB.categoryDao().removeCategory(data.getId());
                Log.d(TAG, "catname: "+regionName.getText().toString());
            } else {
                addCategory.setTag(0);
                addCategory.setImageResource(R.drawable.ic_add_icon);
                Log.d(TAG, "elsecat: "+region.getId()+" region name: "+region.getName()+" tag: "+addCategory.getTag());
            }

            addCategory.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    data.setCatId(region.getId());
                    data.setCatName(region.getName());
                    data.setCatKeyName(region.getKeyName());
                    data.setCatOrder(region.getCatOrder());

                    Log.d(TAG, "regionId:"+region.getId()+" dataId:"+data.getCatId());

                    if (Integer.parseInt(view.getTag().toString())==0) {
                        view.setTag(1);
                        mDB.categoryDao().addCategory(data);
                        addCategory.setImageResource(R.drawable.ic_add_active_icon);
//                        Toast.makeText(mCtx, "region added", Toast.LENGTH_SHORT).show();
                        Log.d(TAG, "catidadd: "+mDB.categoryDao().getCategoryWithCatId(region.getId()));
                    } else {
                        Log.d(TAG, "catidrem: "+mDB.categoryDao().getCategoryWithCatId(region.getId()));
                        view.setTag(0);
                        mDB.categoryDao().removeCategory(region.getId());
                        addCategory.setImageResource(R.drawable.ic_add_icon);
//                        Toast.makeText(mCtx, "region removed", Toast.LENGTH_SHORT).show();

                    }
                }
            });
        }
    }
}
