package com.newsapp.app.thecgnow2.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.newsapp.app.thecgnow2.Config;
import com.newsapp.app.thecgnow2.R;
import com.newsapp.app.thecgnow2.SharedPreferenceConfig;
import com.newsapp.app.thecgnow2.model.NewsCategory;
import com.newsapp.app.thecgnow2.model.OfferCategory;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class SelectLocationActivity extends AppCompatActivity implements View.OnClickListener {

    Spinner spinner;
    Button submit;
    int spinnerPos;
    ArrayAdapter<String> adapter;
    ArrayList<NewsCategory> regions=new ArrayList<>();
    String TAG = "SelectLocationActivity";
    ImageView retry;
    ProgressBar progressBar;
    RequestQueue requestQueue;
    SharedPreferenceConfig sharedPreferenceConfig;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_location);

        initView();
        submit.setOnClickListener(this);
        requestQueue = Volley.newRequestQueue(this);
        sharedPreferenceConfig = new SharedPreferenceConfig(this);
        retry.setOnClickListener(this);
        getLocations();
    }

    private void setSpinnerAdapter() {
        adapter = new ArrayAdapter(this,android.R.layout.simple_list_item_1,regions);
        spinner.setAdapter(adapter);
    }

    private void initView() {
        spinner = findViewById(R.id.spinner_location);
        submit = findViewById(R.id.submit_btn);
        retry = findViewById(R.id.retry);
        progressBar = findViewById(R.id.circular_progress);
    }

    private void getLocations() {
//        offerCategories.add(new OfferCategory(0, getResources().getString(R.string.select_offer_category), "select Category"));
        String get_query = "?auth_key="+sharedPreferenceConfig.readFcmToken();
        StringRequest stringRequest = new StringRequest(Request.Method.GET, Config.GET_REGIONS_URL+get_query,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        regions.clear();
                        progressBar.setVisibility(View.GONE);

                        Log.d(TAG,"regions: "+response);
//                        Toast.makeText(AttendenceActivity.this, "attendance response"+response, Toast.LENGTH_SHORT).show();
                        try {
                            regions.add(new NewsCategory(0, getResources().getString(R.string.select_offer_category), "Select Category"));
                            //Creating the json object from the response
                            JSONObject jsonResponse = new JSONObject(response);
                            JSONArray jsonArray = jsonResponse.getJSONArray("data");
                            Log.d(TAG,jsonArray.toString());
//                            Toast.makeText(AttendenceActivity.this, ""+jsonArray.toString(), Toast.LENGTH_SHORT).show();

                            for (int i=0; i<jsonArray.length(); i++) {
                                JSONObject regionJson = jsonArray.getJSONObject(i);
                                regions.add(new NewsCategory(regionJson.getInt("id"),
                                        regionJson.getString("region_name_hi"),
                                        regionJson.getString("region_name")));
                            }

                            setSpinnerAdapter();

//                            Toast.makeText(mCtx, "pos:"+pos, Toast.LENGTH_SHORT).show();
                           /* Log.d(TAG, "select selection: "+pos);
                            selectRegion.setSelection(pos);
                            Log.d(TAG, "select selection2: "+pos);*/
//                            Toast.makeText(mCtx, "pos:"+pos, Toast.LENGTH_SHORT).show();
//                            jsonResponse.getString(Config.TAG_RESPONSE).equalsIgnoreCase("Success");

                        } catch (JSONException e) {
                            e.printStackTrace();
                            retry.setVisibility(View.VISIBLE);
                            Log.d("mobileerror",""+e);
//                            Toast.makeText(getActivity(), ""+e.toString(), Toast.LENGTH_SHORT).show();
                        } catch (IndexOutOfBoundsException e) {
                            e.printStackTrace();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        retry.setVisibility(View.VISIBLE);
                        progressBar.setVisibility(View.GONE);
                        Toast.makeText(SelectLocationActivity.this, getResources().getString(R.string.couldnot_connect),Toast.LENGTH_LONG).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                //Adding the parameters to the request
                params.put("auth_key", sharedPreferenceConfig.readFcmToken());
                return params;
            }
        };

        //Adding request the the queue
        requestQueue.add(stringRequest);
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == submit.getId()) {

            spinnerPos = spinner.getSelectedItemPosition();
            if (spinnerPos>0) {
                sharedPreferenceConfig.setLocationSelected(true);
                sharedPreferenceConfig.writeLocationId(regions.get(spinnerPos).getId());
                sharedPreferenceConfig.writeLocationIsActive(1==regions.get(spinnerPos).getActive());
                Log.d(TAG, "selected item: " + spinner.getSelectedItem() + " item position: " + spinner.getSelectedItemPosition());
                sharedPreferenceConfig.writeLocationName(regions.get(spinnerPos).getKeyName());
                sharedPreferenceConfig.writeLocationNameHi(regions.get(spinnerPos).getName());
                Log.d(TAG, "lockeyname="+regions.get(spinnerPos).getKeyName()+" locnamehi="+regions.get(spinnerPos).getName());
                startActivity(new Intent(SelectLocationActivity.this, MainActivity.class));
                finish();
            } else {
                Toast.makeText(this, "please select one of the locations to continue...", Toast.LENGTH_SHORT).show();
            }
        } else if (view.getId() == retry.getId()) {
            retry.setVisibility(View.GONE);
            progressBar.setVisibility(View.VISIBLE);
            getLocations();
        }
    }
}
