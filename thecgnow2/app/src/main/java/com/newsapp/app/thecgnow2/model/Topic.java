package com.newsapp.app.thecgnow2.model;

public class Topic {
    int id;
    String srcName;

    public Topic(int id, String srcName) {
        this.id = id;
        this.srcName = srcName;

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSrcName() {
        return srcName;
    }

    public void setSrcName(String srcName) {
        this.srcName = srcName;
    }
}
