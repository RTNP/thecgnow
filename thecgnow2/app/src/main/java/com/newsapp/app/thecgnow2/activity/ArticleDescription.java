package com.newsapp.app.thecgnow2.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.animation.ObjectAnimator;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.text.Html;
import android.text.Spannable;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.newsapp.app.thecgnow2.Config;

import com.newsapp.app.thecgnow2.PicassoImageGetter;
import com.newsapp.app.thecgnow2.R;

import com.newsapp.app.thecgnow2.SharedPreferenceConfig;
import com.newsapp.app.thecgnow2.TheCGNowApp;
import com.newsapp.app.thecgnow2.adapter.NotificationAdapter;
import com.newsapp.app.thecgnow2.model.Advertize;
import com.newsapp.app.thecgnow2.model.Article;
import com.newsapp.app.thecgnow2.model.Notification;
import com.newsapp.app.thecgnow2.roomDB.CGNowDB;
import com.newsapp.app.thecgnow2.roomDB.like.LikeRoom;
import com.google.android.exoplayer2.ExoPlayer;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory;
import com.google.android.exoplayer2.extractor.ExtractorsFactory;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelector;
import com.google.android.exoplayer2.ui.SimpleExoPlayerView;
import com.google.android.exoplayer2.upstream.BandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultHttpDataSourceFactory;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.gson.Gson;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class ArticleDescription extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = "ArticleDescription";
    Article article;
    Advertize bannerAd, labelAd;
    TextView title, description, content, articleTitle;
    ImageView like, waShare, backDrop;
    CollapsingToolbarLayout toolbarLayout;
    AppBarLayout appBarLayout;
    ImageView backArrow, ad_label, ad_banner;
    LinearLayout bottomll;
    CGNowDB mDB;
    TheCGNowApp publicAds;
    RequestQueue requestQueue;
    SharedPreferenceConfig sharedPreferenceConfig;
    CardView imageCard, videoCard;

    BroadcastReceiver broadcastReceiver;
    SimpleExoPlayerView simpleExoPlayerView;
    SimpleExoPlayer simpleExoPlayer;

    private ArrayList<Article> notifications = new ArrayList<>();
    Advertize banner_advertize, label_advertize;
    private RecyclerView recyclerView;
    private NotificationAdapter mAdapter;

    NestedScrollView nestedScrollView;

    PicassoImageGetter imageGetter;
    Spannable html;
    WebView webView;
    int categoryId=0;

//    ImageView articleImg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_article_description);

        Toolbar toolbar = findViewById(R.id.toolbar);
        backArrow=toolbar.findViewById(R.id.back_arrow);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);

//        TextView toolTitle=findViewById(R.id.main_tool_title);
//        toolTitle.setText(R.string.app_title);

        initView();

        initDB();
        requestQueue= Volley.newRequestQueue(this);
        sharedPreferenceConfig = new SharedPreferenceConfig(this);

        imageGetter = new PicassoImageGetter(content, this);
        getIntentData();
        getNotificationIntentData();
//        getUriIntentData();
        registerBroadcastReceiver();
        setUpRecyclerView();
        getAdvertises();

        /*try {
            Picasso.with(this).load(publicAds.getLabelAdvertizes().get(publicAds.labelAdItemPosition).getMediaUrl()).into(ad_label);
            Picasso.with(this).load(publicAds.getAdvertizes().get(publicAds.adBannerItemPosition).getMediaUrl()).into(ad_banner);
        } catch (Exception e) {
            e.printStackTrace();
        }*/
        refreshAds();
//        setClickEventForAds();
        /*mDB = Room.databaseBuilder(this, CGNowDB.class,  getResources().getString(R.string.db_string))
                .fallbackToDestructiveMigration()
                .allowMainThreadQueries().build();*/

//        like.setTag(0);  // default icon

        setLikeUI();
        like.setOnClickListener(this);
        waShare.setOnClickListener(this);
        backArrow.setOnClickListener(this);
        ad_banner.setOnClickListener(this);
        ad_label.setOnClickListener(this);

    }

    private void getAdvertises() {
        String get_query = "?latitude="+sharedPreferenceConfig.readLatitude()+"&longitude="+sharedPreferenceConfig.readLongitude()+"&ad_page="+sharedPreferenceConfig.readAdPage()+"&auth_key="+sharedPreferenceConfig.readFcmToken();
        Log.d(TAG, "urlquery="+Config.GET_ARTICLE_DESC_ADS+get_query);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, Config.GET_ARTICLE_DESC_ADS+get_query,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

//                        progressBar.setVisibility(View.GONE);

                        Log.d(TAG,"res="+response);
//                        Toast.makeText(AttendenceActivity.this, "attendance response"+response, Toast.LENGTH_SHORT).show();
                        try {
                            //Creating the json object from the response
                            JSONObject jsonResponse = new JSONObject(response);
                            JSONArray jsonArray = jsonResponse.getJSONArray("bannerdata");
                            JSONArray jsonArray2 = jsonResponse.getJSONArray("labeldata");
                            Log.d(TAG,"data:"+jsonArray.toString());
//                            Toast.makeText(AttendenceActivity.this, ""+jsonArray.toString(), Toast.LENGTH_SHORT).show();

                            for (int i=0; i<jsonArray.length(); i++) {
                                JSONObject jsonObject =jsonArray.getJSONObject(i);
                                bannerAd = new Advertize(jsonObject.getInt("advertise_id"),
                                        jsonObject.getInt("cat_id"),
                                        jsonObject.getInt("region_id"),
                                        jsonObject.getString("title"),
                                        Config.BASE_URL+jsonObject.getString("media_url"),
                                        jsonObject.getString("media_type"),
                                        jsonObject.getString("date"),
                                        jsonObject.getInt("ad_time"),
                                        jsonObject.getString("ad_url"));
                            }

                            for (int i=0; i<jsonArray2.length(); i++) {
                                JSONObject jsonObject =jsonArray2.getJSONObject(i);
                                labelAd = new Advertize(jsonObject.getInt("advertise_id"),
                                        jsonObject.getInt("cat_id"),
                                        jsonObject.getInt("region_id"),
                                        jsonObject.getString("title"),
                                        Config.BASE_URL+jsonObject.getString("media_url"),
                                        jsonObject.getString("media_type"),
                                        jsonObject.getString("date"),
                                        jsonObject.getInt("ad_time"),
                                        jsonObject.getString("ad_url"));
                            }

                            setAdvertiseUI();


                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.d(TAG,"mobileerror: "+e);
//                            Toast.makeText(getActivity(), ""+e.toString(), Toast.LENGTH_SHORT).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {


//                        loading.dismiss();
//                        progressBar.setVisibility(View.GONE);
                        Log.d(TAG, error.getMessage()+" tostr:"+error.toString());
                        Toast.makeText(ArticleDescription.this, getResources().getString(R.string.couldnot_connect),Toast.LENGTH_LONG).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                //Adding the parameters to the request
//                params.put("id", id);
                params.put("auth_key", sharedPreferenceConfig.readFcmToken());
                return params;
            }


        };

        //Adding request the the queue
        requestQueue.add(stringRequest);
    }

    private void setAdvertiseUI() {
        Picasso.get().load(bannerAd.getMediaUrl()).placeholder(R.drawable.placeholder).into(ad_banner);
        Picasso.get().load(labelAd.getMediaUrl()).placeholder(R.drawable.labelph).into(ad_label);
    }

    /*private void setClickEventForAds() {
        ad_label.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String url = publicAds.getLabelAdvertizes().get(publicAds.labelAdItemPosition).getMediaUrl();
                Intent viewIntent =
                        new Intent("android.intent.action.VIEW",
                                Uri.parse(url));
                startActivity(viewIntent);
            }
        });

        ad_banner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String url = publicAds.getAdvertizes().get(publicAds.adBannerItemPosition).getMediaUrl();
                Intent viewIntent =
                        new Intent("android.intent.action.VIEW",
                                Uri.parse(url));
                startActivity(viewIntent);
            }
        });

        simpleExoPlayerView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String url = publicAds.getAdvertizes().get(publicAds.adBannerItemPosition).getMediaUrl();
                Intent viewIntent =
                        new Intent("android.intent.action.VIEW",
                                Uri.parse(url));
                startActivity(viewIntent);
            }
        });
    }*/

    private void fetchFooterStoryData() {
       /* String url = "http://thecgnow.com/upload/1574173290_airforce.jpg";
        notifications.add(new Notification("44","This is test story", url));
        notifications.add(new Notification("44","This is test story", url));
        notifications.add(new Notification("44","This is test story", url));
        notifications.add(new Notification("44","This is test story", url));*/


//        final ProgressDialog loading = ProgressDialog.show(this, "Fetching data", "Please wait...", false, false);
        String get_query = "?category="+categoryId+"&auth_key="+sharedPreferenceConfig.readFcmToken();
        StringRequest stringRequest = new StringRequest(Request.Method.GET, Config.GET_TOP_STORIES_URL+get_query,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

//                        loading.dismiss();
//                        progressBar.setVisibility(View.GONE);

                        Log.d(TAG,"footerdata"+response);
//                        Toast.makeText(AttendenceActivity.this, "attendance response"+response, Toast.LENGTH_SHORT).show();
                        try {
                            //Creating the json object from the response
                            JSONObject jsonResponse = new JSONObject(response);
                            JSONArray jsonArray = jsonResponse.getJSONArray("data");
                            Log.d(TAG, jsonArray.toString());
//                            Toast.makeText(AttendenceActivity.this, ""+jsonArray.toString(), Toast.LENGTH_SHORT).show();

                            for (int i=0; i<jsonArray.length(); i++) {
                                JSONObject articleJson =jsonArray.getJSONObject(i);

                                notifications.add(new Article(articleJson.getString("id"),
                                        articleJson.getString("title"),
                                        articleJson.getString("urlToImage"),
                                        "",
                                        articleJson.getString("name"),
                                        "img",
                                        articleJson.getString("description"),
                                        articleJson.getString("content"),
                                        articleJson.getString("url"),
                                        articleJson.getString("publishedAt"),
                                        Config.ARTICLE_ITEM_IMAGE,
                                        articleJson.getInt("category_id")));
                            }

//                            setUI(article);
                            recyclerView.setAdapter(mAdapter);


                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.d("mobileerror",""+e);
//                            Toast.makeText(getActivity(), ""+e.toString(), Toast.LENGTH_SHORT).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {


//                        loading.dismiss();
//                        progressBar.setVisibility(View.GONE);
                        Toast.makeText(ArticleDescription.this, getResources().getString(R.string.couldnot_connect),Toast.LENGTH_LONG).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                //Adding the parameters to the request
                params.put("auth_key", sharedPreferenceConfig.readFcmToken());
                return params;
            }


        };

        //Adding request the the queue
        requestQueue.add(stringRequest);
    }

    private void setUpRecyclerView() {
        mAdapter = new NotificationAdapter(notifications,this);
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(mLayoutManager);
    }

    private void getNotificationIntentData() {
        try {
            String title = getIntent().getExtras().getString("title");
            String id = getIntent().getExtras().getString("id");
            Log.d(TAG, "noti title:"+title);
            if (title!=null)
                fetchNotificationData(id, title);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void initDB() {
        publicAds=(TheCGNowApp)getApplicationContext();
        publicAds.initDB(getApplicationContext());
        mDB=publicAds.getDB(getApplicationContext());
    }

    private void registerBroadcastReceiver() {
        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
//                refreshAds();
            }
        };
        registerReceiver(broadcastReceiver, new IntentFilter("com.example.thecgnow.activity.adRefresh"));
    }

    private void refreshAds() {
        updateBannerAd();
        updateLabelAd();
            /*try {
                int labelAdPos = publicAds.labelAdItemPosition;
                String labelAdType = publicAds.getLabelAdvertizes().get(labelAdPos).getMediaType();
                if (labelAdType.equals("LIMG")) {
                    Picasso.with(this).load(publicAds.getLabelAdvertizes().get(labelAdPos).getMediaUrl()).into(ad_label);
                } else if (labelAdType.equals("LGIF")) {
                    Glide.with(this)
                            .asGif()
                            .load(publicAds.getAdvertizes().get(labelAdPos).getMediaUrl())
                            .into(ad_label);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            String adtype=publicAds.getAdvertizes().get(publicAds.adBannerItemPosition).getMediaType();

            try {
                if (adtype.equals("BIMG")) {
                    imageCard.setVisibility(View.VISIBLE);
                    videoCard.setVisibility(View.GONE);
                    Picasso.with(this).load(publicAds.getAdvertizes().get(publicAds.adBannerItemPosition).getMediaUrl()).into(ad_banner);
                } else if (adtype.equals("BVID")) {
                    imageCard.setVisibility(View.GONE);
                    videoCard.setVisibility(View.VISIBLE);
                    try {
                        BandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
                        TrackSelector trackSelector = new DefaultTrackSelector(new AdaptiveTrackSelection.Factory(bandwidthMeter));
                        simpleExoPlayer = ExoPlayerFactory.newSimpleInstance(this, trackSelector);
                        DefaultHttpDataSourceFactory dataSourceFactory = new DefaultHttpDataSourceFactory("exoplayer_video");
                        ExtractorsFactory extractorsFactory = new DefaultExtractorsFactory();
                        MediaSource mediaSource = new ExtractorMediaSource(Uri.parse(publicAds.getAdvertizes().get(publicAds.adBannerItemPosition).getMediaUrl()), dataSourceFactory, extractorsFactory, null, null);
                        simpleExoPlayerView.setPlayer(simpleExoPlayer);
                        simpleExoPlayer.prepare(mediaSource);
                        simpleExoPlayer.setPlayWhenReady(true);
                        simpleExoPlayer.setRepeatMode(ExoPlayer.REPEAT_MODE_ALL);

                        simpleExoPlayerView.addOnAttachStateChangeListener(new View.OnAttachStateChangeListener() {
                            @Override
                            public void onViewAttachedToWindow(View view) {

                            }

                            @Override
                            public void onViewDetachedFromWindow(View view) {
                                if (simpleExoPlayer != null) {
                                    simpleExoPlayer.setPlayWhenReady(false);
                                }
                            }
                        });
                    } catch (Exception e) {
                        Log.d(TAG, "exoplayer error: " + e.toString());
                    }
                } else if (adtype.equals("BGIF")) {
                    imageCard.setVisibility(View.VISIBLE);
                    videoCard.setVisibility(View.GONE);
                    Glide.with(this)
                            .asGif()
                            .load(publicAds.getAdvertizes().get(publicAds.adBannerItemPosition).getMediaUrl())
                            .into(ad_banner);
//                Picasso.with(this).load(publicAds.getAdvertizes().get(publicAds.adBannerItemPosition).getMediaUrl()).into(ad_banner);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }*/
    }

    private void updateLabelAd() {
        ArrayList<Advertize> news_label_ads = publicAds.getNews_label_ads();
        Random random = new Random();
        int adSize = news_label_ads.size();
        if (adSize>0) {
            int randomNumber = random.nextInt(adSize);
            label_advertize = news_label_ads.get(randomNumber);
            if (label_advertize.getMediaType().equals("LIMG")) {
                Picasso.get().load(label_advertize.getMediaUrl()).placeholder(R.drawable.labelph).into(ad_label);
            } else if (label_advertize.getMediaType().equals("LGIF")) {
                Glide.with(this)
                        .asGif()
                        .load(label_advertize.getMediaUrl())
                        .into(ad_label);
            }
        }
    }

    private void updateBannerAd() {
        ArrayList<Advertize> news_banner_ads = publicAds.getNews_banner_ads();
        Random random = new Random();
        int adSize = news_banner_ads.size();
        if (adSize>0) {
            int randomNumber = random.nextInt(adSize);
            banner_advertize = news_banner_ads.get(randomNumber);
            if (banner_advertize.getMediaType().equals("BIMG")) {
                imageCard.setVisibility(View.VISIBLE);
                videoCard.setVisibility(View.GONE);
                Picasso.get().load(banner_advertize.getMediaUrl()).placeholder(R.drawable.placeholder).into(ad_banner);
                Log.d(TAG, "mediaurl="+banner_advertize.getMediaUrl());
            } else if (banner_advertize.getMediaType().equals("BVID")) {
                imageCard.setVisibility(View.GONE);
                videoCard.setVisibility(View.VISIBLE);
                try {
                    BandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
                    TrackSelector trackSelector = new DefaultTrackSelector(new AdaptiveTrackSelection.Factory(bandwidthMeter));
                    simpleExoPlayer = ExoPlayerFactory.newSimpleInstance(this, trackSelector);
                    DefaultHttpDataSourceFactory dataSourceFactory = new DefaultHttpDataSourceFactory("exoplayer_video");
                    ExtractorsFactory extractorsFactory = new DefaultExtractorsFactory();
                    MediaSource mediaSource = new ExtractorMediaSource(Uri.parse(banner_advertize.getMediaUrl()), dataSourceFactory, extractorsFactory, null, null);
                    simpleExoPlayerView.setPlayer(simpleExoPlayer);
                    simpleExoPlayer.prepare(mediaSource);
                    simpleExoPlayer.setPlayWhenReady(true);
                    simpleExoPlayer.setRepeatMode(ExoPlayer.REPEAT_MODE_ALL);

                    simpleExoPlayerView.addOnAttachStateChangeListener(new View.OnAttachStateChangeListener() {
                        @Override
                        public void onViewAttachedToWindow(View view) {

                        }

                        @Override
                        public void onViewDetachedFromWindow(View view) {
                            if (simpleExoPlayer != null) {
                                simpleExoPlayer.setPlayWhenReady(false);
                            }
                        }
                    });
                } catch (Exception e) {
                    Log.d(TAG, "exoplayer error: " + e.toString());
                }
            } else if (banner_advertize.getMediaType().equals("BGIF")) {
                imageCard.setVisibility(View.VISIBLE);
                videoCard.setVisibility(View.GONE);
                Glide.with(this)
                        .asGif()
                        .load(banner_advertize.getMediaUrl())
                        .into(ad_banner);
//                Picasso.with(this).load(publicAds.getAdvertizes().get(publicAds.adBannerItemPosition).getMediaUrl()).into(ad_banner);
            }
        }
    }

    private void setLikeUI() {
        try {

            LikeRoom likeRoom = new LikeRoom();
            likeRoom.setArticleId(article.getId());
            if (mDB.likeDao().getLikeWithArticleId(likeRoom.getArticleId()) == null) {
                like.setImageResource(R.drawable.ic_default_like_icon);
            } else {
                like.setImageResource(R.drawable.ic_liked_icon);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getUriIntentData() {
        try {
            Intent intent=getIntent();
//            String action = intent.getData();

            Uri data = intent.getData();
            assert data != null;
//            fetchData(data.getQueryParameter("title"));
            Log.d(TAG, "data: "+data.getQueryParameter("title"));
            Toast.makeText(this, "data: "+data.getQueryParameter("title"), Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void fetchData(final String title) {
        Log.d(TAG, "url:"+ Config.WEB_HOSE_API_URL);
        final ProgressDialog loading = ProgressDialog.show(this, "Fetching data", "Please wait...", false, false);

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Config.GET_SHARED_ARTICLE_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        loading.dismiss();
//                        progressBar.setVisibility(View.GONE);

                        Log.d(TAG,response);
//                        Toast.makeText(AttendenceActivity.this, "attendance response"+response, Toast.LENGTH_SHORT).show();
                        try {
                            //Creating the json object from the response
                            JSONObject jsonResponse = new JSONObject(response);
                            JSONArray jsonArray = jsonResponse.getJSONArray("articles");
                            Log.d("jsondatabatch",jsonArray.toString());
//                            Toast.makeText(AttendenceActivity.this, ""+jsonArray.toString(), Toast.LENGTH_SHORT).show();

                            String sourceVal, articleId;
                            int random = new Random().nextInt(5) + 10;
                            for (int i=0; i<jsonArray.length(); i++) {
                                JSONObject articleJson =jsonArray.getJSONObject(i);

                                sourceVal = articleJson.getJSONObject("source").getString("name");

                                article=new Article("",
                                        articleJson.getString("title"),
                                        articleJson.getString("urlToImage"),
                                        "",
                                        sourceVal,
                                        "img",
                                        articleJson.getString("description"),
                                        articleJson.getString("content"),
                                        articleJson.getString("url"),
                                        articleJson.getString("publishedAt"),
                                        Config.ARTICLE_ITEM_IMAGE);

                            }

                            setUI(article);


                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.d("mobileerror",""+e);
//                            Toast.makeText(getActivity(), ""+e.toString(), Toast.LENGTH_SHORT).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {


                        loading.dismiss();
//                        progressBar.setVisibility(View.GONE);
                        Toast.makeText(ArticleDescription.this, getResources().getString(R.string.couldnot_connect),Toast.LENGTH_LONG).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                //Adding the parameters to the request
                params.put("title", title);
                params.put("auth_key", sharedPreferenceConfig.readFcmToken());
                return params;
            }


        };

        //Adding request the the queue
        requestQueue.add(stringRequest);
    }

    private void fetchNotificationData(final String id, final String title) {
        Log.d(TAG, "url:"+ Config.GET_NOTIFICATION_ARTICLE_URL);
        Log.d(TAG, "params: "+id+" "+title);
        final ProgressDialog loading = ProgressDialog.show(this, "Fetching data", "Please wait...", false, false);

        String get_query = "?id="+id+"&auth_key="+sharedPreferenceConfig.readFcmToken();
        StringRequest stringRequest = new StringRequest(Request.Method.GET, Config.GET_NOTIFICATION_ARTICLE_URL+get_query,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        loading.dismiss();
//                        progressBar.setVisibility(View.GONE);

                        Log.d(TAG,response);
//                        Toast.makeText(AttendenceActivity.this, "attendance response"+response, Toast.LENGTH_SHORT).show();
                        try {
                            //Creating the json object from the response
                            JSONObject jsonResponse = new JSONObject(response);
                            JSONArray jsonArray = jsonResponse.getJSONArray("data");
                            Log.d(TAG,"data:"+jsonArray.toString());
//                            Toast.makeText(AttendenceActivity.this, ""+jsonArray.toString(), Toast.LENGTH_SHORT).show();

                            String sourceVal, articleId;
                            int random = new Random().nextInt(5) + 10;
                            for (int i=0; i<jsonArray.length(); i++) {
                                JSONObject articleJson =jsonArray.getJSONObject(i);

//                                sourceVal = articleJson.getJSONObject("source").getString("name");

                                article = new Article(articleJson.getString("id"),
                                        articleJson.getString("title"),
                                        articleJson.getString("urlToImage"),
                                        "",
                                        articleJson.getString("name"),
                                        "img",
                                        articleJson.getString("description"),
                                        articleJson.getString("content"),
                                        articleJson.getString("url"),
                                        articleJson.getString("publishedAt"),
                                        Config.ARTICLE_ITEM_IMAGE,
                                        articleJson.getInt("category_id"));

                            }

                            categoryId = article.getCategoryId();
                            setUI(article);


                        } catch (Exception e) {
                            e.printStackTrace();
                            Log.d(TAG,"mobileerror: "+e);
//                            Toast.makeText(getActivity(), ""+e.toString(), Toast.LENGTH_SHORT).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {


                        loading.dismiss();
//                        progressBar.setVisibility(View.GONE);
                        Log.d(TAG, error.getMessage()+" tostr:"+error.toString());
                        Toast.makeText(ArticleDescription.this, getResources().getString(R.string.couldnot_connect),Toast.LENGTH_LONG).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                //Adding the parameters to the request
                params.put("id", id);
                params.put("auth_key", sharedPreferenceConfig.readFcmToken());
                return params;
            }


        };

        //Adding request the the queue
        requestQueue.add(stringRequest);
    }

    private void setUI(final Article article) {

        ObjectAnimator.ofInt(nestedScrollView, "scrollY",  ad_label.getTop()).setDuration(1000).start();
        try {
           /* title.setText(article.getTitle());
            title.setGravity(Gravity.START);*/
           bottomll.setVisibility(View.GONE);
            description.setText(Html.fromHtml(article.getDescription()));
           /* if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                html = (Spannable) Html.fromHtml(article.getContent(), Html.FROM_HTML_MODE_LEGACY, imgGetter, null);
            } else {
                html = (Spannable) Html.fromHtml(article.getContent(), imgGetter, null);
            }*/
//            html = (Spannable) Html.fromHtml(article.getContent(), imageGetter, null);
//            content.setText(html);
           /* DisplayMetrics metrics = new DisplayMetrics();
            getWindowManager().getDefaultDisplay().getMetrics(metrics);
            double width = (metrics.widthPixels/2)*0.95;*/
//            String data = "<html><head> <meta name=\"viewport\"  content=\"width=device-width, initial-scale=1,shrink-to-fit=no\"> </head><body> <style> .zoom_container {display: none;} img{width:"+width+"px;} a{display:none !important;}</style>"+article.getContent()+"</body></html>";
            String data = "<html><head> <meta name=\"viewport\"  content=\"width=device-width, initial-scale=1,shrink-to-fit=no\"> </head><body> <style> .zoom_container {display: none;} p{ font-size:2.5em; margin:2%; line-height: 1.7em; } img{ width:95%; margin:2%; } </style>"+article.getContent()+"</body></html>";
            // webView.setPadding(0, 0, 0, 0);
                webView.setInitialScale(getScale());
            //webView.setInitialScale(30);
            webView.loadData(data, "text/html", "UTF-8");

            toolbarLayout.setTitle(" ");
//            toolbarLayout.setStatusBarScrimColor(getColor(R.color.colorPrimary));

            Log.d(TAG,"imgurl:"+article.getImg());
            title.setVisibility(View.GONE);
            articleTitle.setText(article.getTitle());
//            srcName.setText(article.getSrcName());

            Log.d(TAG, "setui url:"+article.getImg());

            Picasso.get().load(article.getImg()).into(backDrop, new Callback() {
                @Override
                public void onSuccess() {
                    Log.d(TAG, "success");
//                    backDrop.setVisibility(View.VISIBLE);
                }

                @Override
                public void onError(Exception ex) {
//                Toast.makeText(mCtx, "An error occurred", Toast.LENGTH_SHORT).show();
                    Log.d(TAG, "error occured: " + article.getImg());
                }

            });
        } catch (Exception e) {
            e.printStackTrace();
        }

        fetchFooterStoryData();

        appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            boolean isShow = true;
            int scrollRange = -1;

            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if (scrollRange == -1) {
                    scrollRange = appBarLayout.getTotalScrollRange();
                }
                if (scrollRange + verticalOffset == 0) {
                    toolbarLayout.setTitle(getResources().getString(R.string.app_title));
                    isShow = true;
                    backArrow.setVisibility(View.VISIBLE);
                } else if(isShow) {
                    toolbarLayout.setTitle(" ");//careful there should a space between double quote otherwise it wont work
                    isShow = false;
//                    backArrow.setVisibility(View.GONE);
                }
            }
        });

    }

    private Html.ImageGetter imgGetter = new Html.ImageGetter() {

        public Drawable getDrawable(String source) {
            Drawable drawable = null;
//            if(imageNumber == 1) {
                drawable = getResources().getDrawable(R.drawable.placeholder);
            /*    ++imageNumber;
            } else drawable = getResources().getDrawable(R.raw.a);
            drawable.setBounds(0, 0, drawable.getIntrinsicWidth(), drawable
                    .getIntrinsicHeight());*/

            return drawable;
        }
    };

    private void initView() {
        title=findViewById(R.id.title);
        description=findViewById(R.id.description);
        content=findViewById(R.id.content);
//        articleImg=findViewById(R.id.image);
//        srcName=findViewById(R.id.source_name);
        toolbarLayout=findViewById(R.id.collapseActionView);
        like=findViewById(R.id.like_btn);
        waShare=findViewById(R.id.wa_share);
        backDrop=findViewById(R.id.backdrop);
        articleTitle=findViewById(R.id.article_title);
        appBarLayout=findViewById(R.id.app_bar);
        ad_label=findViewById(R.id.ad);
        ad_banner=findViewById(R.id.ad_banner_img);
        imageCard=findViewById(R.id.image_ad_cv);
        videoCard=findViewById(R.id.video_ad_cv);
        simpleExoPlayerView=findViewById(R.id.ad_banner_video);
        recyclerView=findViewById(R.id.story_rv);
        bottomll=findViewById(R.id.bottom_ll);
        nestedScrollView=findViewById(R.id.nsv);
        webView=findViewById(R.id.webView);
    }

    private void getIntentData() {
        try {
            String articleArray = getIntent().getExtras().getString("article");
            Gson gson = new Gson();
            article = gson.fromJson(articleArray, Article.class);
            categoryId = article.getCategoryId();
            title.setText(article.getTitle());
            toolbarLayout.setTitle(" ");
//            toolbarLayout.setStatusBarScrimColor(getColor(R.color.colorPrimary));

            Log.d(TAG,"imgurl:"+article.getImg());
            title.setVisibility(View.GONE);
            articleTitle.setText(article.getTitle());
            if (!article.getDescription().isEmpty() || !article.getDescription().equals("null"))
                description.setText(Html.fromHtml(article.getDescription()));
            if (!article.getContent().isEmpty() || !article.getContent().equals("null")) {
               /* if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                    html = (Spannable) Html.fromHtml(article.getContent()+" <style> .zoom_container {display: none;} </style> ", Html.FROM_HTML_MODE_LEGACY, imageGetter, null);
                } else {
                    html = (Spannable) Html.fromHtml(article.getContent()+" <style> .zoom_container {display: none;} </style> ", imageGetter, null);
                }*/

                /*Display display = ((WindowManager) getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
                int width = display.getWidth();*/

//                DisplayMetrics metrics = new DisplayMetrics();
//                getWindowManager().getDefaultDisplay().getMetrics(metrics);
//                double width = (metrics.widthPixels/2)*0.95;
                String data = "<html><head> <meta name=\"viewport\"  content=\"width=device-width, initial-scale=1,shrink-to-fit=no\"> </head><body> <style> .zoom_container {display: none;} p{ font-size:2.5em; margin:2%; line-height: 1.7em; } img{ width:95%; margin:2%; } </style>"+article.getContent()+"</body></html>";
               // webView.setPadding(0, 0, 0, 0);
                webView.setInitialScale(getScale());
                //webView.setInitialScale(30);
                webView.loadData(data, "text/html", "UTF-8");
//                content.setText(html);
            }
            if (!article.getSrcName().isEmpty() || !article.getSrcName().equals("null"))
//                srcName.setText(article.getSrcName());

            try {
                URL url = new URL(article.getImg());
                Bitmap image = BitmapFactory.decodeStream(url.openConnection().getInputStream());
                Drawable d = new BitmapDrawable(getResources(), image);
                if (image!=null) {
//                    toolbarLayout.setBackground(d);
                }
            } catch(IOException e) {
                System.out.println(e);
            }

//            toolbarLayout.setTitle(article.getTitle());

            Picasso.get().load(article.getImg()).into(backDrop, new Callback() {
                @Override
                public void onSuccess() {
                    Log.d(TAG, "success");
//                    backDrop.setVisibility(View.VISIBLE);
                }

                @Override
                public void onError(Exception ex) {
//                Toast.makeText(mCtx, "An error occurred", Toast.LENGTH_SHORT).show();
                    Log.d(TAG, "error occured: " + article.getImg());
                }

            });

            fetchFooterStoryData();

            appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
                boolean isShow = true;
                int scrollRange = -1;

                @Override
                public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                    if (scrollRange == -1) {
                        scrollRange = appBarLayout.getTotalScrollRange();
                    }
                    if (scrollRange + verticalOffset == 0) {
                        toolbarLayout.setTitle(getResources().getString(R.string.app_title));
                        isShow = true;
                        backArrow.setVisibility(View.VISIBLE);
                    } else if(isShow) {
                        toolbarLayout.setTitle(" ");//careful there should a space between double quote otherwise it wont work
                        isShow = false;
//                        backArrow.setVisibility(View.GONE);
                    }
                }
            });

           /* articleImg.setVisibility(View.GONE);

            Picasso.with(this).load(article.getImg()).into(articleImg, new Callback() {
                @Override
                public void onSuccess() {
                    Log.d(TAG, "success");
                    articleImg.setVisibility(View.VISIBLE);
                }

                @Override
                public void onError() {
//                Toast.makeText(mCtx, "An error occurred", Toast.LENGTH_SHORT).show();
                    Log.d(TAG, "error occured: " + article.getImg());
                }

            });*/

         /*   articleImg.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    showFullImage(article.getImg());
                }
            });*/
        } catch (Exception e) {
            e.printStackTrace();
        }

        /*srcName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent viewIntent =
                        new Intent("android.intent.action.VIEW",
                                Uri.parse(article.getUrl()));
                startActivity(viewIntent);
            }
        });*/
    }

    private int getScale() {
        Display display = ((WindowManager) getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
        int width = display.getWidth();
        Double val = new Double(width)/new Double(800);
        val = val * 100d;
        return val.intValue();
    }

    private void showFullImage(String img) {
        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.full_image_dialog);
//        dialog.setTitle("Title...");
        ImageView imageView=dialog.findViewById(R.id.full_img);

        Picasso.get().load(img).into(imageView, new Callback() {
            @Override
            public void onSuccess() {
                Log.d(TAG, "success");
//                imageView.setVisibility(View.VISIBLE);
            }

            @Override
            public void onError(Exception ex) {
//                Toast.makeText(mCtx, "An error occurred", Toast.LENGTH_SHORT).show();
                Log.d(TAG, "error occured: "+article.getImg());
            }

        });

        dialog.show();

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.like_btn:
                likeArticle();
                break;
            case R.id.wa_share:
                shareArticle();
                break;
            case R.id.back_arrow:
                finish();
                break;
            case R.id.ad:   //label
                redirectToUrl(0);
                break;
            case R.id.ad_banner_img:  //banner
                redirectToUrl(1);
                break;
        }
    }

    private void redirectToUrl(int i) {
        try {
            String url="";
            if (i==0) {
//                url = publicAds.getLabelAdvertizes().get(publicAds.labelAdItemPosition).getAdUrl();
                url = label_advertize.getAdUrl();
            } else if (i==1) {
//                url = publicAds.getAdvertizes().get(publicAds.adBannerItemPosition).getAdUrl();
                url = banner_advertize.getAdUrl();
            }
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.setPackage("com.android.chrome");
            try {
                startActivity(intent);
            } catch (ActivityNotFoundException e) {
                // Chrome is probably not installed
                // Try with the default browser
                intent.setPackage(null);
                startActivity(intent);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void shareArticle() {
        try {
            Intent shareIntent = new Intent(Intent.ACTION_SEND);
            shareIntent.setType("text/plain");
            shareIntent.putExtra(Intent.EXTRA_SUBJECT, "CG More");
//            String shareMessage= "Let me recommend you this application\n";
            String shareMessage = ""+getResources().getString(R.string.share_text) + Uri.encode(article.getTitle());
            shareIntent.putExtra(Intent.EXTRA_TEXT, shareMessage);
            startActivity(Intent.createChooser(shareIntent,  getResources().getString(R.string.choose_one)));
        } catch(Exception e) {
            //e.toString();
        }
    }

    private void likeArticle() {
        LikeRoom likeRoom = new LikeRoom();
        if (article!=null) {
            likeRoom.setArticleId(article.getId());

            if (mDB.likeDao().getLikeWithArticleId(likeRoom.getArticleId()) == null) {
                mDB.likeDao().like(likeRoom);
                like.setImageResource(R.drawable.ic_liked_icon);
//            like.setTag(1);
            } else {
                mDB.likeDao().unlike(likeRoom.getArticleId());
                like.setImageResource(R.drawable.ic_default_like_icon);
//            like.setTag(0);
            }
        }
       /* if (Integer.parseInt(like.getTag().toString())==0) {

            like.setImageResource(R.drawable.ic_liked_icon);
            like.setTag(1);
        } else {
            like.setImageResource(R.drawable.ic_default_like_icon);
            like.setTag(0);
        }*/
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(broadcastReceiver);
    }
}
