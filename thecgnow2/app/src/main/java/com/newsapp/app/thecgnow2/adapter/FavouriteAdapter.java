package com.newsapp.app.thecgnow2.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.newsapp.app.thecgnow2.R;
import com.newsapp.app.thecgnow2.model.Recommend;
import com.newsapp.app.thecgnow2.model.Topic;

import java.util.ArrayList;

public class FavouriteAdapter extends RecyclerView.Adapter{

    ArrayList<Recommend> recommends;
    Context mCtx;
    public String TAG = "FavouriteAdapter";

    public FavouriteAdapter(ArrayList<Recommend> recommends, Context mCtx) {
        this.recommends = recommends;
        this.mCtx = mCtx;
    }

  /*  @Override
    public int getItemViewType(int position) {
        switch (recommends.get(position).getType()) {
            case Config.SUGGEST_RECT:
                return Config.SUGGEST_RECT;
            case Config.SUGGEST_CIRCLE:
                return Config.SUGGEST_CIRCLE;
                default:
                    return 0;
        }
    }*/

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView;

        itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.article_list_suggestion_parent_one, parent, false);

        return new ViewHolderOne(itemView);

        /*switch (viewType) {
            case Config.SUGGEST_RECT:
                itemView = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.article_suggest_topic_one, parent, false);
                return new RectReccomendViewHolder(itemView);
            case Config.SUGGEST_CIRCLE:
                itemView = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.article_suggest_topic_two, parent, false);
                return new CircleReccomendViewHolder(itemView);
                default:
                    return null;
        }*/
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        try {
            ((ViewHolderOne) holder).setSuggestion(recommends.get(position));
        } catch (Exception e) {
            e.printStackTrace();
        }

       /* switch (recommends.get(position).getType()) {
            case Config.SUGGEST_RECT:
                ((ViewHolderOne) holder).setSuggestion(recommends.get(position));
                break;
            case Config.SUGGEST_CIRCLE:
                ((ViewHolderTwo) holder).setSuggestion(recommends.get(position));
                break;
        }*/

    }

    @Override
    public int getItemCount() {
        return recommends.size();
    }

    public class ViewHolderOne extends RecyclerView.ViewHolder {

        RecyclerView recyclerView;
        TextView title;

        public ViewHolderOne(@NonNull View itemView) {
            super(itemView);
            title=itemView.findViewById(R.id.article_suggestion_title);
            recyclerView=itemView.findViewById(R.id.article_suggestion_rv);
        }

        public void setSuggestion(Recommend recommend) {
            ArrayList<Topic> topics=recommend.getTopics();
//            ArrayList<Topic> topics=recommend.getTopics();
            title.setText(recommend.getTitle());
            Log.d(TAG, "topics: "+topics);
            TopicHorizontalAdapter mAdapter=new TopicHorizontalAdapter(topics, mCtx, recommend.getType());
            recyclerView.setHasFixedSize(true);
            recyclerView.setLayoutManager(new LinearLayoutManager(mCtx, LinearLayoutManager.HORIZONTAL, false));
            recyclerView.setAdapter(mAdapter);
        }
    }

    public class ViewHolderTwo extends RecyclerView.ViewHolder {
        public ViewHolderTwo(@NonNull View itemView) {
            super(itemView);
        }

        public void setSuggestion(Recommend recommend) {
            /*ArrayList<Topic> topics=recommend.getTopics();
            title.setText(article.getTitle());
            TopicHorizontalAdapter mAdapter=new TopicHorizontalAdapter(topics, mCtx);
            recyclerView.setHasFixedSize(true);
            recyclerView.setLayoutManager(new LinearLayoutManager(mCtx, LinearLayoutManager.HORIZONTAL, false));
            recyclerView.setAdapter(mAdapter);*/
        }
    }

}
