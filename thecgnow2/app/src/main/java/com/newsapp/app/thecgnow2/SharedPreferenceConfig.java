package com.newsapp.app.thecgnow2;

import android.content.Context;
import android.content.SharedPreferences;

import com.newsapp.app.thecgnow2.R;

public class SharedPreferenceConfig {
    private final SharedPreferences sharedPreferences;
    private final Context context;

    public SharedPreferenceConfig( Context context) {
        this.context = context;
        this.sharedPreferences = context.getSharedPreferences(context.getResources().getString(R.string.login_pref),Context.MODE_PRIVATE);
    }

    public void writeDefaultLang(String val) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(context.getResources().getString(R.string.sp_def_lang),val);
        editor.apply();
    }

    public String readDefaultLang() {
        String val;
        val = sharedPreferences.getString(context.getResources().getString(R.string.sp_def_lang),"en");
        return val;
    }

    public void writeTicketGenerated(Boolean val) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(context.getResources().getString(R.string.sp_ticket_generate),val);
        editor.apply();
    }

    public Boolean readTicketGenerated() {
        boolean val;
        val = sharedPreferences.getBoolean(context.getResources().getString(R.string.sp_ticket_generate),false);
        return val;
    }

    public void writeTambolaTicketGenerated(Boolean val) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(context.getResources().getString(R.string.sp_tambola_generated),val);
        editor.apply();
    }

    public Boolean readTambolaTicketGenerated() {
        boolean val;
        val = sharedPreferences.getBoolean(context.getResources().getString(R.string.sp_tambola_generated),false);
        return val;
    }

    public void writeLocationIsActive(Boolean val) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(context.getResources().getString(R.string.sp_location_active),val);
        editor.apply();
    }

    public Boolean readLocationIsActive() {
        boolean val;
        val = sharedPreferences.getBoolean(context.getResources().getString(R.string.sp_location_active),false);
        return val;
    }

    public void setLocationSelected(Boolean val) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(context.getResources().getString(R.string.sp_loc_select),val);
        editor.apply();
    }

    public Boolean isLocationSelected() {
        boolean val;
        val = sharedPreferences.getBoolean(context.getResources().getString(R.string.sp_loc_select),false);
        return val;
    }

    public void writeLocationName(String val) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(context.getResources().getString(R.string.sp_location_name),val);
        editor.apply();
    }

    public String readLocationName() {
        String val = "0";
        val = sharedPreferences.getString(context.getResources().getString(R.string.sp_location_name),val);
        return val;
    }

    public void writeLocationNameHi(String val) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(context.getResources().getString(R.string.sp_location_name_hi),val);
        editor.apply();
    }

    public String readLocationNameHi() {
        String val = "0";
        val = sharedPreferences.getString(context.getResources().getString(R.string.sp_location_name_hi),val);
        return val;
    }

    public void writeLocationId(int val) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt(context.getResources().getString(R.string.sp_location_id),val);
        editor.apply();
    }

    public int readLocationId() {
        int val;
        val = sharedPreferences.getInt(context.getResources().getString(R.string.sp_location_id),0);
        return val;
    }

    public void writeFirstTimeUse(boolean val) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(context.getResources().getString(R.string.sp_first_use),val);
        editor.apply();
    }

    public boolean readFirstTimeUse() {
        boolean val;
        val = sharedPreferences.getBoolean(context.getResources().getString(R.string.sp_first_use),true);
        return val;
    }

    public void writeNotificationCount(int val) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt(context.getResources().getString(R.string.sp_notification_count),val);
        editor.apply();
    }

    public int readNotificationCount() {
        int val;
        val = sharedPreferences.getInt(context.getResources().getString(R.string.sp_notification_count),0);
        return val;
    }

    public void writeAdPage(int val) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt(context.getResources().getString(R.string.sp_ad_page),val);
        editor.apply();
    }

    public int readAdPage() {
        int val;
        val = sharedPreferences.getInt(context.getResources().getString(R.string.sp_ad_page),0);
        return val;
    }

    public void writeAlarmSet(Boolean val) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(context.getResources().getString(R.string.sp_alarm_set),val);
        editor.apply();
    }

    public Boolean readAlarmSet() {
        boolean val;
        val = sharedPreferences.getBoolean(context.getResources().getString(R.string.sp_alarm_set),false);
        return val;
    }

    public void writePusherEventSubscribed(Boolean val) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(context.getResources().getString(R.string.sp_subs_pusher),val);
        editor.apply();
    }

    public Boolean readPusherEventSubscribed() {
        boolean val;
        val = sharedPreferences.getBoolean(context.getResources().getString(R.string.sp_subs_pusher),false);
        return val;
    }

    public void writeLatitude(String val) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(context.getResources().getString(R.string.sp_latitude),val);
        editor.apply();
    }

    public String readLatitude() {
        String val = "";
        val = sharedPreferences.getString(context.getResources().getString(R.string.sp_latitude),val);
        return val;
    }

    public void writeLongitude(String val) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(context.getResources().getString(R.string.sp_longitude),val);
        editor.apply();
    }

    public String readLongitude() {
        String val = "";
        val = sharedPreferences.getString(context.getResources().getString(R.string.sp_longitude),val);
        return val;
    }

    public void writeResponseData(String sharedKey, String val) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(sharedKey,val);
        editor.apply();
    }

    public String readResponseData(String sharedKey) {
        String val = "";
        val = sharedPreferences.getString(sharedKey,val);
        return val;
    }

    public void writePusherSubsSuccess(Boolean val) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(context.getResources().getString(R.string.sp_subs_success),val);
        editor.apply();
    }

    public Boolean readPusherSubsSuccess() {
        boolean val;
        val = sharedPreferences.getBoolean(context.getResources().getString(R.string.sp_subs_success),false);
        return val;
    }

    public void writeFcmTokenRegistered(Boolean val) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(context.getResources().getString(R.string.sp_server_registered),val);
        editor.apply();
    }

    public Boolean readFcmTokenRegistered() {
        boolean val;
        val = sharedPreferences.getBoolean(context.getResources().getString(R.string.sp_server_registered),false);
        return val;
    }

    public void writePlaySelRegion(String val) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(context.getResources().getString(R.string.sp_play_sel_region),val);
        editor.apply();
    }

    public String readPlaySelRegion() {
        String val = "0";
        val = sharedPreferences.getString(context.getResources().getString(R.string.sp_play_sel_region),val);
        return val;
    }

    public void writeNewsSelRegion(String val) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(context.getResources().getString(R.string.sp_news_sel_region),val);
        editor.apply();
    }

    public String readNewsSelRegion() {
        String val = "0";
        val = sharedPreferences.getString(context.getResources().getString(R.string.sp_news_sel_region),val);
        return val;
    }

    public void writeFcmToken(String val) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(context.getResources().getString(R.string.sp_fcm_token),val);
        editor.apply();
    }

    public String readFcmToken() {
        String val = "";
        val = sharedPreferences.getString(context.getResources().getString(R.string.sp_fcm_token),val);
        return val;
    }

    public void writeTicketNum(int val) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt(context.getResources().getString(R.string.sp_ticket_num),val);
        editor.apply();
    }

    public int readTicketNum() {
        int val;
        val = sharedPreferences.getInt(context.getResources().getString(R.string.sp_ticket_num),0);
        return val;
    }
}
