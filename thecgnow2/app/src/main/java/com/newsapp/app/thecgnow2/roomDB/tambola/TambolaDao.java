package com.newsapp.app.thecgnow2.roomDB.tambola;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

@Dao
public interface TambolaDao {
    @Insert
    public void insertTicketNum(TambolaRoom tambolaRoom);

    @Query("select * from tambola_ticket_number where ticket_date=:date")
    public List<TambolaRoom> getTicketNumbers(String date);

    @Query("SELECT DISTINCT * FROM tambola_ticket_number")
    public TambolaRoom getTicket();

    @Query("SELECT DISTINCT ticket_checked_status FROM tambola_ticket_number where ticket_num=:ticketNum")
    public int getChecked(int ticketNum);

    @Query("update tambola_ticket_number set ticket_checked_status=:status where ticket_num=:ticketNum")
    public void setChecked(int ticketNum, int status);

   /* @Query("DELETE FROM tambola_ticket_number where ticket_date !=:date")
    public void deletePreviousTicket(String date);*/
   @Query("DELETE FROM tambola_ticket_number")
   public void deletePreviousTicket();
}
