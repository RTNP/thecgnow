package com.newsapp.app.thecgnow2.adapter;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.newsapp.app.thecgnow2.Config;
import com.newsapp.app.thecgnow2.R;
import com.newsapp.app.thecgnow2.TheCGNowApp;
import com.newsapp.app.thecgnow2.activity.ArticleDescription;
import com.newsapp.app.thecgnow2.activity.fragments.BottomNavigationDrawerFragment;
import com.newsapp.app.thecgnow2.model.Article;
import com.newsapp.app.thecgnow2.model.Topic;
import com.newsapp.app.thecgnow2.roomDB.CGNowDB;
import com.newsapp.app.thecgnow2.roomDB.follow.FollowRoom;

import com.google.android.exoplayer2.ExoPlayer;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory;
import com.google.android.exoplayer2.extractor.ExtractorsFactory;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelector;
import com.google.android.exoplayer2.ui.SimpleExoPlayerView;
import com.google.android.exoplayer2.upstream.BandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultHttpDataSourceFactory;
import com.google.gson.Gson;

import com.smarteist.autoimageslider.IndicatorView.animation.type.IndicatorAnimationType;
import com.smarteist.autoimageslider.SliderAnimations;
import com.smarteist.autoimageslider.SliderView;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.X509TrustManager;

import pl.droidsonroids.gif.GifImageView;
import pl.pzienowicz.autoscrollviewpager.AutoScrollViewPager;

public class ArticleHomeAdapter extends RecyclerView.Adapter{

    private static final String TAG = "ArticleHomeAdapter";
    ArrayList<Article> articles;
    Context mCtx;
    static FragmentManager fragmentManager;
    static CGNowDB mDB;
    TheCGNowApp cgNowApp;
    RecyclerView.AdapterDataObserver adapterDataObserver=new RecyclerView.AdapterDataObserver() {
        @Override
        public void onChanged() {
            super.onChanged();
            Log.d(TAG, "data set changed");
        }
    };

    public ArticleHomeAdapter(ArrayList<Article> articles, Context mCtx, FragmentManager supportFragmentManager) {
        this.articles = articles;
        this.mCtx = mCtx;
        this.fragmentManager = supportFragmentManager;
        cgNowApp=(TheCGNowApp)mCtx.getApplicationContext();
        cgNowApp.initDB(mCtx);
        mDB=cgNowApp.getDB(mCtx);
    }

    @Override
    public int getItemViewType(int position) {
        int viewType = articles.get(position).getType();
        Gson gson = new Gson();

        Log.d(TAG, "viewtype="+viewType+", article="+gson.toJson(articles.get(position)));

        switch (viewType) {
            case Config.ARTICLE_ITEM_IMAGE:
                return Config.ARTICLE_ITEM_IMAGE;
            case Config.ARTICLE_ITEM_AD_VIDEO:
                return Config.ARTICLE_ITEM_AD_VIDEO;
            case Config.ARTICLE_ITEM_AD_GIF:
                return Config.ARTICLE_ITEM_AD_GIF;
            case Config.ARTICLE_ITEM_AD:
                return Config.ARTICLE_ITEM_AD;
            case Config.LOADING_VIEW:
                return Config.LOADING_VIEW;
                default:
                    return 0;
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View itemView;

        switch (viewType) {
            case Config.ARTICLE_ITEM_IMAGE:
                itemView = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.article_list_home, parent, false);
                return new ArticleHolder(itemView);
            case Config.ARTICLE_ITEM_AD:
                itemView = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.article_list_ad, parent, false);
                return new Article_Ad_Image_Holder(itemView);
            case Config.ARTICLE_ITEM_AD_VIDEO:
                itemView = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.article_list_video_ad, parent, false);
                return new Article_Ad_Video_Holder(itemView);
            case Config.ARTICLE_ITEM_AD_GIF:
                itemView = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.article_list_gif_ad, parent, false);
                return new Article_Ad_Gif_Holder(itemView);
            case Config.LOADING_VIEW:
                itemView = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_loading, parent, false);
                return new Article_Loading_Holder(itemView);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, final int position) {
        //http://www.iotacademycg.com/cadApp/head-office/uploads/assignments/Screenshot_20190921_123553.jpg
        switch (getItemViewType(position)) {
            case Config.ARTICLE_ITEM_IMAGE:
                ((ArticleHolder) holder).setImageArticles(articles.get(position), mCtx);
                break;
            case Config.ARTICLE_ITEM_VIDEO:
                ((VideoHolder) holder).setVideoArticles(articles.get(position), mCtx);
                break;
            case Config.ARTICLE_ITEM_AD:
                    ((Article_Ad_Image_Holder) holder).setImageAd(articles.get(position), mCtx);
                break;
            case Config.SUGGEST_CIRCLE:
            case Config.SUGGEST_RECT:
                ((TopicHolder) holder).suggestTopics(articles.get(position), mCtx);
                break;
            case Config.ARTICLE_ITEM_AD_VIDEO:
                ((Article_Ad_Video_Holder) holder).setVideoAd(articles.get(position), mCtx);
                break;
            case Config.ARTICLE_ITEM_AD_GIF:
                ((Article_Ad_Gif_Holder) holder).setGifAd(articles.get(position), mCtx);
                break;
            case Config.LOADING_VIEW:
                ((Article_Loading_Holder) holder).setLoadingView(articles.get(position), mCtx);
                break;
        }
    }

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
        Log.d(TAG, "adapter attached rv");
    }

    @Override
    public int getItemCount() {
        return articles.size();
    }

    public static class ArticleHolder extends RecyclerView.ViewHolder {
        ImageView imageView, srcImage, waShare, dotMenu;
        TextView title, followTxt, srcName, profileTxt, description;
        CardView cardView;
        LinearLayout follow;

        public ArticleHolder(View itemView) {
            super(itemView);
            imageView=itemView.findViewById(R.id.article_img);
            title=itemView.findViewById(R.id.title);
            cardView=itemView.findViewById(R.id.article_img_cv);
            waShare=itemView.findViewById(R.id.wa_share);
            follow=itemView.findViewById(R.id.follow);
            dotMenu=itemView.findViewById(R.id.dot_menu);
            description=itemView.findViewById(R.id.description);
        }

        public void setImageArticles(final Article article, final Context mCtx) {
            cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Gson gson=new Gson();
                    String articleArray=gson.toJson(article);
                    Intent intent=new Intent(mCtx, ArticleDescription.class);
                    intent.putExtra("article",articleArray);
                    mCtx.startActivity(intent);
                }
            });

            dotMenu.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Bundle args = new Bundle();
                    Gson gson=new Gson();
                    args.putString("article", gson.toJson(article));
                    BottomNavigationDrawerFragment bndf = new BottomNavigationDrawerFragment();
                    bndf.setArguments(args);
                    bndf.show(fragmentManager, bndf.getTag());
                }
            });

            waShare.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    try {
                        Intent shareIntent = new Intent(Intent.ACTION_SEND);
                        shareIntent.setType("text/plain");
                        shareIntent.putExtra(Intent.EXTRA_SUBJECT, "CG More");
                        String shareMessage = "http://iotacademycg.com/cgmore?"+mCtx.getResources().getString(R.string.share_text)+ Uri.encode(article.getTitle().replaceAll(" ", "-"));
                        shareIntent.putExtra(Intent.EXTRA_TEXT, shareMessage);
                        mCtx.startActivity(Intent.createChooser(shareIntent, "choose one"));
                    } catch(Exception e) {
                        //e.toString();
                    }
                }
            });

            title.setText(article.getTitle());
            description.setText(article.getDescription());

            Log.d(TAG, "srcimage: "+article.getSrcImage());

            Log.d(TAG, "imageurl:"+article.getImg());
            Picasso.get().load(article.getImg()).placeholder(R.drawable.placeholder).into(imageView, new Callback() {
                @Override
                public void onSuccess() {
                    Log.d(TAG, article.getImg());
                }

                @Override
                public void onError(Exception e) {
//                Toast.makeText(mCtx, "An error occurred", Toast.LENGTH_SHORT).show();
                    Log.d(TAG, "error occured: "+article.getImg());
                }

            });

        }
    }


    public static class TopicHolder extends RecyclerView.ViewHolder {
        TextView title;
        RecyclerView recyclerView;

        public TopicHolder(@NonNull View itemView) {
            super(itemView);
            title=itemView.findViewById(R.id.article_suggestion_title);
            recyclerView=itemView.findViewById(R.id.article_suggestion_rv);
        }

        public void suggestTopics(Article article, Context mCtx) {
            ArrayList<Topic> topics=article.getTopics();
            title.setText(article.getTitle());
            TopicHorizontalAdapter mAdapter=new TopicHorizontalAdapter(topics, mCtx, article.getType());
            recyclerView.setHasFixedSize(true);
            recyclerView.setLayoutManager(new LinearLayoutManager(mCtx, LinearLayoutManager.HORIZONTAL, false));
            recyclerView.setAdapter(mAdapter);
        }
    }

    public static class VideoHolder extends RecyclerView.ViewHolder {
        SimpleExoPlayerView simpleExoPlayerView;
        SimpleExoPlayer simpleExoPlayer;
        CardView cardView;
        ImageView waShare, dotMenu;
        LinearLayout follow;
        TextView followTxt, srcName, profileTxt;

        public VideoHolder(View itemView) {
            super(itemView);
            simpleExoPlayerView=itemView.findViewById(R.id.article_video);
            cardView=itemView.findViewById(R.id.article_video_cv);
            waShare=itemView.findViewById(R.id.wa_share);
            follow=itemView.findViewById(R.id.follow);
            dotMenu=itemView.findViewById(R.id.dot_menu);
            followTxt=itemView.findViewById(R.id.follow_txt);
            srcName=itemView.findViewById(R.id.source_name);
            profileTxt=itemView.findViewById(R.id.profile_txt);
        }

        public void setVideoArticles(final Article article, final Context mCtx) {

            srcName.setText(article.getSrcName());
            profileTxt.setText(""+article.getSrcName().charAt(0));

            cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Toast.makeText(mCtx, "video cv clicked", Toast.LENGTH_SHORT).show();
                }
            });

            if (mDB.followDao().getFollowingWithSrcName(article.getSrcName())==null) {
                followTxt.setText(mCtx.getResources().getString(R.string.follow_btn));
            } else {
                followTxt.setText("following");
            }

            follow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    FollowRoom data = new FollowRoom();
                    data.setSourceName(article.getSrcName());
                    if (mDB.followDao().getFollowingWithSrcName(article.getSrcName())==null) {
                        mDB.followDao().followTo(data);
                        followTxt.setText("following");
                        Toast.makeText(mCtx, "followed to " + article.getSrcName(), Toast.LENGTH_SHORT).show();
                    } else {
                        mDB.followDao().unFollowTo(data.getSourceName());
                        followTxt.setText(mCtx.getResources().getString(R.string.follow_btn));
                        Toast.makeText(mCtx, "unfollowed to " + article.getSrcName(), Toast.LENGTH_SHORT).show();
                    }
                }
            });

            dotMenu.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    BottomNavigationDrawerFragment bndf = new BottomNavigationDrawerFragment();
//                    bndf.show(fragmentManager, bndf, bndf.getTag());
                    bndf.show(fragmentManager, bndf.getTag());
                }
            });

            waShare.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    try {
                        Intent shareIntent = new Intent(Intent.ACTION_SEND);
                        shareIntent.setType("text/plain");
                        shareIntent.putExtra(Intent.EXTRA_SUBJECT, "CG More");
                        String shareMessage = "http://iotacademycg.com/cgmore?" +  Uri.encode(mCtx.getResources().getString(R.string.share_text)+article.getTitle().replaceAll(" ", "-"));
                        shareIntent.putExtra(Intent.EXTRA_TEXT, shareMessage);
                        mCtx.startActivity(Intent.createChooser(shareIntent, "choose one"));
                    } catch(Exception e) {
                        //e.toString();
                    }
                }
            });
            try {
                BandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
                TrackSelector trackSelector = new DefaultTrackSelector(new AdaptiveTrackSelection.Factory(bandwidthMeter));
                simpleExoPlayer = ExoPlayerFactory.newSimpleInstance(mCtx, trackSelector);
                DefaultHttpDataSourceFactory dataSourceFactory = new DefaultHttpDataSourceFactory("exoplayer_video");
                ExtractorsFactory extractorsFactory = new DefaultExtractorsFactory();
                MediaSource mediaSource = new ExtractorMediaSource(Uri.parse(article.getVideo()), dataSourceFactory, extractorsFactory, null, null);
                simpleExoPlayerView.setPlayer(simpleExoPlayer);
                simpleExoPlayer.prepare(mediaSource);
                simpleExoPlayer.setPlayWhenReady(true);
                simpleExoPlayer.setRepeatMode(ExoPlayer.REPEAT_MODE_ALL);
            } catch (Exception e) {
                Log.d(TAG, "exoplayer error: "+e.toString());
            }
        }
    }

    public static class Article_Ad_Image_Holder extends RecyclerView.ViewHolder {

        CardView cardView;
        ImageView adImage;
        TextView adText;
//        SliderView sliderView;
        AutoScrollViewPager sliderView;

        public Article_Ad_Image_Holder(View itemView) {
            super(itemView);

            cardView=itemView.findViewById(R.id.image_ad_cv);
            adImage=itemView.findViewById(R.id.image_ad);
//            adText=itemView.findViewById(R.id.image_ad_txt);
//            sliderView = itemView.findViewById(R.id.adImgSlider);
            sliderView = itemView.findViewById(R.id.view_pager);
        }

        public void setImageAd(final Article article, final Context mCtx) {

            cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Log.d(TAG, "adurl="+article.getAdUrl());
                    try {
                        Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse(article.getAdUrl()));
                        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        i.setPackage("com.android.chrome");
                        try {
                            mCtx.startActivity(i);
                        } catch (ActivityNotFoundException e) {
                            // Chrome is probably not installed
                            // Try with the default browser
                            i.setPackage(null);
                            mCtx.startActivity(i);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            });

            try {
                Integer size = article.getAdArticles().size();
                ArrayList<Article> adArticles = new ArrayList<>();
                adArticles.addAll(article.getAdArticles());
                ArrayList<String> test = new ArrayList<String>();
                for (int i = 0; i < size; i++) {
                    test.add(adArticles.get(i).getImg());
                }
                ArrayList<String> test1 = new ArrayList<String>();
                for (int i = 0; i < size; i++) {
                    test1.add(adArticles.get(i).getUrl());
                }
                ArrayList<String> test2 = new ArrayList<String>();
                for (int i = 0; i < size; i++) {
                    test2.add(adArticles.get(i).getTitle());
                }

                Picasso.get().load(test.get(0)).placeholder(R.drawable.placeholder).into(adImage);

                Log.d(TAG, "before slider view");

            } catch (Exception e) {
                e.printStackTrace();
            }

            Log.d(TAG, "after slider view");

        }
    }

    public static class Article_Loading_Holder extends RecyclerView.ViewHolder {
        ProgressBar progressBar;

        public Article_Loading_Holder(@NonNull View itemView) {
            super(itemView);
            progressBar = itemView.findViewById(R.id.loading_item);
        }

        public void setLoadingView(Article article, Context mCtx) {

        }
    }

    public static class Article_Ad_Gif_Holder extends RecyclerView.ViewHolder {

        CardView cardView;
        GifImageView adGif;
        TextView adText;

        public Article_Ad_Gif_Holder(View itemView) {
            super(itemView);

            cardView=itemView.findViewById(R.id.gif_ad_cv);
            adGif=itemView.findViewById(R.id.gif_ad);
            adText=itemView.findViewById(R.id.gif_ad_txt);
        }

        public void setGifAd(final Article article, final Context mCtx) {

            cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse(article.getUrl()));
                    i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    i.setPackage("com.android.chrome");
                    try {
                        mCtx.startActivity(i);
                    } catch (ActivityNotFoundException e) {
                        // Chrome is probably not installed
                        // Try with the default browser
                        i.setPackage(null);
                        mCtx.startActivity(i);
                    }
                }
            });

            Glide.with(mCtx)
                    .asGif()
                    .load(article.getImg())
                    .into(adGif);

        }
    }

    public static class Article_Ad_Video_Holder extends RecyclerView.ViewHolder implements View.OnClickListener {
        SimpleExoPlayerView simpleExoPlayerView;
        SimpleExoPlayer simpleExoPlayer;
        CardView cardView;
        ImageView mute;
        float currentvolume;

        public Article_Ad_Video_Holder(View itemView) {
            super(itemView);
            simpleExoPlayerView=itemView.findViewById(R.id.article_video_ad);
            cardView=itemView.findViewById(R.id.video_ad_cv);
            mute=itemView.findViewById(R.id.mute);
        }

        public void setVideoAd(final Article article, final Context mCtx) {

            cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
//                    Toast.makeText(mCtx, "video ad clicked", Toast.LENGTH_SHORT).show();
                    Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse(article.getUrl()));
                    i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    i.setPackage("com.android.chrome");
                    try {
                        mCtx.startActivity(i);
                    } catch (ActivityNotFoundException e) {
                        // Chrome is probably not installed
                        // Try with the default browser
                        i.setPackage(null);
                        mCtx.startActivity(i);
                    }
                }
            });

            simpleExoPlayerView.addOnAttachStateChangeListener(new View.OnAttachStateChangeListener() {
                @Override
                public void onViewAttachedToWindow(View view) {
                    Log.d(TAG, "exoplayerview attached.");
                }

                @Override
                public void onViewDetachedFromWindow(View view) {
                    Log.d(TAG, "exoplayerview dettached.");
                    if (simpleExoPlayer!=null) {
                        simpleExoPlayer.setPlayWhenReady(false);
                    }
                }
            });

            Log.d(TAG, "exoplayer called");
            try {
                BandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
                TrackSelector trackSelector = new DefaultTrackSelector(new AdaptiveTrackSelection.Factory(bandwidthMeter));
                simpleExoPlayer = ExoPlayerFactory.newSimpleInstance(mCtx, trackSelector);
                DefaultHttpDataSourceFactory dataSourceFactory = new DefaultHttpDataSourceFactory("exoplayer_video");
                ExtractorsFactory extractorsFactory = new DefaultExtractorsFactory();
                MediaSource mediaSource = new ExtractorMediaSource(Uri.parse("https://ia601204.us.archive.org/31/items/ShortVideoClipNature.mp4SD/Short%20video%20clip-nature.mp4-SD.mp4"), dataSourceFactory, extractorsFactory, null, null);
                simpleExoPlayerView.setPlayer(simpleExoPlayer);
                simpleExoPlayer.prepare(mediaSource);
                simpleExoPlayer.setPlayWhenReady(true);

                simpleExoPlayer.setRepeatMode(ExoPlayer.REPEAT_MODE_ALL);
            } catch (Exception e) {
                Log.d(TAG, "exoplayer error: " + e.toString());
            }

            mute.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (view.getId()==R.id.mute) {
                currentvolume = simpleExoPlayer.getVolume();
                Log.d(TAG, "currentVol: "+currentvolume);
                if (Float.compare(currentvolume, 0f)==0) {
                    simpleExoPlayer.setVolume(1f);
                    mute.setImageResource(R.drawable.unmute);
                } else {
                    simpleExoPlayer.setVolume(0f);
                    mute.setImageResource(R.drawable.mute);
                }
            }
        }
    }

}
