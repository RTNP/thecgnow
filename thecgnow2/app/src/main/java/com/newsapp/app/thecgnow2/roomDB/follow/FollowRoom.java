package com.newsapp.app.thecgnow2.roomDB.follow;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "followings")
public class FollowRoom {
    @PrimaryKey(autoGenerate = true)
    private int id;

    @ColumnInfo(name = "follow_source_name")
    private String sourceName;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSourceName() {
        return sourceName;
    }

    public void setSourceName(String sourceName) {
        this.sourceName = sourceName;
    }
}
