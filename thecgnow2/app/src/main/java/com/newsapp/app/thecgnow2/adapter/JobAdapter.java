package com.newsapp.app.thecgnow2.adapter;

import android.content.Context;
import android.content.Intent;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.newsapp.app.thecgnow2.Config;
import com.newsapp.app.thecgnow2.R;
import com.newsapp.app.thecgnow2.activity.JobDescriptionActivity;
import com.newsapp.app.thecgnow2.activity.JobListActivity;
import com.newsapp.app.thecgnow2.model.Job;
import com.newsapp.app.thecgnow2.model.JobList;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class JobAdapter extends RecyclerView.Adapter {

    ArrayList<JobList> jobs;
    Context mCtx;

    public JobAdapter(ArrayList<JobList> jobs, Context mCtx) {
        this.jobs = jobs;
        this.mCtx = mCtx;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView;

        itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.job_list_item, parent, false);

        return new ViewHolderOne(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        JobList job = jobs.get(position);
        try {
            ((ViewHolderOne) holder).setViews(job);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return jobs.size();
    }

    public class ViewHolderOne extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView title, description;
        ImageView img;
        LinearLayout linearLayout;

        public ViewHolderOne(@NonNull View itemView) {
            super(itemView);
            title=itemView.findViewById(R.id.title);
            img=itemView.findViewById(R.id.img);
            description=itemView.findViewById(R.id.description);
            linearLayout=itemView.findViewById(R.id.ll);
        }

        public void setViews(JobList job) {
            title.setText(job.getJobTitle());
            Picasso.get().load(Config.BASE_URL+job.getAdUrl()).placeholder(R.drawable.placeholder).into(img);
            description.setText(Html.fromHtml(job.getDiscription()));
            linearLayout.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (view.getId()==R.id.ll) {
                /*Intent intent=new Intent(mCtx, JobListActivity.class);
                intent.putExtra("cat_id", jobs.get(getAdapterPosition()).getId());
                mCtx.startActivity(intent);*/

                Gson gson = new Gson();
                String jobArray=gson.toJson(jobs.get(getAdapterPosition()));
                Intent intent=new Intent(mCtx, JobDescriptionActivity.class);
                intent.putExtra("job", jobArray);
                mCtx.startActivity(intent);
            }
        }
    }
}
