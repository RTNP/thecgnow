package com.newsapp.app.thecgnow2.activity.fragments.housieboard;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.newsapp.app.thecgnow2.R;
import com.newsapp.app.thecgnow2.activity.fragments.HomeFragment;

public class ComingSoonFragment extends Fragment {

    private static final String ARG_NAME = "arg_name";

    public static ComingSoonFragment newInstance(String name) {
        Bundle bundle = new Bundle();
        bundle.putString(ARG_NAME, name);
        ComingSoonFragment dashboardFragment = new ComingSoonFragment();
        dashboardFragment.setArguments(bundle);
        return dashboardFragment;
    }

    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_coming_soon, container, false);

        initView(view);

        return view;

    }

    private void initView(View view) {
    }
}
