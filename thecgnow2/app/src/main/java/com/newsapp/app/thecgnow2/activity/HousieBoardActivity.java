package com.newsapp.app.thecgnow2.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import android.animation.ObjectAnimator;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.GridLayout;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.newsapp.app.thecgnow2.BuildConfig;
import com.newsapp.app.thecgnow2.Config;
import com.newsapp.app.thecgnow2.NotificationHelper;
import com.newsapp.app.thecgnow2.PusherConfig;
import com.newsapp.app.thecgnow2.R;
import com.newsapp.app.thecgnow2.SharedPreferenceConfig;
import com.newsapp.app.thecgnow2.TheCGNowApp;
import com.newsapp.app.thecgnow2.activity.fragments.housieboard.BoardFragment;
import com.newsapp.app.thecgnow2.activity.ui.housieboard.SectionsPagerAdapter;
import com.newsapp.app.thecgnow2.adapter.WinnerListAdapter;
import com.newsapp.app.thecgnow2.roomDB.CGNowDB;
import com.newsapp.app.thecgnow2.roomDB.tambola.TambolaRoom;
import com.google.android.material.tabs.TabLayout;
import com.pusher.client.Pusher;
import com.pusher.client.PusherOptions;
import com.pusher.client.channel.Channel;
import com.pusher.client.channel.ChannelEventListener;
import com.pusher.client.channel.SubscriptionEventListener;
import com.pusher.client.connection.ConnectionEventListener;
import com.pusher.client.connection.ConnectionStateChange;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Random;

import javax.sql.ConnectionEvent;

public class HousieBoardActivity extends AppCompatActivity implements TicketBoardView.OnToggledListener, View.OnClickListener, ChannelEventListener, ConnectionEventListener {

    private final String TAG = "HousieBoardActivity";
    TicketBoardView[] ticketBoardView;
    GridLayout myGridLayout;
    TicketTextView[] textView;
    TextView ticketNum;
    int[][] cominations, threeCombination, colWiseThreeToTwoCombNumbers;

    ImageView adLabel, adBanner, leftAdLabel, rightAdLabel;

    SharedPreferenceConfig sharedPreferenceConfig;

    ScrollView mainScrollView;
    RecyclerView recyclerView;
    WinnerListAdapter mAdapter;

    String number;
    int random;
    private Handler myHandler;
    private final long startTime = System.currentTimeMillis();

    CGNowDB mDB;
    TheCGNowApp publicAds;

    BroadcastReceiver broadcastReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_housie_board);

       /* initView();
        initDB();*/

        setUpViewPager();
        sharedPreferenceConfig=new SharedPreferenceConfig(this);

        if (!TheCGNowApp.isPusherSubsBind) {
            setPusherOptions();
        }

//        setPusherOptions();

        getIntentForShare();    // if activity starts from NotificationReceiver

    }

    private void getIntentForShare() {
        try {
            String imageuri = getIntent().getExtras().getString("shareuri");
            Uri uri = Uri.parse(imageuri);
            shareApp();
        } catch (Exception e) {
//            e.printStackTrace();
        }
    }

    private void shareApp() {
        Log.d(TAG, "share app called");
        try {
            Intent shareIntent = new Intent(Intent.ACTION_SEND);
            shareIntent.setType("text/plain");
            shareIntent.putExtra(Intent.EXTRA_SUBJECT, "CG More");
            String shareMessage= "Let me recommend you this application\n";
            shareMessage = shareMessage + "https://play.google.com/store/apps/details?id=" + BuildConfig.APPLICATION_ID ;
            shareIntent.putExtra(Intent.EXTRA_TEXT, shareMessage);
            startActivity(Intent.createChooser(shareIntent, "choose one"));
            Log.d(TAG, "share app called inside try block");
        } catch(Exception e) {
            //e.toString();
            e.printStackTrace();
        }
    }


    private void setUpRTCNotification() {
        String hours="8";
        String minutes="0";
        NotificationHelper.scheduleRepeatingRTCNotification(this, hours, minutes);
        NotificationHelper.enableBootReceiver(this);
        Log.d(TAG, "rtnoti set success");
    }

    private void setUpViewPager() {
        SectionsPagerAdapter sectionsPagerAdapter = new SectionsPagerAdapter(this, getSupportFragmentManager());
        ViewPager viewPager = findViewById(R.id.view_pager);
        viewPager.setAdapter(sectionsPagerAdapter);
//        viewPager.setCurrentItem(1);
        TabLayout tabs = findViewById(R.id.tabs);
        tabs.setupWithViewPager(viewPager);
    }

    private void setAdvertise() {
        try {
            Picasso.get().load(publicAds.getLabelAdvertizes().get(publicAds.labelAdItemPosition).getMediaUrl()).into(adLabel);
            Picasso.get().load(publicAds.getLabelAdvertizes().get(publicAds.labelAdItemPosition).getMediaUrl()).into(leftAdLabel);
            Picasso.get().load(publicAds.getLabelAdvertizes().get(publicAds.labelAdItemPosition).getMediaUrl()).into(rightAdLabel);
            Picasso.get().load(publicAds.getAdvertizes().get(publicAds.adBannerItemPosition).getMediaUrl()).into(adBanner);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void registerBroadcastReceiver() {
        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                refreshAds();
            }
        };
        registerReceiver(broadcastReceiver, new IntentFilter("com.example.thecgnow.activity.adRefresh"));
    }

    private void refreshAds() {
            /*Picasso.with(this).load(publicAds.getLabelAdvertizes().get(publicAds.labelAdItemPosition).getMediaUrl()).into(adLabel);
            Picasso.with(this).load(publicAds.getAdvertizes().get(publicAds.adBannerItemPosition).getMediaUrl()).into(adBanner);*/
            setAdvertise();
    }

    private int getDayDiffrence() {
        long cDate = Calendar.getInstance().getTimeInMillis();
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy");
        long pDate = 0;
        try {
            pDate = sdf.parse((mDB.tambolaDao().getTicket().getTicketDate())).getTime();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        long diff = cDate - pDate;

        float dayCount = (float) diff / (24 * 60 * 60 * 1000);
        Log.d(TAG, "daycount: "+(int)dayCount+" pdate:"+pDate+" cDate: "+cDate);
        return (int)dayCount;
    }

    private void retrieveTicketFromDB() {
        String curDate=getCurrentDate();

        try {
            List<TambolaRoom> data = mDB.tambolaDao().getTicketNumbers(curDate);
            for (int row = 0; row < 3; row++) {
                for (int col = 0; col < 9; col++) {
                    int ticketNum = data.get(row * 9 + col).getTicketNum();
                    if (ticketNum != 0)
                        textView[row * 9 + col].setText(ticketNum + "");
                    else
                        textView[row * 9 + col].setText(" ");

                    Log.d(TAG, "data check: " + data.get(row * 9 + col).getChecked());

                    if (mDB.tambolaDao().getChecked(ticketNum) == 1) {
                        textView[row * 9 + col].performClick();
                        Log.d(TAG, "pos: " + (row * 9 + col) + " clicked.");
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void initDB() {
        publicAds=(TheCGNowApp)getApplicationContext();
        publicAds.initDB(getApplicationContext());
        mDB=publicAds.getDB(getApplicationContext());
    }

    private void focusOnView(){
        new Handler().post(new Runnable() {
            @Override
            public void run() {
//                mainScrollView.smoothScrollTo(0, ticketNum.getTop());
                ObjectAnimator.ofInt(mainScrollView, "scrollY",  ticketNum.getTop()).setDuration(1000).start();
            }
        });
    }

    private void setPusherOptions() {
        TheCGNowApp.isPusherSubsBind=true;
        PusherOptions options = new PusherOptions();
        options.setCluster(PusherConfig.cluster);

        Pusher pusher = new Pusher(PusherConfig.key, options);

        Channel channel = pusher.subscribe("my-channel", this, "test-topic");

        Log.d(TAG, "subscribed to event: "+channel.getName());
        pusher.connect(this);
    }

    private void generateColWiseNum() {
        int count=0, r;
        colWiseThreeToTwoCombNumbers=new int[9][3];
        ArrayList<Integer> randoms=new ArrayList<>();
        for (int row=0; row<3; row++) {
            count=0;
            for (int col=0; col<9; col++) {
                if (count>=5) {
                    break;
                }
                if (threeCombination[count][row]==col+1) {
                    r=generateNumForCol(col, randoms);
                    colWiseThreeToTwoCombNumbers[col][row]=r;
                    randoms.add(r);
                    count++;
                } else {
                    colWiseThreeToTwoCombNumbers[col][row]=0;
//                    textView[row * 9 + col].setText(" ");
                }
                System.out.print("arrcol: "+colWiseThreeToTwoCombNumbers[col][row]+" ");
            }
            System.out.print("arrcolcount: "+count);
            System.out.println();
        }
        printArray2();
        System.out.println("after short:");

        shortColNumbers();
    }

    private void shortColNumbers() {
        /*for (int i = 0; i < 5; i++) {
            for (int j=0; j<3; j++) {
                for (int k = 0; k < 3 - 1 - j; k++) {
                    if (colWiseThreeToTwoCombNumbers[i][k] > colWiseThreeToTwoCombNumbers[i][k+1]) {
                        int temp = colWiseThreeToTwoCombNumbers[i][k];
                        colWiseThreeToTwoCombNumbers[i][k] = colWiseThreeToTwoCombNumbers[i][k+1];
                        colWiseThreeToTwoCombNumbers[i][k+1] = temp;
                    }
                    System.out.print("arr: "+colWiseThreeToTwoCombNumbers[i][k]+" ");
                }
                System.out.println();
            }
            System.out.print("Iteration " + (i + 1) + ": ");
        }*/

        for (int row=0; row<3; row++) {
            for (int col=0; col<9; col++) {
                for (int trow=0; trow<3-1-row; trow++) {
                    if (colWiseThreeToTwoCombNumbers[col][trow] > colWiseThreeToTwoCombNumbers[col][trow + 1]) {
                        if (colWiseThreeToTwoCombNumbers[col][trow+1]==0 && trow==0) {
                                if (colWiseThreeToTwoCombNumbers[col][trow] > colWiseThreeToTwoCombNumbers[col][trow + 2] && colWiseThreeToTwoCombNumbers[col][trow + 2]!=0) {
                                    swapFirstAndLastNum(col, trow);
                                }

                        } else if (colWiseThreeToTwoCombNumbers[col][trow+1]!=0){
                            swapNum(col, trow);
                        }
                    }
                }
//                System.out.print("arrcolshort: "+colWiseThreeToTwoCombNumbers[col][row]+" ");
            }
//            System.out.println();
        }
        printArray2();
    }

    private void swapFirstAndLastNum(int col, int row) {
        int temp = colWiseThreeToTwoCombNumbers[col][row];
        colWiseThreeToTwoCombNumbers[col][row] = colWiseThreeToTwoCombNumbers[col][row+2];
        colWiseThreeToTwoCombNumbers[col][row+2] = temp;
    }

    private void printArray2() {
        for (int row=0; row<3; row++) {
            for (int col=0; col<9; col++) {
                System.out.print("arrprint: "+colWiseThreeToTwoCombNumbers[col][row]+"  ");
            }
            System.out.println();
        }
    }

    private void swapNum(int col, int row) {
        int temp = colWiseThreeToTwoCombNumbers[col][row];
        colWiseThreeToTwoCombNumbers[col][row] = colWiseThreeToTwoCombNumbers[col][row+1];
        colWiseThreeToTwoCombNumbers[col][row+1] = temp;
    }

    private void printArray(int[] arr) {
        for (int i = 0; i <arr.length; i++) {
            System.out.print("arr: "+arr[i]+" ");
        }
        System.out.println();
    }

    private void makeAllColumnAtleastOneNum() {
        int num=1, count=0;

       do {
           num=1;
           getThreeCombination();
           count++;
           for (int row=0; row<3; row++) {
               for (int col=0; col<5; col++) {
                   Log.d(TAG, "combi: "+threeCombination[col][row]+ " num: "+num);
                   if (threeCombination[col][row]==num) {
                       num++;
                   }
               }
           }

           System.out.println("makeallnum: "+num+ "  ");
       } while (num<10);

       Log.d(TAG, "makeallcount: "+count);
    }

    private void generateTicket() {
        generateFiveFromNineCombinations();  // it will generate 126 combination to put 5 columns uniquely
//        getThreeCombination();               // select any three combination from 126 combinations
        makeAllColumnAtleastOneNum();          // it ensures that any column of all row should not be empty
        generateColWiseNum();  //
        
        String curDate=getCurrentDate();

        TambolaRoom tambolaRoom=new TambolaRoom();
        tambolaRoom.setChecked(0);
        for (int row=0; row<3; row++) {
            for (int col=0; col<9; col++) {

                if (colWiseThreeToTwoCombNumbers[col][row]!=0) {
                    textView[row * 9 + col].setText(colWiseThreeToTwoCombNumbers[col][row]+"");
                    tambolaRoom.setTicketNum(colWiseThreeToTwoCombNumbers[col][row]);
                    tambolaRoom.setTicketDate(curDate);
                    mDB.tambolaDao().insertTicketNum(tambolaRoom);
                } else {
                    textView[row * 9 + col].setText(" ");
                    tambolaRoom.setTicketNum(colWiseThreeToTwoCombNumbers[col][row]);
                    tambolaRoom.setTicketDate(curDate);
                    mDB.tambolaDao().insertTicketNum(tambolaRoom);
                }
            }
        }

    }

    private String getCurrentDate() {
        Date c = Calendar.getInstance().getTime();
        System.out.println("Current time => " + c);

        SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
        return df.format(c);
    }

    private int generateNumForCol(int j, ArrayList<Integer> randoms) {
//        int num=new Random().nextInt(9)+j*10;
//        return num+"";

        int num;
        do {
            num=new Random().nextInt(9)+j*10;
            Log.d(TAG, "generated random number: "+num);
        } while (randoms.contains(num) || num==0);

        return num;
    }

    private void getThreeCombination() {
        int r;
        ArrayList<Integer> randoms=new ArrayList<>();

        threeCombination=new int[5][3];
        for (int i=0; i<3; i++) {
            r=getUniqueRandomCombination(randoms);
            randoms.add(r);

            for (int j=0; j<5; j++) {
                threeCombination[j][i]=cominations[j][r];
            }
            Log.d(TAG, "combinations: "+threeCombination[0][i]+" "+threeCombination[1][i]+" "+threeCombination[2][i]
                    +" "+threeCombination[3][i]+" "+threeCombination[4][i]);
        }
    }

    private Integer getUniqueRandomCombination(ArrayList<Integer> randoms) {


        do {
            random = new Random().nextInt(126); // no. from 0 to 125
        } while (randoms.contains(random));

        return random;
    }

    private void initView() {
        myGridLayout=findViewById(R.id.ticket_grid);
        ticketNum=findViewById(R.id.ticket_num_txt);
//        recyclerView=findViewById(R.id.winner_rv);
        mainScrollView=findViewById(R.id.main_scrollView);
        adLabel=findViewById(R.id.ad_label);
        adBanner=findViewById(R.id.ad_banner);
    }

    @Override
    public void OnToggled(TicketBoardView v, boolean touchOn) {

        //get the id string
        String idString = v.getIdX() + ":" + v.getIdY();

        Toast.makeText(HousieBoardActivity.this,
                "Toogled:\n" +
                        idString + "\n" +
                        touchOn,
                Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onClick(View view) {
        int tag = Integer.parseInt(view.getTag().toString());
        TicketTextView tv=(TicketTextView) view;
        focusOnView();

        int numOfCol = myGridLayout.getColumnCount();
        int numOfRow = myGridLayout.getRowCount();
        int pos, x, y;
//        x=view.get
        x=tv.getIdX();
        y=tv.getIdY();
        pos=y*numOfCol+x;
//        tv.setText("hello");
        tv.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
        boolean touchOn=tv.touchOn;   // false
      /*  if (!touchOn && !(tv.getText().toString().isEmpty() || tv.getText().toString().equals(" ") || ticketNum.getText().toString().isEmpty())) {
            if (Integer.parseInt(tv.getText().toString())==Integer.parseInt(ticketNum.getText().toString())) {
                tv.setBackgroundColor(Color.RED);
            }
        } else {
            tv.setBackgroundColor(Color.WHITE);
        }*/

      try {

          int ticketNum = mDB.tambolaDao().getTicketNumbers(getCurrentDate()).get(pos).getTicketNum();

          Log.d(TAG, "ticket num: " + ticketNum);
          if (!touchOn && !tv.getText().toString().equals(" ")) {
              tv.setBackgroundColor(Color.GREEN);
              if (ticketNum != 0)
                  mDB.tambolaDao().setChecked(ticketNum, 1);
          } else {
              tv.setBackgroundColor(Color.WHITE);
              if (ticketNum != 0)
                  mDB.tambolaDao().setChecked(ticketNum, 0);
          }
      } catch (Exception e) {
          e.printStackTrace();
      }

        tv.setTouchOn(!touchOn);
//        Toast.makeText(this, "pos: "+pos+" flag: "+!touchOn, Toast.LENGTH_SHORT).show();



        /*for(int yPos=0; yPos<numOfRow; yPos++) {
            for (int xPos = 0; xPos < numOfCol; xPos++) {
                pos = yPos*numOfCol + xPos;
                if (tag==pos) {
                   *//* if ()
                    view.setBackgroundColor(Color.RED);*//*
                    Toast.makeText(this, "position is:"+pos, Toast.LENGTH_SHORT).show();
                    break;
                }
            }
        }*/
    }

    private void generateTwoFromThreeCombinations() {
       /* colWiseThreeToTwoComb=new int[2][3];
        int count=0;
        for (int i=0; i<3; i++) {
            for (int j=i+1; j<2; j++) {
                if (count<3) {
                    colWiseThreeToTwoComb[0][count] = i;
                    colWiseThreeToTwoComb[1][count] = j;
                }
                count++;
            }
            if (count>=3) {
                break;
            }
        }*/
    }

    private void generateFiveFromNineCombinations() {
        int combination = 126, endDigit = 9, count=0;
        cominations = new int[5][126];

        for (int i=1; i<endDigit; i++) {
            for (int j=i+1; j<=endDigit; j++) {
                for (int k=j+1; k<=endDigit; k++) {
                    for (int l=k+1; l<=endDigit; l++) {
                        for (int m=l+1; m<=endDigit; m++) {
                            if (count<combination) {
                                cominations[0][count] = i;
                                cominations[1][count] = j;
                                cominations[2][count] = k;
                                cominations[3][count] = l;
                                cominations[4][count] = m;
                            }
//                            numbers.setText(i+"  "+j+"  "+k+"  "+l+"  "+m+"  "+count);

                            count++;
                        }
                    }
                }
            }

            if (count>=combination) {
                break;
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
//        mDB.close();
    }

    @Override
    public void onSubscriptionSucceeded(String channelName) {
        System.out.println(String.format("[%d] Subscription to channel [%s] succeeded", timestamp(), channelName));
    }

    @Override
    public void onEvent(String channelName, String eventName, String data) {
        try {
            JSONObject object=new JSONObject(data);
            Log.d("pusherTAG", "value: "+object.get("name")+ object.get("message"));
//                    Toast.makeText(HousieBoardActivity.this, "number is: "+object.get("number"), Toast.LENGTH_SHORT).show();

            number=object.get("number").toString();

            Intent in = new Intent("com.example.thecgnow.tambola_number.generated");  // intent filter to identify
            Bundle extras = new Bundle();
            extras.putString("com.example.thecgnow.tambola_number.number", number);

            in.putExtras(extras);
            sendBroadcast(in);
        } catch (JSONException e) {
            e.printStackTrace();
        }

//        Log.d(TAG, "upnumbers: "+numbers.toString());
    }

    @Override
    public void onConnectionStateChange(ConnectionStateChange change) {
        System.out.println(String.format(" Connection state changed from [%s] to [%s]",
                change.getPreviousState(), change.getCurrentState()));
    }

    @Override
    public void onError(final String message, final String code, final Exception e) {
        System.out.println(String.format("[%d] An error was received with message [%s], code [%s], exception [%s]",
                timestamp(), message, code, e));
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
//                Toast.makeText(HousieBoardActivity.this, getResources().getString(R.string.couldnot_connect), Toast.LENGTH_SHORT).show();
                finish();
            }
        });
//        sharedPreferenceConfig.writePusherEventSubscribed(false);
    }

    private long timestamp() {
        return System.currentTimeMillis() - startTime;
    }

}
