package com.newsapp.app.thecgnow2.roomDB.ad;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

@Dao
public interface AdDao {
    @Insert
    public void adPos(AdRoom adRoom);

    @Query("update ads set ad_pos = :pos where ad_section = :section")
    public void setPos(int pos, int section);

    @Query("select * from ads")
    public List<AdRoom> getAdPositions();

    @Query("select distinct ad_pos from ads where ad_section = :section")
    public int getAdPosWithSection(int section);

    @Query("select count(*) from ads where ad_section = :section")
    public int getSizeOfPos(int section);

   /* @Query("SELECT DISTINCT * FROM likes WHERE like_article_id =:articleId")
    public AdRoom getLikeWithArticleId(String articleId);*/

    @Query("DELETE FROM ads WHERE ad_section =:section")
    public void deleteAdPositionWithSection(int section);
}
