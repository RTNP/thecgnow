package com.newsapp.app.thecgnow2.activity.fragments.housieboard;

import android.app.DatePickerDialog;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.newsapp.app.thecgnow2.R;
import com.newsapp.app.thecgnow2.adapter.WinnerListAdapter;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class WinnersFragment extends Fragment implements View.OnClickListener {
    
    LinearLayout selectDate;
    TextView dateTxt;
    Calendar myCalendar;
    Context mCtx;

    RecyclerView recyclerView;
    WinnerListAdapter mAdapter;

    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_winners, container, false);
        
        initView(view);


        selectDate.setOnClickListener(this);
        setUpRecyclerView();
        
        return view;
    }

    private void setUpRecyclerView() {
     /*   mAdapter = new WinnerListAdapter();
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(mCtx);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setAdapter(mAdapter);*/
    }

    private void initView(View view) {
        selectDate=view.findViewById(R.id.select_date);
        dateTxt=view.findViewById(R.id.date_txt);

        recyclerView=view.findViewById(R.id.winner_rv);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.select_date:
                final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear,
                                          int dayOfMonth) {
                        // TODO Auto-generated method stub
                        myCalendar.set(Calendar.YEAR, year);
                        myCalendar.set(Calendar.MONTH, monthOfYear);
                        myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                        String myFormat = "yyyy-MM-dd"; //In which you need put here
                        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
                        dateTxt.setText(sdf.format(myCalendar.getTime()));
                    }

                };
                showDatePicker(date);
                break;
        }
    }


    private void showDatePicker(DatePickerDialog.OnDateSetListener date) {
        myCalendar = Calendar.getInstance();

                new DatePickerDialog(mCtx, date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();

    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        mCtx=context;
    }
}
