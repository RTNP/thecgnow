package com.newsapp.app.thecgnow2.roomDB.category;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "categories")
public class CategoryRoom {
    @PrimaryKey(autoGenerate = true)
    private int id;

    @ColumnInfo(name = "category_id")
    private int catId;

    @ColumnInfo(name = "category_name")
    private String catName;

    @ColumnInfo(name = "category_key_name")
    private String catKeyName;

    @ColumnInfo(name = "category_order")
    private int catOrder;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCatId() {
        return catId;
    }

    public void setCatId(int catId) {
        this.catId = catId;
    }

    public String getCatName() {
        return catName;
    }

    public void setCatName(String catName) {
        this.catName = catName;
    }

    public String getCatKeyName() {
        return catKeyName;
    }

    public void setCatKeyName(String catKeyName) {
        this.catKeyName = catKeyName;
    }

    public int getCatOrder() {
        return catOrder;
    }

    public void setCatOrder(int catOrder) {
        this.catOrder = catOrder;
    }
}
