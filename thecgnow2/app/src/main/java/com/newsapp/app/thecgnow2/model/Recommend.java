package com.newsapp.app.thecgnow2.model;

import java.util.ArrayList;

public class Recommend {
    int type;
    String title;
    ArrayList<Topic> topics;

    public Recommend(int type, String title, ArrayList<Topic> topics) {
        this.type = type;
        this.title = title;
        this.topics = topics;
    }

    public ArrayList<Topic> getTopics() {
        return topics;
    }

    public void setTopics(ArrayList<Topic> topics) {
        this.topics = topics;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }
}
