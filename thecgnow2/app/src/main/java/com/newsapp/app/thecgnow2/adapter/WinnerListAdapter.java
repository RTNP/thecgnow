package com.newsapp.app.thecgnow2.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.newsapp.app.thecgnow2.R;
import com.newsapp.app.thecgnow2.model.Winner;

import java.util.ArrayList;

public class WinnerListAdapter extends RecyclerView.Adapter<WinnerListAdapter.viewHolderOne> {

    ArrayList<Winner> winners;
    Context mCtx;
    String TAG = "WinnerListAdapter";

    public WinnerListAdapter(ArrayList<Winner> winnerList, Context mCtx) {
        this.winners = winnerList;
        this.mCtx = mCtx;
    }

    @NonNull
    @Override
    public viewHolderOne onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView;

        itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.winner_list_item, parent, false);

        return new viewHolderOne(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull viewHolderOne holder, int position) {
        Winner winner = winners.get(position);
        holder.name.setText(winner.getName());
        Log.d(TAG, "prize="+winner.getPrize());
        switch (winner.getPrize()) {
            case 1:
                holder.prize.setImageResource(R.drawable.first_prize);
//                Log.d(TAG, "prize1="+winner.getPrize());
                break;
            case 2:
                holder.prize.setImageResource(R.drawable.second_prize);
                break;
            case 3:
                holder.prize.setImageResource(R.drawable.third_prize);
                break;
                default:
//                    Log.d(TAG, "prizedef="+winner.getPrize());
                    holder.prize.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public int getItemCount() {
        return winners.size();
    }

    public class viewHolderOne extends RecyclerView.ViewHolder {

        TextView name;
        ImageView prize;

        public viewHolderOne(@NonNull View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.winner_name);
            prize = itemView.findViewById(R.id.prize);
        }
    }
}
