package com.newsapp.app.thecgnow2.activity.fragments;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.android.volley.AuthFailureError;
import com.android.volley.Cache;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.newsapp.app.thecgnow2.Config;
import com.newsapp.app.thecgnow2.R;
import com.newsapp.app.thecgnow2.SharedPreferenceConfig;
import com.newsapp.app.thecgnow2.activity.fragments.housie.HowToPlayFragment;
import com.newsapp.app.thecgnow2.activity.fragments.housie.PlayFragment;
import com.newsapp.app.thecgnow2.activity.fragments.housie.WinnersFragment;
import com.newsapp.app.thecgnow2.activity.fragments.housieboard.BoardFragment;
import com.newsapp.app.thecgnow2.activity.ui.housie.SectionsPagerAdapter;
import com.newsapp.app.thecgnow2.adapter.JobListAdapter;
import com.newsapp.app.thecgnow2.model.JobList;
import com.newsapp.app.thecgnow2.model.NewsCategory;
import com.google.android.material.tabs.TabLayout;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class PlayGameFragment extends Fragment {

    public final String TAG = "PlayGameFragment";
//    TextView numbers;
    RequestQueue requestQueue;
    SharedPreferenceConfig sharedPreferenceConfig;
    public static ViewPager viewPager;
    public static SectionsPagerAdapter sectionsPagerAdapter;
    static TabLayout tabs;

    private static final String ARG_NAME = "arg_name";

    public static PlayGameFragment newInstance(String name) {
        Bundle bundle = new Bundle();
        bundle.putString(ARG_NAME, name);
        PlayGameFragment dashboardFragment = new PlayGameFragment();
        dashboardFragment.setArguments(bundle);
        return dashboardFragment;
    }

    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view= inflater.inflate(R.layout.fragment_play_game, container, false);

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        requestQueue= Volley.newRequestQueue(getContext());
        sharedPreferenceConfig=new SharedPreferenceConfig(getContext());

        SectionsPagerAdapter.fragments =new ArrayList<>();
        SectionsPagerAdapter.fragments.add(new HowToPlayFragment());
        if(sharedPreferenceConfig.readTicketGenerated()){
            SectionsPagerAdapter.fragments.add(new BoardFragment());
        }else {
            SectionsPagerAdapter.fragments.add(new PlayFragment());
        }
        SectionsPagerAdapter.fragments.add(new WinnersFragment());

        sectionsPagerAdapter = new SectionsPagerAdapter(getActivity(), getChildFragmentManager());
        viewPager = view.findViewById(R.id.view_pager);
        viewPager.setAdapter(sectionsPagerAdapter);
        viewPager.setCurrentItem(1);
        tabs = view.findViewById(R.id.tabs);
        tabs.setupWithViewPager(viewPager);
    }

    static public void removeTab(int position) {
        if (tabs.getTabCount() >= 1 && position<tabs.getTabCount()) {
            tabs.removeTabAt(position);
            sectionsPagerAdapter.removeTabPage(position);
        }
    }

    static public void addTab(Fragment nFrag, String title) {
        tabs.addTab(tabs.newTab().setText(title));
        sectionsPagerAdapter.addTabPage(nFrag);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
}
