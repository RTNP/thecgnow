package com.newsapp.app.thecgnow2.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.os.AsyncTask;
import android.os.Bundle;

import com.android.volley.AuthFailureError;
import com.android.volley.Cache;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.bottomnavigation.LabelVisibilityMode;
import com.newsapp.app.thecgnow2.AppUpdateChecker;
import com.newsapp.app.thecgnow2.Config;
import com.newsapp.app.thecgnow2.LocaleHelper;
import com.newsapp.app.thecgnow2.R;
import com.newsapp.app.thecgnow2.SharedPreferenceConfig;
import com.newsapp.app.thecgnow2.TheCGNowApp;
import com.newsapp.app.thecgnow2.activity.fragments.GKFragment;
import com.newsapp.app.thecgnow2.activity.fragments.HomeFragment;
import com.newsapp.app.thecgnow2.activity.fragments.MyCityFragment;
import com.newsapp.app.thecgnow2.activity.fragments.PlayGameFragment;
import com.newsapp.app.thecgnow2.activity.fragments.housieboard.ComingSoonFragment;
import com.newsapp.app.thecgnow2.model.Advertize;

import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.view.GravityCompat;
import androidx.appcompat.app.ActionBarDrawerToggle;

import android.view.MenuItem;

import com.google.android.material.navigation.NavigationView;
import com.newsapp.app.thecgnow2.modification.AppFragmentPageAdapter;

import androidx.drawerlayout.widget.DrawerLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.viewpager.widget.ViewPager;

import android.view.Menu;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private static final String TAG = "MainActivity";
    public static BottomNavigationView bottomBar;
    public static TheCGNowApp publicAds;
    public static boolean isActive;

    SharedPreferenceConfig sharedPreferenceConfig;
    RequestQueue requestQueue;

    BroadcastReceiver broadcastReceiver;
    boolean doubleBackToExitPressedOnce = false;
    String selectedFragment="";

    public static ViewPager fragment_container;

    AppFragmentPageAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        loadLocale();

        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.home);
        fragment_container = findViewById(R.id.fragment_container);

        SharedPreferences.OnSharedPreferenceChangeListener listener = new SharedPreferences.OnSharedPreferenceChangeListener() {
            @Override
            public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String s) {
                Log.e("MainActivity",s);
            }
        };
        SharedPreferences sharedPreferences = this.getSharedPreferences(this.getResources().getString(R.string.login_pref),Context.MODE_PRIVATE);
        sharedPreferences.registerOnSharedPreferenceChangeListener(listener);

        adapter = new AppFragmentPageAdapter(getSupportFragmentManager());
        adapter.addFragment(HomeFragment.newInstance("News"),0);
        adapter.addFragment(MyCityFragment.newInstance("MyCity"),1);
        adapter.addFragment(GKFragment.newInstance("Current Affairs"),2);
        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject(sharedPreferenceConfig.readResponseData(Config.tambolaKey));
            isActive = jsonObject.getBoolean("data");
            sharedPreferenceConfig.writeLocationIsActive(isActive);
            if(isActive) {
                adapter.addFragment(PlayGameFragment.newInstance(""),3);
                selectedFragment="PlayGame";
            } else {
                adapter.addFragment(ComingSoonFragment.newInstance(""),3);
                selectedFragment="ComingSoon";
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        fragment_container.setAdapter(adapter);
        fragment_container.setOffscreenPageLimit(adapter.getCount() - 1);

        LinearLayout toolbarLinearLayout = findViewById(R.id.toolbar_ll);
        toolbarLinearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, SearchArticleActivity.class));
            }
        });

        bottomBar=findViewById(R.id.bottomBar);
        bottomBar.setItemIconTintList(null);

        initView();

        try {
            if (ContextCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED ) {
                ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, 101);
            }
        } catch (Exception e){
            e.printStackTrace();
        }

        checkForNewUpdate();

        requestQueue= Volley.newRequestQueue(this);
        publicAds=(TheCGNowApp)getApplicationContext();
        publicAds.initContext(getApplicationContext());

        Log.d(TAG, "user token:"+sharedPreferenceConfig.readFcmToken());

        registerBroadcastReceiver();

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);
    }


    private void registerBroadcastReceiver() {
        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                int notiTask = intent.getExtras().getInt(Config.NOTIFICATION_KEY_STRING, 0);

                if (notiTask !=0) {
                    setNotificationBadgeCount(sharedPreferenceConfig.readNotificationCount());
                }
            }
        };
        registerReceiver(broadcastReceiver, new IntentFilter(Config.NOTIFICATION_RECEIVED));
    }


    private void checkForNewUpdate() {
        AppUpdateChecker appUpdateChecker=new AppUpdateChecker(this);  //pass the activity in constructor
        appUpdateChecker.checkForUpdate(false); //mannual check false here
    }

    private void initView() {
        bottomBar.setLabelVisibilityMode(LabelVisibilityMode.LABEL_VISIBILITY_LABELED);
        bottomBar.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                /*Bottom Bar menu*/
                int id = item.getItemId();
                switch (id) {
                    case R.id.navigation_home:
                        fragment_container.setCurrentItem(0);
                        return true;
                    case R.id.navigation_mycity:
                        fragment_container.setCurrentItem(1);
                        return true;
                    case R.id.navigation_current_affairs:
                        fragment_container.setCurrentItem(2);
                        return true;
                    case R.id.navigation_housie:
                        JSONObject jsonObject = null;
                        try {
                            jsonObject = new JSONObject(sharedPreferenceConfig.readResponseData(Config.tambolaKey));
                            isActive = jsonObject.getBoolean("data");
                            sharedPreferenceConfig.writeLocationIsActive(isActive);
                            if(isActive && !selectedFragment.equals("PlayGame"))
                            {
                                adapter.removeFragment(ComingSoonFragment.newInstance(""),3);
                                adapter.addFragment(PlayGameFragment.newInstance(""),3);
                                adapter.notifyDataSetChanged();
                            } else if (!isActive && !selectedFragment.equals("ComingSoon")) {
                                adapter.addFragment(ComingSoonFragment.newInstance(""),3);
                                adapter.removeFragment(PlayGameFragment.newInstance(""),3);
                                adapter.notifyDataSetChanged();
                            }
                            fragment_container.setCurrentItem(3);
                        } catch (JSONException e) {
                            e.printStackTrace();
                            fragment_container.setCurrentItem(3);
                        }
                        return true;
                }
                return false;
            }
        });
    }

    private void setDoubleBackClick() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }

        fragment_container.setCurrentItem(0);
        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce=false;
            }
        }, 2000);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
//        return true;
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_notification) {
            startActivity(new Intent(MainActivity.this, NotificationActivity.class));
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.nav_lang) {
            showChangeLangDialog();
        } else if (id == R.id.nav_category) {
            startActivity(new Intent(this, AddCategoryActivity.class));
        } else if (id == R.id.nav_share) {
            Toast.makeText(getApplicationContext(),"Share Application",Toast.LENGTH_LONG).show();
        } else if (id == R.id.nav_send) {
            Toast.makeText(getApplicationContext(),"Share Application",Toast.LENGTH_LONG).show();
        }
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void showChangeLangDialog() {
        final String[] listItems = {"हिंदी", "English"};
        AlertDialog.Builder mBuilder = new AlertDialog.Builder(this);
        mBuilder.setTitle("Choose Language...");
        mBuilder.setSingleChoiceItems(listItems, -1, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if (i==0) {
                    setLocale("hi");
                } else {
                    setLocale("en");
                }

                Toast.makeText(MainActivity.this, "changes will be made after restarting app", Toast.LENGTH_SHORT).show();
                dialogInterface.dismiss();
            }
        });

        AlertDialog mDialog = mBuilder.create();
        mDialog.show();
    }

    private void setLocale(String lang) {
        Locale locale = new Locale(lang);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale=locale;
        getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
        sharedPreferenceConfig=new SharedPreferenceConfig(this);
        sharedPreferenceConfig.writeDefaultLang(lang);
    }

    public void loadLocale() {
        sharedPreferenceConfig=new SharedPreferenceConfig(this);
        String lang = sharedPreferenceConfig.readDefaultLang();
        Log.d(TAG, "lang="+lang);
        LocaleHelper.setLocale(MainActivity.this, lang);
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(LocaleHelper.onAttach(base, "hi"));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(broadcastReceiver);
    }

    public static void setNotificationBadgeCount(int count) {

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            setDoubleBackClick();
        }

    }

}
