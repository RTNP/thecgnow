package com.newsapp.app.thecgnow2.activity.fragments.housie;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.newsapp.app.thecgnow2.R;
import com.newsapp.app.thecgnow2.SharedPreferenceConfig;
import com.newsapp.app.thecgnow2.activity.HousieBoardActivity;
import com.newsapp.app.thecgnow2.activity.MainActivity;
import com.newsapp.app.thecgnow2.activity.fragments.PlayGameFragment;
import com.newsapp.app.thecgnow2.activity.fragments.housieboard.BoardFragment;
import com.newsapp.app.thecgnow2.activity.ui.housie.SectionsPagerAdapter;

import java.util.ArrayList;

public class PlayFragment extends Fragment {

    ImageView play;
    Context mCtx;
    SharedPreferenceConfig sharedPreferenceConfig;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_play, container, false);

        initView(view);
        sharedPreferenceConfig=new SharedPreferenceConfig(mCtx);

        play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                if (Integer.parseInt(sharedPreferenceConfig.readPlaySelRegion())!=0) {
//                    if (Integer.parseInt(sharedPreferenceConfig.readPlaySelRegion())==9) {
//                        sharedPreferenceConfig.writePlaySelRegion("0");
                        sharedPreferenceConfig.writeTicketGenerated(true);
                        PlayGameFragment.removeTab(0);
                        PlayGameFragment.removeTab(1);
                        PlayGameFragment.removeTab(2);
                        PlayGameFragment.addTab(new HowToPlayFragment(),getResources().getString(R.string.tab_housie_txt1));
                        PlayGameFragment.addTab(new BoardFragment(),getResources().getString(R.string.tab_housie_txt2));
                        PlayGameFragment.addTab(new WinnersFragment(),getResources().getString(R.string.tab_housie_txt3));
                        PlayGameFragment.viewPager.setCurrentItem(1);
//                        startActivity(new Intent(mCtx, HousieBoardActivity.class));
                   /* } else {
                        Toast.makeText(mCtx, mCtx.getResources().getString(R.string.housie_coming_soon_for_your_region), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(mCtx, mCtx.getResources().getString(R.string.select_atleast_one_region), Toast.LENGTH_SHORT).show();
                }*/
            }
        });

        return view;
    }

    private void initView(View view) {
        play=view.findViewById(R.id.play_button);
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        mCtx=context;
    }
}
