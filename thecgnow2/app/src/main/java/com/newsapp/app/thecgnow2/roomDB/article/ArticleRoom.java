package com.newsapp.app.thecgnow2.roomDB.article;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "articles")
public class ArticleRoom {
    @PrimaryKey(autoGenerate = true)
    private int id;

    @ColumnInfo(name = "article_source_name")
    private String sourceName;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSourceName() {
        return sourceName;
    }

    public void setSourceName(String sourceName) {
        this.sourceName = sourceName;
    }
}
