package com.newsapp.app.thecgnow2.activity.fragments.home;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;


import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.android.volley.AuthFailureError;
import com.android.volley.Cache;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.newsapp.app.thecgnow2.Config;
import com.newsapp.app.thecgnow2.R;
import com.newsapp.app.thecgnow2.adapter.ArticleHomeAdapter;
import com.newsapp.app.thecgnow2.model.Advertize;
import com.newsapp.app.thecgnow2.model.Article;
import com.newsapp.app.thecgnow2.model.Topic;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import static com.newsapp.app.thecgnow2.activity.MainActivity.publicAds;

public class ForYouFragment extends Fragment {

    private ArrayList<Article> articles = new ArrayList<>();
    private ArrayList<Topic> topics = new ArrayList<>();
    private RecyclerView recyclerView;
    private ArticleHomeAdapter mAdapter;
    private RequestQueue requestQueue;
    SwipeRefreshLayout pullToRefresh;
    TextView updateMsg;
    ProgressBar progressBar;
    ArrayList<Integer> adPositions=new ArrayList<>();    // it defines the positions of ads in recyclerview used for identify and refresh all ads
    private View currentFocusedLayout, oldFocusedLayout;

    Context mCtx;
    public static final String TAG="ForYouFragment";

    BroadcastReceiver minuteUpdateReceiver, broadcastReceiver;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view= inflater.inflate(R.layout.fragment_home_all_categories, container, false);
        /*TextView textView=view.findViewById(R.id.section_label_home);
        textView.setText("For You Fragment");*/

        initView(view);
        updateMsg.setVisibility(View.GONE);

        requestQueue=Volley.newRequestQueue(mCtx);
     /*  WebView wv1=view.findViewById(R.id.test_wv);
        wv1.getSettings().setLoadsImagesAutomatically(true);
        wv1.getSettings().setJavaScriptEnabled(true);
        wv1.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
        wv1.loadUrl("www.iotacademycg.com");*/

//        mAdapter = new ArticleHomeAdapter(articles, advertizes, mCtx, getActivity().getSupportFragmentManager());
        recyclerView.setHasFixedSize(true);
//        recyclerView=view.findViewById(R.id.batch_rv);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(mCtx);
        recyclerView.setLayoutManager(mLayoutManager);

        checkAdVisibility();
//        setHandlerForAdRefresh();
        registerBroadcastReceiver();

        requestQueue = Volley.newRequestQueue(getActivity());

        addTopics();
//        fetchData();
        recyclerView.setAdapter(mAdapter);

        pullToRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                //fetchData();
            }
        });

 /*       ConnectivityManager cm = (ConnectivityManager) mCtx.getSystemService(Context.CONNECTIVITY_SERVICE);

        Log.d(TAG, "network status: "+ (cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isConnected()));
*/
        return view;
    }



    private void registerBroadcastReceiver() {
        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                refreshAds();
            }
        };
        mCtx.registerReceiver(broadcastReceiver, new IntentFilter("com.example.thecgnow.activity.adRefresh"));
    }


    public void refreshAds() {
        String url=publicAds.getAdvertizes().get(publicAds.adBannerItemPosition).getMediaUrl();
        String adUrl=publicAds.getAdvertizes().get(publicAds.adBannerItemPosition).getAdUrl();
        Log.d(TAG, "ad refreshing adpos: "+publicAds.adBannerItemPosition+" url: "+url+" adpos:"+adPositions.size());
        for (int i:adPositions) {
            try {
//                articles.set(i, new Article("", "title changed", url, "", adUrl,  Config.ARTICLE_ITEM_AD));
                mAdapter.notifyItemChanged(i);
//                        Log.d(TAG, "broadcast item changed: "+i);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void checkAdVisibility() {
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

                if (newState == AbsListView.OnScrollListener.SCROLL_STATE_IDLE) {
                    //get the recyclerview position which is completely visible and first
                    int positionView = ((LinearLayoutManager) recyclerView.getLayoutManager()).findFirstCompletelyVisibleItemPosition();
                    Log.i("VISISBLE", positionView + "");
                    if (positionView >= 0) {
                        if (oldFocusedLayout != null) {
                            //Stop the previous video playback after new scroll
//                            VideoView vv_dashboard = (VideoView) oldFocusedLayout.findViewById(R.id.vv_dashboard);
//                            vv_dashboard.stopPlayback();

                        }


                        currentFocusedLayout = ((LinearLayoutManager) recyclerView.getLayoutManager()).findViewByPosition(positionView);
                      /*  VideoView vv_dashboard = (VideoView) currentFocusedLayout.findViewById(R.id.vv_dashboard);
                        //to play video of selected recylerview, videosData is an array-list which is send to recyclerview adapter to fill the view. Here we getting that specific video which is displayed through recyclerview.
                        playVideo(videosData.get(positionView));*/
//                      Log.d(TAG, "view compare: "+oldFocusedLayout.getId()+" - "+currentFocusedLayout.getId());
                        Log.d(TAG, "currentpos: "+positionView);
                      if (adPositions.contains(positionView) && oldFocusedLayout!=currentFocusedLayout) {
                          Config.AD_VISIBILITY_COUNTER++;
                          Log.d(TAG, "ad viewed count: " + Config.AD_VISIBILITY_COUNTER);
                      }
                        oldFocusedLayout = currentFocusedLayout;
                    }
                }
            }
        });
    }

    private void addTopics() {
        topics.add(new Topic(0, "1 News source1"));
        topics.add(new Topic(0, "2 News source2"));
        topics.add(new Topic(0, "3 News source3"));
        topics.add(new Topic(0, "4 News source4"));
        topics.add(new Topic(0, "5 News source5"));
    }

    public void addAd(String title, String url, String adUrl, int adType, int index) {
        switch (adType) {
            case Config.ARTICLE_ITEM_AD:
//                articles.add(index, new Article("", title, url, "",adUrl, adType));
                break;
            case Config.ARTICLE_ITEM_AD_VIDEO:
//                articles.add(index, new Article("", title, "", url,adUrl, adType));
                break;
            case Config.ARTICLE_ITEM_AD_GIF:
//                articles.add(index, new Article("", title, url, "",adUrl, adType));
                break;
        }
    }

    private void fetchData() {
        //http://www.iotacademycg.com/cadApp/head-office/uploads/assignments/Screenshot_20190921_123553.jpg
     /*   articles.add(new Article(1,"title1",
                "http://www.iotacademycg.com/cadApp/head-office/uploads/assignments/Screenshot_20190921_123553.jpg",
                "", Config.ARTICLE_ITEM_IMAGE));*/
      /*  articles.add(new Article(2,"title1",
                "http://www.iotacademycg.com/cadApp/head-office/uploads/assignments/Screenshot_20190921_123553.jpg",
                "", Config.ARTICLE_ITEM_IMAGE));*/
//        articles.add(new Article(3, "Advertise", "", "", Config.ARTICLE_ITEM_AD));
       /* articles.add(new Article(4,"title1",
                "",
                "https://github.com/danylovolokh/VideoPlayerManager/raw/master/app/src/main/assets/video_sample_1.mp4", Config.ARTICLE_ITEM_VIDEO));
//        articles.add(new Article(5, "Topics you should follow", "", "", Config.ARTICLE_ITEM_SUGGEST_TOPIC));
        articles.add(new Article(6, "Advertise", "",
                "https://github.com/danylovolokh/VideoPlayerManager/raw/master/app/src/main/assets/video_sample_1.mp4",
                Config.ARTICLE_ITEM_AD_VIDEO));*/

//        final ProgressDialog loading = ProgressDialog.show(mCtx, "Fetching data", "Please wait...", false, false);

//        progressBar.setVisibility(View.VISIBLE);
        Log.d(TAG, "url:"+Config.WEB_HOSE_API_URL);
        articles.clear();
        updateMsg.setVisibility(View.VISIBLE);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Config.WEB_HOSE_API_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        pullToRefresh.setRefreshing(false);
//                        loading.dismiss();
//                        progressBar.setVisibility(View.GONE);
                        updateMsg.setVisibility(View.GONE);
                        Log.d("batchlist_response",response);
//                        Toast.makeText(AttendenceActivity.this, "attendance response"+response, Toast.LENGTH_SHORT).show();
                        try {
                            //Creating the json object from the response
                            JSONObject jsonResponse = new JSONObject(response);
                            JSONArray jsonArray = jsonResponse.getJSONArray("articles");
                            Log.d("jsondatabatch",jsonArray.toString());
//                            Toast.makeText(AttendenceActivity.this, ""+jsonArray.toString(), Toast.LENGTH_SHORT).show();

                            String sourceVal, articleId;
                            int random = new Random().nextInt(5) + 10;
                            for (int i=0; i<jsonArray.length(); i++) {
                                JSONObject articleJson =jsonArray.getJSONObject(i);

                                //http://www.iotacademycg.com/cadApp/head-office/uploads/assignments/Screenshot_20190921_123553.jpg
                             /* if (i%6==0 && i!=0) {
                                  Advertize advertize=publicAds.getAdvertizes().get(0);
                                  if (advertize.getMediaType().equals("BIMG")) {
                                      addAd("Advertise",
                                              advertize.getMediaUrl(),
                                              advertize.getAdUrl(),
                                              Config.ARTICLE_ITEM_AD);
                                  }
                                  adPositions.add(articles.size()-1);
                              }*/

                              articleId =  articleJson.getJSONObject("source").getString("name")
                                      + articleJson.getString("publishedAt");
                                sourceVal = articleJson.getJSONObject("source").getString("name");

                                articles.add(new Article(articleId,
                                        articleJson.getString("title"),
                                        articleJson.getString("urlToImage"),
                                        "",
                                        sourceVal,
                                        "img",
                                        articleJson.getString("description"),
                                        articleJson.getString("content"),
                                        articleJson.getString("url"),
                                        articleJson.getString("publishedAt"),
                                        Config.ARTICLE_ITEM_IMAGE));
//                                articles.add(new Article(i,
//                                        jresponse.getString("batch_code")))

                            }

                            showAdPosValues();
                            Collections.shuffle(articles);
                            adPositions.clear();
                            for (int i=0; i<articles.size(); i++) {
                                if (i%6==0 && i!=0) {
                                    Advertize advertize=publicAds.getAdvertizes().get(0);
                                    if (advertize.getMediaType().equals("BIMG")) {
                                        addAd("Advertise",
                                                advertize.getMediaUrl(),
                                                advertize.getAdUrl(),
                                                Config.ARTICLE_ITEM_AD,
                                                i);
                                    }

                                    adPositions.add(i);
                                    Log.d(TAG, "adposval: "+adPositions.get(adPositions.size()-1));
                                    Log.d(TAG, "size: "+articles.size()+" sizeadpos: "+adPositions.size()+" i="+i);
                                }

                                if (i%random==0 && i!=0) {
                                    articles.add(i, new Article("", Config.SUGGEST_CIRCLE, "Topics you should follow", "", topics));
                                    random = new Random().nextInt(5) + 10; // [0, 5] + 20 => [10, 15]
                                }
                            }
                            mAdapter.notifyDataSetChanged();
//                            jsonResponse.getString(Config.TAG_RESPONSE).equalsIgnoreCase("Success");

                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.d("mobileerror",""+e);
//                            Toast.makeText(getActivity(), ""+e.toString(), Toast.LENGTH_SHORT).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        pullToRefresh.setRefreshing(false);
                        updateMsg.setVisibility(View.GONE);
//                        loading.dismiss();
//                        progressBar.setVisibility(View.GONE);
                        Toast.makeText(mCtx, mCtx.getResources().getString(R.string.couldnot_connect),Toast.LENGTH_LONG).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                //Adding the parameters to the request
//                params.put("batch_code", codeVal);
                return params;
            }

            @Override
            protected Response<String> parseNetworkResponse(NetworkResponse response) {
                try {
                    Cache.Entry cacheEntry = HttpHeaderParser.parseCacheHeaders(response);
                    if (cacheEntry == null) {
                        cacheEntry = new Cache.Entry();
                    }
                    final long cacheHitButRefreshed = 10 * 60 * 1000; // in 10 minutes cache will be hit, but also refreshed on background
                    final long cacheExpired = 24 * 60 * 60 * 1000; // in 24 hours this cache entry expires completely
                    long now = System.currentTimeMillis();
                    final long softExpire = now + cacheHitButRefreshed;
                    final long ttl = now + cacheExpired;
                    cacheEntry.data = response.data;
                    cacheEntry.softTtl = softExpire;
                    cacheEntry.ttl = ttl;
                    String headerValue;
                    headerValue = response.headers.get("Date");
                    if (headerValue != null) {
                        cacheEntry.serverDate = HttpHeaderParser.parseDateAsEpoch(headerValue);
                    }
                    headerValue = response.headers.get("Last-Modified");
                    if (headerValue != null) {
                        cacheEntry.lastModified = HttpHeaderParser.parseDateAsEpoch(headerValue);
                    }
                    cacheEntry.responseHeaders = response.headers;
                    final String jsonString = new String(response.data,
                            HttpHeaderParser.parseCharset(response.headers));
                    return Response.success(jsonString, cacheEntry);
                } catch (Exception e) {
                    return Response.error(new ParseError(e));
                }
            }
        };

        //Adding request the the queue
        requestQueue.add(stringRequest);
        //http://videocdn.bodybuilding.com/video/mp4/62000/62792m.mp4
    }

    private void showAdPosValues() {
        for (int i:adPositions) {
            Log.d(TAG, "adpos: "+i);
        }
    }

    private void startMinuteUpdater() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(Intent.ACTION_TIME_TICK);
        minuteUpdateReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                Config.TIME_COUNTER++;
                String url="http://www.iotacademycg.com/cadApp/head-office/uploads/assignments/Screenshot_20190921_123553.jpg";
                for (int i:adPositions) {
                    try {
//                        articles.set(i, new Article("", "title changed", url, "", Config.ARTICLE_ITEM_AD));
                        mAdapter.notifyItemChanged(i);
                        Log.d(TAG, "broadcast item changed: "+i);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        };

        mCtx.registerReceiver(minuteUpdateReceiver, intentFilter);
    }

    private void initView(View view) {
        recyclerView=view.findViewById(R.id.home_foryou_rv);
        pullToRefresh=view.findViewById(R.id.pullToRefreshHome);
        updateMsg=view.findViewById(R.id.update_txt);
        progressBar=view.findViewById(R.id.progress_circular);
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        mCtx=context;
    }

    @Override
    public void onResume() {
        super.onResume();
//        startMinuteUpdater();
    }

    @Override
    public void onPause() {
        super.onPause();
//        mCtx.unregisterReceiver(minuteUpdateReceiver);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mCtx.unregisterReceiver(broadcastReceiver);
    }
}
