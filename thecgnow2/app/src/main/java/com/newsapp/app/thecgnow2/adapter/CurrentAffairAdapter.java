package com.newsapp.app.thecgnow2.adapter;

import android.content.Context;
import android.content.Intent;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.newsapp.app.thecgnow2.R;
import com.newsapp.app.thecgnow2.activity.GKDescriptionActivity;
import com.newsapp.app.thecgnow2.model.GK;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class CurrentAffairAdapter extends RecyclerView.Adapter {

    ArrayList<GK> gks;
    Context mCtx;

    public CurrentAffairAdapter(ArrayList<GK> gks, Context mCtx) {
        this.gks = gks;
        this.mCtx = mCtx;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView;

        itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.current_affair_list_item, parent, false);

        return new ViewHolderOne(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        GK gk=gks.get(position);
        try {
            ((ViewHolderOne) holder).setViews(gk);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return gks.size();
    }

    public class ViewHolderOne extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView title, description;
        ImageView img;
        LinearLayout linearLayout;

        public ViewHolderOne(@NonNull View itemView) {
            super(itemView);
            title=itemView.findViewById(R.id.title);
            img=itemView.findViewById(R.id.img);
            description=itemView.findViewById(R.id.description);
            linearLayout=itemView.findViewById(R.id.ll);
        }

        public void setViews(GK gk) {
            title.setText(gk.getTitle());
            Picasso.get().load(gk.getImageUrl()).placeholder(R.drawable.placeholder).into(img);
            description.setText(Html.fromHtml(gk.getDescription()));
            linearLayout.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (view.getId()==R.id.ll) {
                Gson gson = new Gson();
                String gkArrays=gson.toJson(gks.get(getAdapterPosition()));
                Intent intent=new Intent(mCtx, GKDescriptionActivity.class);
                intent.putExtra("gk", gkArrays);
                mCtx.startActivity(intent);
            }
        }
    }
}
