package com.newsapp.app.thecgnow2.roomDB;

import androidx.room.Database;
import androidx.room.RoomDatabase;

import com.newsapp.app.thecgnow2.roomDB.ad.AdDao;
import com.newsapp.app.thecgnow2.roomDB.ad.AdRoom;
import com.newsapp.app.thecgnow2.roomDB.article.ArticleDao;
import com.newsapp.app.thecgnow2.roomDB.article.ArticleRoom;
import com.newsapp.app.thecgnow2.roomDB.category.CategoryDao;
import com.newsapp.app.thecgnow2.roomDB.category.CategoryRoom;
import com.newsapp.app.thecgnow2.roomDB.follow.FollowDao;
import com.newsapp.app.thecgnow2.roomDB.follow.FollowRoom;
import com.newsapp.app.thecgnow2.roomDB.like.LikeDao;
import com.newsapp.app.thecgnow2.roomDB.like.LikeRoom;
import com.newsapp.app.thecgnow2.roomDB.notification.NotificationDao;
import com.newsapp.app.thecgnow2.roomDB.notification.NotificationRoom;
import com.newsapp.app.thecgnow2.roomDB.tambola.NumberDao;
import com.newsapp.app.thecgnow2.roomDB.tambola.NumberRoom;
import com.newsapp.app.thecgnow2.roomDB.tambola.TambolaDao;
import com.newsapp.app.thecgnow2.roomDB.tambola.TambolaRoom;

@Database(entities = {ArticleRoom.class, FollowRoom.class, LikeRoom.class, CategoryRoom.class, TambolaRoom.class, AdRoom.class, NumberRoom.class, NotificationRoom.class}, version = 1, exportSchema = false)
public abstract class CGNowDB extends RoomDatabase {
    public abstract ArticleDao articleDao();
    public abstract FollowDao followDao();
    public abstract LikeDao likeDao();
    public abstract CategoryDao categoryDao();
    public abstract TambolaDao tambolaDao();
    public abstract AdDao adDao();
    public abstract NumberDao numberDao();
    public abstract NotificationDao notificationDao();
}
