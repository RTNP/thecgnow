package com.newsapp.app.thecgnow2.model;

import com.google.gson.annotations.SerializedName;

public class Notification {
    @SerializedName("id")
    String id;
    @SerializedName("title")
    String title;
    @SerializedName("urlToImage")
    String imageurl;

    public Notification(String id, String title, String imageurl) {
        this.id = id;
        this.title = title;
        this.imageurl = imageurl;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImageurl() {
        return imageurl;
    }

    public void setImageurl(String imageurl) {
        this.imageurl = imageurl;
    }
}
