package com.newsapp.app.thecgnow2.adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.newsapp.app.thecgnow2.R;
import com.newsapp.app.thecgnow2.TheCGNowApp;
import com.newsapp.app.thecgnow2.activity.ArticleDescription;
import com.newsapp.app.thecgnow2.model.Article;
import com.newsapp.app.thecgnow2.roomDB.CGNowDB;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.NotiViewHolder> {

    ArrayList<Article> articles;
    Context mCtx;
    String TAG = "NotificationAdapter";

    public NotificationAdapter(ArrayList<Article> articles, Context mCtx) {
        this.articles = articles;
        this.mCtx = mCtx;
    }

    @NonNull
    @Override
    public NotiViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.notificaiton_item, parent, false);
        return new NotiViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull NotiViewHolder holder, final int position) {
        final Article article = articles.get(position);
        holder.textView.setText(article.getTitle());
        Log.d(TAG, "imgurl="+article.getImg());
        Picasso.get().load(article.getMediaUrl()).into(holder.imageView);
        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Toast.makeText(mCtx, ""+notification.getTitle(), Toast.LENGTH_SHORT).show();
                Gson gson=new Gson();
                String articleArray=gson.toJson(article);
                Intent intent=new Intent(mCtx, ArticleDescription.class);
                intent.putExtra("article",articleArray);
                mCtx.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        Log.d(TAG, "notificationsize="+articles.size());
        return articles.size();
    }

    public class NotiViewHolder extends RecyclerView.ViewHolder {
        ImageView imageView;
        TextView textView;
        CardView cardView;
        public NotiViewHolder(@NonNull View itemView) {
            super(itemView);
            imageView=itemView.findViewById(R.id.image);
            textView=itemView.findViewById(R.id.title);
            cardView=itemView.findViewById(R.id.cv);
        }
    }
}
