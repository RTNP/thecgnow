package com.newsapp.app.thecgnow2.adapter;

import android.util.Log;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;


import com.newsapp.app.thecgnow2.activity.fragments.TabFragment;
import com.newsapp.app.thecgnow2.model.NewsCategory;

import java.util.ArrayList;
import java.util.List;

public class ViewPagerAdapter extends FragmentStatePagerAdapter {
    private final List<NewsCategory> mFragmentTitleList = new ArrayList<>();
    private String TAG="ViewPagerAdapter";

    public ViewPagerAdapter(@NonNull FragmentManager fm) {
        super(fm,BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
    }

    public void addFrag(NewsCategory category) {
        Log.d(TAG, "mftitle: "+ category.getName()+" key: "+category.getKeyName());
        mFragmentTitleList.add(category);
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        int categoryId=1;
        Log.d(TAG, "viewpager query: "+mFragmentTitleList.get(position).getName()+ "key: "+mFragmentTitleList.get(position).getKeyName());
        return TabFragment.newInstance(position + 1,
                mFragmentTitleList.get(position).getId(),
                mFragmentTitleList.get(position).getKeyName());
    }

    @Override
    public int getCount() {
        return mFragmentTitleList.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return mFragmentTitleList.get(position).toString();
    }
}
