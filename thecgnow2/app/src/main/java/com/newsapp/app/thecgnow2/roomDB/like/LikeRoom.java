package com.newsapp.app.thecgnow2.roomDB.like;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "likes")
public class LikeRoom {

    @PrimaryKey(autoGenerate = true)
    private int id;

    @ColumnInfo(name = "like_article_id")
    private String articleId;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getArticleId() {
        return articleId;
    }

    public void setArticleId(String articleId) {
        this.articleId = articleId;
    }
}
