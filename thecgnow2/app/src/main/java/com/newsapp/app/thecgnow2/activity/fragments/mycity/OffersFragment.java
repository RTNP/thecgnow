package com.newsapp.app.thecgnow2.activity.fragments.mycity;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.Cache;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.newsapp.app.thecgnow2.Config;
import com.newsapp.app.thecgnow2.R;
import com.newsapp.app.thecgnow2.SharedPreferenceConfig;
import com.newsapp.app.thecgnow2.activity.MainActivity;
import com.newsapp.app.thecgnow2.activity.fragments.MyCityFragment;
import com.newsapp.app.thecgnow2.adapter.JobAdapter;
import com.newsapp.app.thecgnow2.adapter.JobListAdapter;
import com.newsapp.app.thecgnow2.adapter.OfferAdapter;
import com.newsapp.app.thecgnow2.model.JobList;
import com.newsapp.app.thecgnow2.model.Offer;
import com.newsapp.app.thecgnow2.model.OfferCategory;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class OffersFragment extends Fragment implements AdapterView.OnItemSelectedListener {

    private ArrayList<Offer> offers = new ArrayList<>();
    private RecyclerView recyclerView;
    private OfferAdapter mAdapter;
    private RequestQueue requestQueue;
    ProgressBar progressBar;
    private SharedPreferenceConfig sharedPreferenceConfig;
    Context mCtx;
    private String TAG = "OffersFragment";
    Spinner spinner;
    ArrayAdapter<String> adapter;
    ArrayList<OfferCategory> offerCategories=new ArrayList<>();
    NestedScrollView nestedScrollView;
    boolean isScrolling=false, isLoading=false;
    int page = 0, catId=0, preCatId=0;
    ProgressBar loading_bottom;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view= inflater.inflate(R.layout.fragment_offer, container, false);

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initView(view);
        loading_bottom.setVisibility(View.GONE);
        spinner.setOnItemSelectedListener(this);
        progressBar.setVisibility(View.GONE);

        mAdapter = new OfferAdapter(offers,mCtx);
        Log.d(TAG, "mAdapter initialized1");
//        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(mCtx);
        recyclerView.setLayoutManager(mLayoutManager);

        requestQueue = Volley.newRequestQueue(getActivity());
        sharedPreferenceConfig = new SharedPreferenceConfig(mCtx);

        initScrollListener();
        getOfferCategories();
        Log.d(TAG, "mAdapter initialized2");
        recyclerView.setAdapter(mAdapter);
        Log.d(TAG, "mAdapter initialized3");
    }

    private void initScrollListener() {
        if (nestedScrollView!=null) {
            nestedScrollView.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
                @Override
                public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                    if (scrollY > oldScrollY) {
//                        Log.i(TAG, "Scroll DOWN");
                        isScrolling=true;
                    }
                    if (!isLoading && isScrolling && scrollY >= ( v.getChildAt(v.getChildCount()-1).getMeasuredHeight() - v.getMeasuredHeight() )) {
                        Log.i(TAG, "BOTTOM SCROLL");
                        isLoading=true;
                        isScrolling=false;
                        fetchData(catId);
                    }
                }
            });
        }
    }

    private void getOfferCategories() {

        offerCategories.add(new OfferCategory(0, getResources().getString(R.string.select_offer_category), "All Categories"));
        Gson gson = new Gson();
        Type type = new TypeToken<ArrayList<OfferCategory>>(){}.getType();
        try {
            JSONObject jsonObject = new JSONObject(sharedPreferenceConfig.readResponseData(Config.offerCategoryKey));
            offerCategories.addAll(gson.fromJson(jsonObject.getJSONArray("data").toString(), type));
            Log.d(TAG, "offerCategoryList="+gson.toJson(offerCategories));
            setAdapter();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void setAdapter() {
        adapter = new ArrayAdapter(getActivity(),android.R.layout.simple_list_item_1,offerCategories);
        spinner.setAdapter(adapter);
    }

    private void fetchData(final int offerCatId) {
        if (preCatId!=offerCatId) {
            page=0;
        }

        loading_bottom.setVisibility(View.VISIBLE);

        String get_query = "?latitude="+sharedPreferenceConfig.readLatitude()+"&longitude="+sharedPreferenceConfig.readLongitude()+"&cat_id="+offerCatId+"&page="+page+"&auth_key="+sharedPreferenceConfig.readFcmToken();
        Log.d(TAG, "offerquery: "+Config.GET_OFFER_LIST_URL+get_query);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, Config.GET_OFFER_LIST_URL+get_query,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        Log.d(TAG,"data"+response);
                        progressBar.setVisibility(View.GONE);
                        loading_bottom.setVisibility(View.GONE);

                        offers.clear();
//                        Toast.makeText(AttendenceActivity.this, "attendance response"+response, Toast.LENGTH_SHORT).show();
                        try {
                            //Creating the json object from the response
                            JSONObject jsonResponse = new JSONObject(response);

                            if (offerCatId==0) {
                                sharedPreferenceConfig.writeResponseData(Config.offerListKey, jsonResponse.toString());
                            }

                            preCatId=offerCatId;
                            page=jsonResponse.getInt("page");

                            JSONArray jsonArray = jsonResponse.getJSONArray("data");
                            Log.d(TAG,jsonArray.toString());
//                            Toast.makeText(AttendenceActivity.this, ""+jsonArray.toString(), Toast.LENGTH_SHORT).show();

                            String sourceVal, articleId;
                            for (int i=0; i<jsonArray.length(); i++) {
                                JSONObject j =jsonArray.getJSONObject(i);

                                offers.add(new Offer(j.getInt("offer_id"),
                                        j.getInt("region_id"),
                                        j.getString("title"),
                                        j.getInt("offer_category_id"),
                                        j.getString("offer_image")));
                            }
                            mAdapter.notifyDataSetChanged();
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.d("mobileerror",""+e);
//                            Toast.makeText(getActivity(), ""+e.toString(), Toast.LENGTH_SHORT).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {

                        progressBar.setVisibility(View.GONE);

                        String message = null;
                        if (volleyError instanceof NetworkError) {
                            message = "Cannot connect to Internet...Please check your connection!";
                        } else if (volleyError instanceof ServerError) {
                            message = "The server could not be found. Please try again after some time!!";
                        } else if (volleyError instanceof AuthFailureError) {
                            message = "Cannot connect to Internet...Please check your connection!";
                        } else if (volleyError instanceof ParseError) {
                            message = "Parsing error! Please try again after some time!!";
                        } else if (volleyError instanceof NoConnectionError) {
                            message = "Cannot connect to Internet...Please check your connection!";
                        } else if (volleyError instanceof TimeoutError) {
                            message = "Connection TimeOut! Please check your internet connection.";
                        }

                        if (message!=null) {
                            Toast.makeText(mCtx, mCtx.getResources().getString(R.string.couldnot_connect),Toast.LENGTH_LONG).show();
                        }
//                        Toast.makeText(mCtx, mCtx.getResources().getString(R.string.couldnot_connect),Toast.LENGTH_LONG).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                //Adding the parameters to the request
                params.put("auth_key", sharedPreferenceConfig.readFcmToken());
                params.put("cat_id", offerCatId+"");
                return params;
            }

            /*@Override
            protected Response<String> parseNetworkResponse(NetworkResponse response) {
                try {
                    Cache.Entry cacheEntry = HttpHeaderParser.parseCacheHeaders(response);
                    if (cacheEntry == null) {
                        cacheEntry = new Cache.Entry();
                    }
                    final long cacheHitButRefreshed =  2 * 60 * 1000; // 10 * 60 * 1000 in 10 minutes cache will be hit, but also refreshed on background
                    final long cacheExpired = 24 * 60 * 60 * 1000; // in 24 hours this cache entry expires completely
                    long now = System.currentTimeMillis();
                    final long softExpire = now + cacheHitButRefreshed;
                    final long ttl = now + cacheExpired;
                    cacheEntry.data = response.data;
                    cacheEntry.softTtl = softExpire;
                    cacheEntry.ttl = ttl;
                    String headerValue;
                    headerValue = response.headers.get("Date");
                    if (headerValue != null) {
                        cacheEntry.serverDate = HttpHeaderParser.parseDateAsEpoch(headerValue);
                    }
                    headerValue = response.headers.get("Last-Modified");
                    if (headerValue != null) {
                        cacheEntry.lastModified = HttpHeaderParser.parseDateAsEpoch(headerValue);
                    }
                    cacheEntry.responseHeaders = response.headers;
                    final String jsonString = new String(response.data,
                            HttpHeaderParser.parseCharset(response.headers));
                    return Response.success(jsonString, cacheEntry);
                } catch (Exception e) {
                    return Response.error(new ParseError(e));
                }
            }*/
        };

        //Adding request the the queue
        requestQueue.add(stringRequest);
    }

    private void initView(View view) {
        recyclerView=view.findViewById(R.id.offer_rv);
        spinner=view.findViewById(R.id.spinner);
        progressBar=view.findViewById(R.id.progressBar_cyclic);
        nestedScrollView = view.findViewById(R.id.nested_sv);
        loading_bottom = view.findViewById(R.id.loading_bottom);
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        mCtx=context;
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        Log.d(TAG, "item pos: "+i);
        catId = offerCategories.get(i).getId();
        if (i>0) {
            progressBar.setVisibility(View.VISIBLE);
            fetchData(offerCategories.get(i).getId());
//            Toast.makeText(mCtx, "fetching data for category: "+offerCategories.get(i).getName(), Toast.LENGTH_SHORT).show();
        } else {
            Gson gson = new Gson();
            Type type = new TypeToken<ArrayList<Offer>>(){}.getType();
            try {
                JSONObject jsonObject = new JSONObject(sharedPreferenceConfig.readResponseData(Config.offerListKey));
                offers = gson.fromJson(jsonObject.getJSONArray("data").toString(), type);
                if (offers.size()==0) {
                    fetchData(0);
                    Log.d(TAG, "cat=0, offersize=0");
                }
                Log.d(TAG, "offerlist="+gson.toJson(offers));
                Log.d(TAG, "offersize="+offers.size());

//                mAdapter.notifyDataSetChanged();
                mAdapter = new OfferAdapter(offers, mCtx);
                recyclerView.setAdapter(mAdapter);
                Log.d(TAG, "offersize2="+offers.size());
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
//        inflater.inflate(R.menu.fragment_menu, menu);
//        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        int id = item.getItemId();
        if (id==R.id.action_refresh && MyCityFragment.tabs.getSelectedTabPosition()==1 && MainActivity.bottomBar.getSelectedItemId()==R.id.navigation_mycity) {
            Toast.makeText(mCtx, "offer fragment", Toast.LENGTH_SHORT).show();
            page=0;
            fetchData(catId);
        }

        return super.onOptionsItemSelected(item);
    }
}
