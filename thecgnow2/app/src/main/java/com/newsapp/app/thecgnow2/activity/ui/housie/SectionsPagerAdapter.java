package com.newsapp.app.thecgnow2.activity.ui.housie;

import android.content.Context;
import android.content.SharedPreferences;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.newsapp.app.thecgnow2.R;
import com.newsapp.app.thecgnow2.SharedPreferenceConfig;
import com.newsapp.app.thecgnow2.activity.fragments.housie.HowToPlayFragment;
import com.newsapp.app.thecgnow2.activity.fragments.housie.PlayFragment;
import com.newsapp.app.thecgnow2.activity.fragments.housie.WinnersFragment;
import com.newsapp.app.thecgnow2.activity.fragments.housieboard.BoardFragment;

import org.jetbrains.annotations.TestOnly;

import java.util.ArrayList;

/**
 * A [FragmentPagerAdapter] that returns a fragment corresponding to
 * one of the sections/tabs/pages.
 */
public class SectionsPagerAdapter extends FragmentPagerAdapter implements SharedPreferences.OnSharedPreferenceChangeListener {

    @StringRes
    private static final int[] TAB_TITLES = new int[]{R.string.tab_housie_txt1, R.string.tab_housie_txt2, R.string.tab_housie_txt3};
    private final Context mContext;
    private SharedPreferenceConfig sharedPreferenceConfig;
    boolean myState;
    public static ArrayList<Fragment> fragments;

    public SectionsPagerAdapter(Context context, FragmentManager fm) {
        super(fm,BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
        mContext = context;
        sharedPreferenceConfig = new SharedPreferenceConfig(mContext);
        myState=sharedPreferenceConfig.readTicketGenerated();
    }

    @Override
    public Fragment getItem(int position) {

        if(fragments.get(position)!=null){
            return fragments.get(position);
        }

        return PlaceholderFragment.newInstance(position + 1);
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return mContext.getResources().getString(TAB_TITLES[position]);
    }

    @Override
    public int getCount() {
        // Show 2 total pages.
        return TAB_TITLES.length;
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        if(key==mContext.getResources().getString(R.string.sp_ticket_generate)){
            myState = sharedPreferenceConfig.readTicketGenerated();
        }
    }

    public void removeTabPage(int position) {
        if (!fragments.isEmpty() && position<fragments.size()) {
            fragments.remove(position);
            notifyDataSetChanged();
        }
    }

    public void addTabPage(Fragment nFrag) {
        fragments.add(nFrag);
        notifyDataSetChanged();
    }

}