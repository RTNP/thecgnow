package com.newsapp.app.thecgnow2.roomDB.tambola;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "tambola_ticket_number")
public class TambolaRoom {
    @PrimaryKey(autoGenerate = true)
    private int id;

    @ColumnInfo(name = "ticket_num")
    private int ticketNum;

    @ColumnInfo(name = "ticket_date")
    private String ticketDate;

    @ColumnInfo(name = "ticket_checked_status")
    private int checked;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getTicketNum() {
        return ticketNum;
    }

    public void setTicketNum(int ticketNum) {
        this.ticketNum = ticketNum;
    }

    public String getTicketDate() {
        return ticketDate;
    }

    public void setTicketDate(String ticketDate) {
        this.ticketDate = ticketDate;
    }

    public int getChecked() {
        return checked;
    }

    public void setChecked(int status) {
        this.checked = checked;
    }
}
