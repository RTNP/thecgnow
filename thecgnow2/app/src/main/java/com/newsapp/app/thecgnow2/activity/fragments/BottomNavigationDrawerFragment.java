package com.newsapp.app.thecgnow2.activity.fragments;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.newsapp.app.thecgnow2.R;
import com.newsapp.app.thecgnow2.TheCGNowApp;
import com.newsapp.app.thecgnow2.model.Article;
import com.newsapp.app.thecgnow2.roomDB.CGNowDB;
import com.newsapp.app.thecgnow2.roomDB.like.LikeRoom;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.google.android.material.navigation.NavigationView;
import com.google.gson.Gson;

public class BottomNavigationDrawerFragment extends BottomSheetDialogFragment {

    Bundle mArgs;
    CGNowDB mDB;
    TheCGNowApp myApp;
    Context mCtx;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_bottomsheet, container, false);

        mCtx=getActivity().getApplicationContext();
        initDB();
        mArgs=new Bundle();
        mArgs = getArguments();
        Gson gson=new Gson();
        final Article article = gson.fromJson(mArgs.getString("article"), Article.class);

        NavigationView nav_view = view.findViewById(R.id.navigation_view);
        nav_view.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()) {
                    /*case R.id.bottom_nav_share:
                        Toast.makeText(getActivity(), "share "+article.getSrcName(), Toast.LENGTH_SHORT).show();
                        return true;*/
                    case R.id.bottom_nav_like:
//                        Toast.makeText(getActivity(), "like "+article.getSrcName(), Toast.LENGTH_SHORT).show();
                        likeArticle(article);
                        return true;
                    case R.id.bottom_nav_dislike:
//                        Toast.makeText(getActivity(), "dislike "+article.getSrcName(), Toast.LENGTH_SHORT).show();
                        dislikeArticle(article);
                        return true;
                }
                return true;
            }
        });

        return view;
    }

    private void initDB() {
        myApp=(TheCGNowApp)mCtx;
        myApp.initDB(mCtx);
        mDB=myApp.getDB(mCtx);
    }

    private void dislikeArticle(Article article) {
        LikeRoom likeRoom = new LikeRoom();
        if (article!=null) {
            likeRoom.setArticleId(article.getId());

            if (mDB.likeDao().getLikeWithArticleId(likeRoom.getArticleId()) == null) {
//                mDB.likeDao().like(likeRoom);
//                like.setImageResource(R.drawable.ic_liked_icon);
//            like.setTag(1);
            } else {
                mDB.likeDao().unlike(likeRoom.getArticleId());
//                like.setImageResource(R.drawable.ic_default_like_icon);
//            like.setTag(0);
            }
        }
        Toast.makeText(mCtx, "article disliked.", Toast.LENGTH_SHORT).show();
        this.dismiss();
    }

    private void likeArticle(Article article) {
        LikeRoom likeRoom = new LikeRoom();
        if (article!=null) {
            likeRoom.setArticleId(article.getId());

            if (mDB.likeDao().getLikeWithArticleId(likeRoom.getArticleId()) == null) {
                mDB.likeDao().like(likeRoom);
//                like.setImageResource(R.drawable.ic_liked_icon);
//            like.setTag(1);
            }/* else {
                mDB.likeDao().unlike(likeRoom.getArticleId());
//                like.setImageResource(R.drawable.ic_default_like_icon);
//            like.setTag(0);
            }*/
        }
        Toast.makeText(mCtx, "article liked.", Toast.LENGTH_SHORT).show();
        this.dismiss();
    }

    @Override
    public void setupDialog(@NonNull Dialog dialog, int style) {
//        super.setupDialog(dialog, style);
        super.setupDialog(dialog, style);

        View view = LayoutInflater.from(getContext()).inflate(R.layout.fragment_bottomsheet, null);
        dialog.setContentView(view);
    }

}
