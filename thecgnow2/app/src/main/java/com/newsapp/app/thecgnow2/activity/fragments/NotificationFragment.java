package com.newsapp.app.thecgnow2.activity.fragments;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.newsapp.app.thecgnow2.R;
import com.newsapp.app.thecgnow2.SharedPreferenceConfig;
import com.newsapp.app.thecgnow2.TheCGNowApp;
import com.newsapp.app.thecgnow2.activity.MainActivity;
import com.newsapp.app.thecgnow2.adapter.NotificationAdapter;
import com.newsapp.app.thecgnow2.model.Notification;
import com.newsapp.app.thecgnow2.roomDB.CGNowDB;
import com.newsapp.app.thecgnow2.roomDB.notification.NotificationRoom;

import java.util.ArrayList;
import java.util.List;

public class NotificationFragment extends Fragment {

    private ArrayList<Notification> notifications = new ArrayList<>();
    private RecyclerView recyclerView;
    private NotificationAdapter mAdapter;
    private RequestQueue requestQueue;
    BroadcastReceiver broadcastReceiver;

    CGNowDB mDB;
    TheCGNowApp publicAds;
    SharedPreferenceConfig sharedPreferenceConfig;

    Context mCtx;
    public static final String TAG="NotificaitonFragment";

    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view= inflater.inflate(R.layout.fragment_notification, container, false);

        initView(view);

        /*mAdapter = new NotificationAdapter(notifications,mCtx);
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(mCtx);
        recyclerView.setLayoutManager(mLayoutManager);*/

        requestQueue = Volley.newRequestQueue(getActivity());
        sharedPreferenceConfig=new SharedPreferenceConfig(mCtx);

        initDB();
        sharedPreferenceConfig.writeNotificationCount(0);
        registerBroadcastReceiver();
        fetchData();
        MainActivity.setNotificationBadgeCount(0);

        return view;
    }

    private void initDB() {
        publicAds=(TheCGNowApp)getActivity().getApplicationContext();
        publicAds.initDB(getActivity().getApplicationContext());
        mDB=publicAds.getDB(getActivity().getApplicationContext());
    }

    private void registerBroadcastReceiver() {
        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String title = intent.getStringExtra("com.example.thecgnow.notification.data.title");
                String image = intent.getStringExtra("com.example.thecgnow.notification.data.image");
                String id = intent.getStringExtra("com.example.thecgnow.notification.data.id");
                addNotification(id, title, image);
            }
        };
        getActivity().registerReceiver(broadcastReceiver, new IntentFilter("com.example.thecgnow.notification_received"));
    }

    private void addNotification(String id, String title, String image) {

        notifications.add(new Notification(id, title, image));
        mAdapter.notifyItemInserted(notifications.size()-1);
        sharedPreferenceConfig.writeNotificationCount(0);
        MainActivity.setNotificationBadgeCount(0);
    }

    private void fetchData() {
        List<NotificationRoom> notiRooms = mDB.notificationDao().getNotifications();
        Gson gson = new Gson();
        Log.d(TAG, "notisize="+notiRooms.size()+", notilist="+gson.toJson(notiRooms));
        for (NotificationRoom nr : notiRooms) {
            notifications.add(new Notification(nr.getId()+"", nr.getTitle(), nr.getImageUrl()));
        }
//        recyclerView.setAdapter(mAdapter);
    }

    private void initView(View view) {
        recyclerView=view.findViewById(R.id.notification_rv);
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        mCtx=context;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        getActivity().unregisterReceiver(broadcastReceiver);
    }
}
