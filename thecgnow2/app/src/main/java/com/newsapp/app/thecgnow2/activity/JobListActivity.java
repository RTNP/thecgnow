package com.newsapp.app.thecgnow2.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.newsapp.app.thecgnow2.Config;
import com.newsapp.app.thecgnow2.R;
import com.newsapp.app.thecgnow2.SharedPreferenceConfig;
import com.newsapp.app.thecgnow2.adapter.JobListAdapter;
import com.newsapp.app.thecgnow2.model.JobList;
import com.newsapp.app.thecgnow2.model.NewsCategory;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class JobListActivity extends AppCompatActivity {

    public String TAG = "JobListActivity";
    ProgressBar progressBar;
    private ArrayList<JobList> jobs = new ArrayList<>();
    private RecyclerView recyclerView;
    private JobListAdapter mAdapter;
    private RequestQueue requestQueue;
    SharedPreferenceConfig sharedPreferenceConfig;
    private int cid;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_job_list);

        initView();

        setUpJobRecyclerView();

        requestQueue = Volley.newRequestQueue(this);
        sharedPreferenceConfig = new SharedPreferenceConfig(this);

        progressBar.setVisibility(View.VISIBLE);
//        fetchData();

        recyclerView.setAdapter(mAdapter);
    }

    private void setUpJobRecyclerView() {
        Gson gson = new Gson();
        Type type = new TypeToken<ArrayList<JobList>>(){}.getType();
        try {
            JSONObject jsonObject = new JSONObject(sharedPreferenceConfig.readResponseData(Config.jobListKey));
            jobs = gson.fromJson(jsonObject.getJSONArray("data").toString(), type);
            Log.d(TAG, "joblist="+gson.toJson(jobs));

            mAdapter = new JobListAdapter(jobs, this);
            recyclerView.setHasFixedSize(true);
            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
            recyclerView.setLayoutManager(mLayoutManager);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void initView() {
        recyclerView=findViewById(R.id.job_rv);
        progressBar=findViewById(R.id.progressBar_cyclic);
    }

    private void fetchData() {
        String get_query = "?latitude="+sharedPreferenceConfig.readLatitude()+"&longitude="+sharedPreferenceConfig.readLongitude()+"&auth_key="+sharedPreferenceConfig.readFcmToken();
        StringRequest stringRequest = new StringRequest(Request.Method.GET, Config.GET_JOB_LIST_URL+get_query,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        progressBar.setVisibility(View.GONE);
                        Log.d("batchlist_response",response);
                        jobs.clear();
//                        Toast.makeText(AttendenceActivity.this, "attendance response"+response, Toast.LENGTH_SHORT).show();
                        try {
                            //Creating the json object from the response
                            JSONObject jsonResponse = new JSONObject(response);
                            JSONArray jsonArray = jsonResponse.getJSONArray("data");
                            Log.d("jsondatabatch",jsonArray.toString());
//                            Toast.makeText(AttendenceActivity.this, ""+jsonArray.toString(), Toast.LENGTH_SHORT).show();

                            String sourceVal, articleId;
                            for (int i=0; i<jsonArray.length(); i++) {
                                JSONObject j =jsonArray.getJSONObject(i);

                                jobs.add(new JobList(j.getInt("id"),
                                        j.getInt("region_id"),
                                        j.getInt("job_category_id"),
                                        j.getString("company_name"),
                                        j.getString("contact_no"),
                                        j.getString("job_description"),
                                        "http://thecgnow.com/"+j.getString("ad_url")));
                            }
                            mAdapter.notifyDataSetChanged();
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.d("mobileerror",""+e);
//                            Toast.makeText(getActivity(), ""+e.toString(), Toast.LENGTH_SHORT).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {

                        progressBar.setVisibility(View.GONE);
                        String message = null;
                        if (volleyError instanceof NetworkError) {
                            message = "Cannot connect to Internet...Please check your connection!";
                        } else if (volleyError instanceof ServerError) {
                            message = "The server could not be found. Please try again after some time!!";
                        } else if (volleyError instanceof AuthFailureError) {
                            message = "Cannot connect to Internet...Please check your connection!";
                        } else if (volleyError instanceof ParseError) {
                            message = "Parsing error! Please try again after some time!!";
                        } else if (volleyError instanceof NoConnectionError) {
                            message = "Cannot connect to Internet...Please check your connection!";
                        } else if (volleyError instanceof TimeoutError) {
                            message = "Connection TimeOut! Please check your internet connection.";
                        }

                        if (message!=null) {
                            Toast.makeText(JobListActivity.this, getResources().getString(R.string.couldnot_connect),Toast.LENGTH_LONG).show();
                        }
//                        Toast.makeText(mCtx, mCtx.getResources().getString(R.string.couldnot_connect),Toast.LENGTH_LONG).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                //Adding the parameters to the request
                params.put("cid", cid+"");
                params.put("auth_key", sharedPreferenceConfig.readFcmToken());
                return params;
            }

        };

        //Adding request the the queue
        requestQueue.add(stringRequest);
    }


}
