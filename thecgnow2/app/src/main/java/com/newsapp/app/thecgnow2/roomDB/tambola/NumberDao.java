package com.newsapp.app.thecgnow2.roomDB.tambola;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

@Dao
public interface NumberDao {
    @Insert
    public void insertTambolaNumber(NumberRoom numberRoom);

    @Query("select * from tambola_numbers where ticket_date=:date")
    public List<NumberRoom> getTambolaNumbersByDate(String date);

    @Query("SELECT DISTINCT * FROM tambola_numbers")
    public NumberRoom getTambolaNumbers();

    @Query("DELETE FROM tambola_numbers where ticket_date !=:date")
    public void deletePrevSessionTambolaNumbers(String date);

    @Query("DELETE FROM tambola_numbers")
    public void deleteAllNumbers();
}
