package com.newsapp.app.thecgnow2.activity.fragments.housie;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.newsapp.app.thecgnow2.R;

public class HowToPlayFragment extends Fragment {

    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_how_to_play, container, false);

        initView(view);

        return view;
    }

    private void initView(View view) {
    }
}
