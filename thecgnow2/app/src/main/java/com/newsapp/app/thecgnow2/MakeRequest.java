package com.newsapp.app.thecgnow2;

import android.app.Activity;
import android.util.Log;
import android.widget.Toast;

import org.jetbrains.annotations.NotNull;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.ResponseBody;
import okhttp3.logging.HttpLoggingInterceptor;

public class MakeRequest {

    static public void getRequestOnSharedPreference(String url, Activity activity, String sharedKey){
        SharedPreferenceConfig sharedPreferenceConfig = new SharedPreferenceConfig(activity.getApplicationContext());
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BASIC);
        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(logging)
                .build();
        Request newReq = new Request.Builder()
                .url(url)
                .build();
        client.newCall(newReq).enqueue(new Callback() {
            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {
                (activity).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
//                        Toast.makeText(activity,"Error : " + e.getMessage(),Toast.LENGTH_LONG).show();
                    }
                });
            }
            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {
                try (ResponseBody responseBody = response.body()) {
                    if (!response.isSuccessful()) throw new IOException("Unexpected code " + response);
                    JSONObject myResponse = null;
                    try {
                        myResponse = new JSONObject(response.body().string());
                    } catch (JSONException e) {
                        Log.e("Error : ",e.getMessage());
                    }
                    JSONObject finalMyResponse = myResponse;
                    (activity).runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            sharedPreferenceConfig.writeResponseData(sharedKey, finalMyResponse.toString());
                            Log.d("MakeRequest", sharedKey+" "+finalMyResponse.toString());
                        }
                    });
                }
            }
        });
    }
    static public void postRequestOnSharedPreference(String url, JSONObject postData,Activity activity, String sharedKey){
        SharedPreferenceConfig sharedPreferenceConfig = new SharedPreferenceConfig(activity.getApplicationContext());
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BASIC);
        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(logging)
                .build();
        final MediaType JSON = MediaType.parse("application/json; charset=utf-8");
        RequestBody body = RequestBody.create(postData.toString(),JSON);
        Request newReq = new Request.Builder()
                .addHeader("Content-Type", "application/json")
                .url(url)
                .post(body)
                .build();
        client.newCall(newReq).enqueue(new Callback() {
            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {
                (activity).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(activity,"Error : " + e.getMessage(),Toast.LENGTH_LONG).show();
                    }
                });
            }
            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {
                try (ResponseBody responseBody = response.body()) {
                    if (!response.isSuccessful()) throw new IOException("Unexpected code " + response);
                    JSONObject myResponse = null;
                    try {
                        myResponse = new JSONObject(response.body().string());
                    } catch (JSONException e) {
                        Toast.makeText(activity,"Error : " + e.getMessage(),Toast.LENGTH_LONG).show();
                    }
                    JSONObject finalMyResponse = myResponse;
                    (activity).runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            sharedPreferenceConfig.writeResponseData(sharedKey, finalMyResponse.toString());
                            Log.d("MakeRequest", sharedKey+" "+finalMyResponse.toString());
                        }
                    });
                }
            }
        });
    }

}
