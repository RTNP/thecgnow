package com.newsapp.app.thecgnow2.roomDB.notification;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

@Dao
public interface NotificationDao {
    @Insert
    public void addNotification(NotificationRoom notification);

    @Query("select * from notifications")
    public List<NotificationRoom> getNotifications();

    @Query("DELETE FROM notifications WHERE date =:date")
    public void deleteNotificationsForDate(String date);

    @Query("DELETE FROM notifications WHERE title =:noti_title")
    public void deleteNotificationsForTitle(String noti_title);
}
