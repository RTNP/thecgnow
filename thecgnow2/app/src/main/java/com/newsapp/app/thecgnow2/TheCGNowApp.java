package com.newsapp.app.thecgnow2;

import android.app.Application;
import android.content.Context;

import androidx.room.Room;

import com.newsapp.app.thecgnow2.R;
import com.newsapp.app.thecgnow2.model.Advertize;
import com.newsapp.app.thecgnow2.roomDB.CGNowDB;

import java.util.ArrayList;

public class TheCGNowApp extends Application {
    // updated arraylists
    ArrayList<Advertize> news_banner_ads = new ArrayList<>();
    ArrayList<Advertize> news_label_ads = new ArrayList<>();
    ArrayList<Advertize> tambola_banner_ads = new ArrayList<>();
    ArrayList<Advertize> tambola_label_ads = new ArrayList<>();

    // old arraylists
   ArrayList<Advertize> advertizes = new ArrayList<>();
   ArrayList<Advertize> tambolaLAd= new ArrayList<>();
    ArrayList<Advertize> tambolaBAd= new ArrayList<>();
   ArrayList<Advertize> labelAdvertizes = new ArrayList<>();
   ArrayList<Advertize> currentAffairsLAd = new ArrayList<>();
   ArrayList<Advertize> currentAffairsBAd = new ArrayList<>();
   public static boolean isPusherSubsBind = false;
    public int adBannerItemPosition=0, labelAdItemPosition=0, tambolaLAdPos=0, tambolaBAdPos=0;   // it is used to identifies the position of advertiseList to which is currently showing
    public CGNowDB mDB;
    public static Context mCtx;
    public static String locale;
    public static String currentTambolaNum="";

    @Override
    public Context getApplicationContext() {
        return super.getApplicationContext();
    }

    public CGNowDB getDB(Context mCtx) {
        return mDB;
    }

    public void initContext(Context context) {
        this.mCtx=context;
        SharedPreferenceConfig sp=new SharedPreferenceConfig(mCtx);
        locale=sp.readDefaultLang();
    }
    public void initDB(Context context) {
        mDB = Room.databaseBuilder(context, CGNowDB.class,  getResources().getString(R.string.db_string))
                .fallbackToDestructiveMigration()
                .allowMainThreadQueries().build();
    }

    // methods for advertizes
    public ArrayList<Advertize> getAdvertizes() {
        return advertizes;
    }

    public void setAdvertizes(ArrayList<Advertize> advertizes) {
        this.advertizes = advertizes;
    }

    public void addBannerAdvertize(Advertize advertize) {
        advertizes.add(advertize);
    }

    public void removeBannerAdvertize(int pos) {
        advertizes.remove(pos);
    }

    // methods for laberAdvertizes
    public ArrayList<Advertize> getLabelAdvertizes() {
        return labelAdvertizes;
    }

    public void setLabelAdvertizes(ArrayList<Advertize> labelAdvertizes) {
        this.labelAdvertizes = labelAdvertizes;
    }

    public void addLabelAdvertize(Advertize advertize) {
        labelAdvertizes.add(advertize);
    }

    public void removeLabelAdvertize(int pos) {
        labelAdvertizes.remove(pos);
    }

    // methods for tambolaLAd (tambola labels advertise)

    public ArrayList<Advertize> getTambolaLAd() {
        return tambolaLAd;
    }

    public void setTambolaLAd(ArrayList<Advertize> tambolaLAd) {
        this.tambolaLAd = tambolaLAd;
    }

    public void addTambolaLAd(Advertize advertize) {
        tambolaLAd.add(advertize);
    }

    public void removeTambolaLAd(int pos) {
        tambolaLAd.remove(pos);
    }

    // methods for tambolaBAd (tambola banners advertise)

    public ArrayList<Advertize> getTambolaBAd() {
        return tambolaBAd;
    }

    public void setTambolaBAd(ArrayList<Advertize> advertize) {
        this.tambolaBAd = advertize;
    }

    public void addTambolaBAd(Advertize advertize) {
        tambolaBAd.add(advertize);
    }

    public void removeTambolaBAd(int pos) {
        tambolaBAd.remove(pos);
    }


    // updated arraylists
    public ArrayList<Advertize> getNews_banner_ads() {
        return news_banner_ads;
    }

    public void setNews_banner_ads(ArrayList<Advertize> news_banner_ads) {
        this.news_banner_ads = news_banner_ads;
    }

    public ArrayList<Advertize> getNews_label_ads() {
        return news_label_ads;
    }

    public void setNews_label_ads(ArrayList<Advertize> news_label_ads) {
        this.news_label_ads = news_label_ads;
    }

    public ArrayList<Advertize> getTambola_banner_ads() {
        return tambola_banner_ads;
    }

    public void setTambola_banner_ads(ArrayList<Advertize> tambola_banner_ads) {
        this.tambola_banner_ads = tambola_banner_ads;
    }

    public ArrayList<Advertize> getTambola_label_ads() {
        return tambola_label_ads;
    }

    public void setTambola_label_ads(ArrayList<Advertize> tambola_label_ads) {
        this.tambola_label_ads = tambola_label_ads;
    }
}
