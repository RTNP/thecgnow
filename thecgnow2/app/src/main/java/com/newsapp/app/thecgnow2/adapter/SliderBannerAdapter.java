package com.newsapp.app.thecgnow2.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.newsapp.app.thecgnow2.R;
import com.newsapp.app.thecgnow2.activity.ArticleDescription;
import com.newsapp.app.thecgnow2.model.Article;
import com.smarteist.autoimageslider.SliderViewAdapter;

import java.util.ArrayList;

public class SliderBannerAdapter extends
        SliderViewAdapter<SliderBannerAdapter.SliderAdapterVH> {
    private Context context;
    private ArrayList<Article> articles;

    public SliderBannerAdapter(Context context, ArrayList<Article> articles) {
        this.context = context;
        this.articles = articles;
    }

    @Override
    public SliderAdapterVH onCreateViewHolder(ViewGroup parent) {
        View inflate = LayoutInflater.from(parent.getContext()).inflate(R.layout.image_slider_layout_item, null);
        return new SliderAdapterVH(inflate);
    }

    @Override
    public void onBindViewHolder(SliderAdapterVH viewHolder, final int position) {

       Article article = articles.get(position);

        viewHolder.textViewDescription.setText(article.getTitle());
        viewHolder.textViewDescription.setTextSize(18);
        viewHolder.textViewDescription.setTextColor(Color.BLACK);
        Glide.with(viewHolder.itemView)
                .load(article.getMediaUrl())
                .fitCenter()
                .into(viewHolder.imageViewBackground);

        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Toast.makeText(context, "This is item in position " + position, Toast.LENGTH_SHORT).show();
                Gson gson=new Gson();
                String articleArray=gson.toJson(article);
                Intent intent=new Intent(context, ArticleDescription.class);
                intent.putExtra("article",articleArray);
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getCount() {
        //slider view count could be dynamic size
        return articles.size();
    }

    class SliderAdapterVH extends SliderViewAdapter.ViewHolder {

        View itemView;
        ImageView imageViewBackground;
        ImageView imageGifContainer;
        TextView textViewDescription;

        public SliderAdapterVH(View itemView) {
            super(itemView);
            imageViewBackground = itemView.findViewById(R.id.iv_auto_image_slider);
            imageGifContainer = itemView.findViewById(R.id.iv_gif_container);
            textViewDescription = itemView.findViewById(R.id.tv_auto_image_slider);
            this.itemView = itemView;
        }
    }
}
