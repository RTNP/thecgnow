package com.newsapp.app.thecgnow2.activity.fragments;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.newsapp.app.thecgnow2.Config;
import com.newsapp.app.thecgnow2.R;
import com.newsapp.app.thecgnow2.SharedPreferenceConfig;
import com.newsapp.app.thecgnow2.TheCGNowApp;
import com.newsapp.app.thecgnow2.adapter.ViewPagerAdapter;
import com.newsapp.app.thecgnow2.model.NewsCategory;
import com.newsapp.app.thecgnow2.roomDB.CGNowDB;
import com.newsapp.app.thecgnow2.roomDB.category.CategoryRoom;
import com.google.android.material.tabs.TabLayout;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;

public class HomeFragment extends Fragment {

    private Context mCtx;
    public final String TAG="HomeFragment";
    private ArrayList<NewsCategory> newsCategories=new ArrayList<>();
    private ArrayList<NewsCategory> categoryList=new ArrayList<>();
    ArrayList<String> catStringList = new ArrayList<>();
    SharedPreferenceConfig sharedPreferenceConfig;
    RequestQueue requestQueue;
    private CGNowDB mDB;
    private TheCGNowApp cgNowApp;
    private ViewPager viewPager;
    TabLayout tabs;
    private static final String ARG_NAME = "arg_name";

    public static HomeFragment newInstance(String name) {
        Bundle bundle = new Bundle();
        bundle.putString(ARG_NAME, name);
        HomeFragment dashboardFragment = new HomeFragment();
        dashboardFragment.setArguments(bundle);
        return dashboardFragment;
    }

    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        initDB();
        sharedPreferenceConfig = new SharedPreferenceConfig(mCtx);
        requestQueue = Volley.newRequestQueue(mCtx);
        Log.d(TAG, "fragment home:");
        return inflater.inflate(R.layout.fragment_home, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        viewPager = view.findViewById(R.id.view_pager_home);
        tabs = view.findViewById(R.id.tabs_home);

        if (mDB.categoryDao().getCategoriesInOrder().size()!=0) {
            Log.d(TAG, "default tabs loading...");
            setDefaultNewsCategories();
        } else {
            Log.d(TAG, "default tabs loading failed catsize="+mDB.categoryDao().getCategoriesInOrder().size());
        }
        fetchNewsCategories();
    }

    private void setDefaultNewsCategories() {
        for (CategoryRoom c : mDB.categoryDao().getCategoriesInOrder()) {
            NewsCategory n = new NewsCategory(c.getCatId(), c.getCatName(), c.getCatKeyName());
            newsCategories.add(new NewsCategory(c.getCatId(), c.getCatName(), c.getCatKeyName()));
            Log.d(TAG, "catname: " + c.getCatName() + " catkey: " + c.getCatKeyName());
            Log.d(TAG, "catname2: " + newsCategories.get(newsCategories.size() - 1).getName() + " catkey: " + newsCategories.get(newsCategories.size() - 1).getKeyName());
        }

        setupViewPager();
    }

    private void fetchNewsCategories() {
        Gson gson = new Gson();
        Type type = new TypeToken<ArrayList<NewsCategory>>(){}.getType();
        try {
            JSONObject jsonObject = new JSONObject(sharedPreferenceConfig.readResponseData(Config.categoryListKey));
            categoryList = gson.fromJson(jsonObject.getJSONArray("data").toString(), type);
            Log.d(TAG, "categorylist="+gson.toJson(categoryList));
            setNewsCategories();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void initDB() {
        cgNowApp=(TheCGNowApp)mCtx.getApplicationContext();
        cgNowApp.initDB(mCtx);
        mDB=cgNowApp.getDB(mCtx);
    }


    private void setNewsCategories() {

        if (sharedPreferenceConfig.readFirstTimeUse()) {
            /*newsCategories.clear();
            newsCategories.addAll(categoryList);*/
            for (NewsCategory n : categoryList) {
                CategoryRoom cr = new CategoryRoom();
                cr.setCatId(n.getId());
                cr.setCatName(n.getName());
                cr.setCatKeyName(n.getKeyName());
                mDB.categoryDao().addCategory(cr);
            }

            sharedPreferenceConfig.writeFirstTimeUse(false);
        }

        Gson gson = new Gson();
        Log.d(TAG, "categoryinorder: "+gson.toJson(mDB.categoryDao().getCategoriesInOrder()));

        newsCategories.clear();

        for (CategoryRoom c : mDB.categoryDao().getCategoriesInOrder()) {
            NewsCategory n = new NewsCategory(c.getCatId(), c.getCatName(), c.getCatKeyName());
            if (isCategoryValid(n) && !catStringList.contains(n.getKeyName())) {
                catStringList.add(n.getKeyName());
                newsCategories.add(new NewsCategory(c.getCatId(), c.getCatName(), c.getCatKeyName()));
                Log.d(TAG, "catname: " + c.getCatName() + " catkey: " + c.getCatKeyName());
                Log.d(TAG, "catname2: " + newsCategories.get(newsCategories.size() - 1).getName() + " catkey: " + newsCategories.get(newsCategories.size() - 1).getKeyName());
            } else {
                mDB.categoryDao().removeCategory(c.getId());
            }
        }

        setupViewPager();

    }

    public boolean isCategoryValid(NewsCategory newsCategory) {
        for (NewsCategory n : categoryList) {
            if (n.getKeyName().equals(newsCategory.getKeyName())) {
                return true;
            }
        }
        return false;
    }

    private void setupViewPager() {

        ViewPagerAdapter adapter = new ViewPagerAdapter(getActivity().getSupportFragmentManager());

        int count = newsCategories.size();
        for (int i=0; i<count; i++){
          Log.d(TAG, "addfragada: "+newsCategories.get(i).getName()+" key: "+newsCategories.get(i).getKeyName());
            adapter.addFrag(newsCategories.get(i));
        }

        viewPager.setAdapter(adapter);
        viewPager.setOffscreenPageLimit(adapter.getCount()-1);
        tabs.setupWithViewPager(viewPager);
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        mCtx=context;
    }


}
