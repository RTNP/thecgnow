package com.newsapp.app.thecgnow2;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import androidx.core.app.NotificationCompat;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.newsapp.app.thecgnow2.R;
import com.newsapp.app.thecgnow2.activity.MainActivity;
import com.newsapp.app.thecgnow2.roomDB.CGNowDB;
import com.newsapp.app.thecgnow2.roomDB.notification.NotificationRoom;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class MyFirebaseMessagingService extends FirebaseMessagingService {
    private static final String TAG = "MyFirebaseMsgService";
    RequestQueue requestQueue;
    SharedPreferenceConfig sharedPreferenceConfig;
    CGNowDB mDB;
    TheCGNowApp publicAds;


    // [START on_new_token]

    @Override
    public void onNewToken(String token) {
        super.onNewToken(token);
        Log.d(TAG, "Refreshed token: " + token);
        sharedPreferenceConfig = new SharedPreferenceConfig(this);
        sharedPreferenceConfig.writeFcmToken(token);

        // If you want to send messages to this application instance or
        // manage this apps subscriptions on the server side, send the
        // Instance ID token to your app server.
    }

    // [END on_new_token]

    // [START receive_message]

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        if (remoteMessage.getData().size() > 0) {

            Log.d(TAG, "Message data payload: " + remoteMessage.getData());

            initDB();

            if (/* Check if data needs to be processed by long running job */ false) {

                // For long-running tasks (10 seconds or more) use WorkManager.

                scheduleJob();

            } else {
                try {
                    Map<String, String> params = remoteMessage.getData();
                    JSONObject object = new JSONObject(params);
                    JSONObject object1 = new JSONObject(object.get("data").toString());

                    Gson gson = new Gson();
                    Log.d(TAG, "rmdata:" + object.toString());
                    Log.d(TAG, "rmdata2:" + object1.get("image"));
                    Log.d(TAG, "rmdata1:" + object.get("data"));
                    Log.d(TAG, "rmdata3:" + object1.get("source"));
                    String title = object1.get("title").toString();
                    String source = object1.get("source").toString();

                    if (source.equals("news")) {
                        sendNotification(title);
                        // Handle message within 10 seconds
                        handleNow(object1);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

        }

        // Check if message contains a notification payload.

        if (remoteMessage.getNotification() != null) {

            Log.d(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());

        }

        // Also if you intend on generating your own notifications as a result of a received FCM

        // message, here is where that should be initiated. See sendNotification method below.
        // [END receive_message]
    }

    private void initDB() {
        publicAds=(TheCGNowApp)getApplication().getApplicationContext();
        publicAds.initDB(getApplication().getApplicationContext());
        mDB=publicAds.getDB(getApplication().getApplicationContext());
    }

    private void scheduleJob() {

        // [START dispatch_job]

        // [END dispatch_job]

    }
    /**

     * Handle time allotted to BroadcastReceivers.
     * @param object1

     */

    private void handleNow(JSONObject object1) {
        sharedPreferenceConfig=new SharedPreferenceConfig(this);
        int noti_count = sharedPreferenceConfig.readNotificationCount();
        sharedPreferenceConfig.writeNotificationCount(noti_count+1);
        try {
            String title = object1.get("title").toString();
            String image = object1.get("image").toString();
            String id = object1.get("id").toString();

            // Adding value in sqllite db
            NotificationRoom notificationRoom = new NotificationRoom();
            notificationRoom.setId(Integer.parseInt(id));
            notificationRoom.setTitle(title);
            notificationRoom.setImageUrl(image);

            mDB.notificationDao().addNotification(notificationRoom);

            // creating intent to send broadcast
            Intent in = new Intent(Config.NOTIFICATION_RECEIVED);  // intent filter to identify
            Bundle extras = new Bundle();
            extras.putString("com.example.thecgnow.notification.data.title", title);
            extras.putString("com.example.thecgnow.notification.data.image", image);
            extras.putString("com.example.thecgnow.notification.data.id", id);
            extras.putInt(Config.NOTIFICATION_KEY_STRING, Config.NOTIFICATION_KEY_VALUE);
            in.putExtras(extras);
            sendBroadcast(in);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.d(TAG, "Short lived task is done.");


    }

    private void sendNotification(String messageBody) {

        Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra("fragToLoad", Config.POS_NOTI_FRAG);

        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,

                PendingIntent.FLAG_ONE_SHOT);



//        String channelId = getString(R.string.default_notification_channel_id);

        String channelId = "ch1";

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        Context context = this;

        NotificationCompat.Builder notificationBuilder =

                new NotificationCompat.Builder(this, channelId)

                        .setSmallIcon(R.drawable.app_icon_noti, View.INVISIBLE)

                        .setLargeIcon(BitmapFactory.decodeResource(context.getResources(),
                                R.mipmap.ic_launcher))

                        .setContentTitle(messageBody)

//                        .setContentText("TheCGNow")

//                        .setAutoCancel(true)

                        .setSound(defaultSoundUri)

                        .setContentIntent(pendingIntent);



        NotificationManager notificationManager =

                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);



        // Since android Oreo notification channel is needed.

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

            NotificationChannel channel = new NotificationChannel(channelId,

                    "Channel human readable title",

                    NotificationManager.IMPORTANCE_DEFAULT);

            notificationManager.createNotificationChannel(channel);

        }

        notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());

    }
}
