package com.newsapp.app.thecgnow2.activity.fragments;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.newsapp.app.thecgnow2.Config;
import com.newsapp.app.thecgnow2.R;
import com.newsapp.app.thecgnow2.TheCGNowApp;
import com.newsapp.app.thecgnow2.adapter.FavouriteAdapter;
import com.newsapp.app.thecgnow2.model.Recommend;
import com.newsapp.app.thecgnow2.model.Topic;
import com.newsapp.app.thecgnow2.roomDB.CGNowDB;
import com.newsapp.app.thecgnow2.roomDB.follow.FollowRoom;

import java.util.ArrayList;
import java.util.List;

public class FavouriteFragment extends Fragment {

    private ArrayList<Recommend> recommends = new ArrayList<>();
    private RecyclerView recyclerView;
    private FavouriteAdapter mAdapter;
    private RequestQueue requestQueue;

    CGNowDB mDB;
    TheCGNowApp cgNowApp;

    Context mCtx;
    public static final String TAG="NotificaitonFragment";

    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view= inflater.inflate(R.layout.fragment_favourite, container, false);

        initView(view);
        cgNowApp=(TheCGNowApp)mCtx.getApplicationContext();
        cgNowApp.initDB(mCtx);

        mAdapter = new FavouriteAdapter(recommends, mCtx);
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(mCtx);
        recyclerView.setLayoutManager(mLayoutManager);

        requestQueue = Volley.newRequestQueue(getActivity());

        fetchData();
        recyclerView.setAdapter(mAdapter);

        return view;
    }

    private void fetchData() {
//        mDB = Room.databaseBuilder(mCtx, CGNowDB.class, mCtx.getResources().getString(R.string.db_string)).allowMainThreadQueries().build();
        mDB=cgNowApp.getDB(mCtx.getApplicationContext());
        List<FollowRoom> following = mDB.followDao().getFollowings();
        ArrayList<Topic> frTopics=new ArrayList<>();
        for (FollowRoom fr : following) {
            frTopics.add(new Topic(1, fr.getSourceName()));
        }

        recommends.add(new Recommend(Config.SUGGEST_CIRCLE, "Your Followings", frTopics));
    }

    private void initView(View view) {
        recyclerView=view.findViewById(R.id.favourite_rv);
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        mCtx=context;
    }
}
