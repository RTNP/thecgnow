package com.newsapp.app.thecgnow2.roomDB.ad;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "ads")
public class AdRoom {
    @PrimaryKey(autoGenerate = true)
    private int id;

    @ColumnInfo(name = "ad_section")
    private int adSection;

    @ColumnInfo(name = "ad_pos")
    private int adPos;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getAdSection() {
        return adSection;
    }

    public void setAdSection(int adSection) {
        this.adSection = adSection;
    }

    public int getAdPos() {
        return adPos;
    }

    public void setAdPos(int adPos) {
        this.adPos = adPos;
    }
}
