package com.newsapp.app.thecgnow2.adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.newsapp.app.thecgnow2.R;
import com.newsapp.app.thecgnow2.model.JobList;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class JobListAdapter extends RecyclerView.Adapter {

    ArrayList<JobList> jobs;
    Context mCtx;
    String TAG = "JobListAdapter";

    public JobListAdapter(ArrayList<JobList> jobs, Context mCtx) {
        this.jobs = jobs;
        this.mCtx = mCtx;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView;

        itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.job_click_list_item, parent, false);

        return new ViewHolderOne(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        JobList jobList=jobs.get(position);
        try {
            ((ViewHolderOne) holder).setViews(jobList);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return jobs.size();
    }

    public class ViewHolderOne extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView title, contact, description;
        ImageView adImage;
        LinearLayout adView;

        public ViewHolderOne(@NonNull View itemView) {
            super(itemView);
            title=itemView.findViewById(R.id.title);
            contact=itemView.findViewById(R.id.contact);
            description=itemView.findViewById(R.id.description);
            adImage=itemView.findViewById(R.id.image_ad);
            adView=itemView.findViewById(R.id.ad_ll);
        }

        public void setViews(final JobList job) {
            title.setText(job.getJobTitle());
            contact.setText(job.getContact());
            description.setText(Html.fromHtml(job.getDiscription()));
            Picasso.get().load(job.getAdUrl()).into(adImage, new Callback() {
                @Override
                public void onSuccess() {
                    adView.setVisibility(View.VISIBLE);
                }

                @Override
                public void onError(Exception e) {
                    Log.d(TAG, "adurl="+job.getAdUrl());
                }

            });
            contact.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (view.getId()==R.id.contact) {
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:"+jobs.get(getAdapterPosition()).getContact().trim()));
                mCtx.startActivity(intent);
            }
        }
    }
}
