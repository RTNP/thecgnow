package com.newsapp.app.thecgnow2.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.newsapp.app.thecgnow2.Config;
import com.newsapp.app.thecgnow2.R;
import com.newsapp.app.thecgnow2.model.Offer;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class OfferAdapter extends RecyclerView.Adapter {

    ArrayList<Offer> offers;
    Context mCtx;
    String TAG = "OfferAdapter";

    public OfferAdapter(ArrayList<Offer> offers, Context mCtx) {
        this.offers = offers;
        this.mCtx = mCtx;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView;

        itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.offer_list_item, parent, false);

        return new ViewHolderOne(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        Offer offer = offers.get(position);
        try {
            ((ViewHolderOne) holder).setViews(offer);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        Log.d(TAG, "offersize="+offers.size());
        return offers.size();
    }

    public class ViewHolderOne extends RecyclerView.ViewHolder {
        ImageView offerImg;

        public ViewHolderOne(@NonNull View itemView) {
            super(itemView);
            offerImg=itemView.findViewById(R.id.img);
        }

        public void setViews(Offer offer) {
//            title.setText("offer "+getAdapterPosition());
            Log.d(TAG, "imgurl="+offer.getOfferImage());
            Picasso.get().load("http://thecgnow.com/"+offer.getOfferImage()).placeholder(R.drawable.placeholder).into(offerImg);
        }
    }
}
