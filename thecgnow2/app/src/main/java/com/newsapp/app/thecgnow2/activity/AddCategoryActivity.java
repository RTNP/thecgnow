package com.newsapp.app.thecgnow2.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Cache;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.newsapp.app.thecgnow2.Config;
import com.newsapp.app.thecgnow2.R;
import com.newsapp.app.thecgnow2.SharedPreferenceConfig;
import com.newsapp.app.thecgnow2.adapter.AddRegionAdapter;
import com.newsapp.app.thecgnow2.model.NewsCategory;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class AddCategoryActivity extends AppCompatActivity {

    private static final String TAG = "AddCategoryActivity";
    RecyclerView recyclerView;
    AddRegionAdapter mAdapter;
    ArrayList<NewsCategory> regions=new ArrayList<>();
    RequestQueue requestQueue;
    SharedPreferenceConfig sharedPreferenceConfig;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_category);

        initView();
        requestQueue= Volley.newRequestQueue(this);
        sharedPreferenceConfig = new SharedPreferenceConfig(this);
        setUpRecyclerView();
        getRegionFromDB();
    }

    private void setUpRecyclerView() {
        mAdapter = new AddRegionAdapter(regions, this);
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setAdapter(mAdapter);
    }

    private void getRegionFromDB() {
//        regions.add("Select regions");
        String get_query = "?auth_key="+sharedPreferenceConfig.readFcmToken();
        Log.d(TAG, "query:"+Config.WEB_HOSE_API_URL+get_query);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, Config.GET_CATEGORY_URL+get_query,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        regions.clear();
                        Log.d(TAG,"categories: "+response);
//                        Toast.makeText(AttendenceActivity.this, "attendance response"+response, Toast.LENGTH_SHORT).show();
                        try {
                            //Creating the json object from the response
                            JSONObject jsonResponse = new JSONObject(response);
                            JSONArray jsonArray = jsonResponse.getJSONArray("data");
                            Log.d(TAG,jsonArray.toString());
//                            Toast.makeText(AttendenceActivity.this, ""+jsonArray.toString(), Toast.LENGTH_SHORT).show();


                            for (int i=0; i<jsonArray.length(); i++) {
                                JSONObject regionJson = jsonArray.getJSONObject(i);
//                                if (regionJson.getInt("active")==1) {
                                    regions.add(new NewsCategory(regionJson.getInt("id"),
                                            regionJson.getString("region_name_hi"),
                                            regionJson.getString("region_name"),
                                            regionJson.getInt("category_order")));
//                                }
                            }

                            mAdapter.notifyDataSetChanged();

                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.d("mobileerror",""+e);
//                            Toast.makeText(getActivity(), ""+e.toString(), Toast.LENGTH_SHORT).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(AddCategoryActivity.this, getResources().getString(R.string.couldnot_connect),Toast.LENGTH_LONG).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                //Adding the parameters to the request
                params.put("auth_key", sharedPreferenceConfig.readFcmToken());
                return params;
            }
        };

        //Adding request the the queue
        requestQueue.add(stringRequest);
    }

    private void initView() {
        recyclerView=findViewById(R.id.categories);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();
    }
}
