package com.newsapp.app.thecgnow2.activity.fragments.housieboard;

import android.Manifest;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.GridLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.app.NotificationCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.Cache;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.newsapp.app.thecgnow2.Config;
import com.newsapp.app.thecgnow2.ExpandableHeightGridView;
import com.newsapp.app.thecgnow2.NotificationReceiver;
import com.newsapp.app.thecgnow2.PusherConfig;
import com.newsapp.app.thecgnow2.R;
import com.newsapp.app.thecgnow2.SharedPreferenceConfig;
import com.newsapp.app.thecgnow2.TambolaNumberListener;
import com.newsapp.app.thecgnow2.TheCGNowApp;
import com.newsapp.app.thecgnow2.activity.HousieBoardActivity;
import com.newsapp.app.thecgnow2.activity.JobDescriptionActivity;
import com.newsapp.app.thecgnow2.activity.MainActivity;
import com.newsapp.app.thecgnow2.activity.TicketBoardView;
import com.newsapp.app.thecgnow2.activity.TicketTextView;
import com.newsapp.app.thecgnow2.adapter.CustomGridAdapter;
import com.newsapp.app.thecgnow2.adapter.WinnerListAdapter;
import com.newsapp.app.thecgnow2.model.Advertize;
import com.newsapp.app.thecgnow2.model.JobList;
import com.newsapp.app.thecgnow2.roomDB.CGNowDB;
import com.newsapp.app.thecgnow2.roomDB.tambola.NumberRoom;
import com.newsapp.app.thecgnow2.roomDB.tambola.TambolaRoom;
import com.google.android.exoplayer2.ExoPlayer;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory;
import com.google.android.exoplayer2.extractor.ExtractorsFactory;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelector;
import com.google.android.exoplayer2.ui.PlayerControlView;
import com.google.android.exoplayer2.ui.SimpleExoPlayerView;
import com.google.android.exoplayer2.upstream.BandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultHttpDataSourceFactory;
import com.google.gson.Gson;
import com.jcminarro.roundkornerlayout.RoundKornerLinearLayout;
import com.pusher.client.Pusher;
import com.pusher.client.PusherOptions;
import com.pusher.client.channel.Channel;
import com.pusher.client.channel.ChannelEventListener;
import com.pusher.client.channel.SubscriptionEventListener;
import com.pusher.client.connection.ConnectionEventListener;
import com.pusher.client.connection.ConnectionStateChange;
import com.squareup.picasso.Picasso;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Random;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.WebSocket;
import okhttp3.WebSocketListener;
import okio.ByteString;

public class BoardFragment extends Fragment implements View.OnClickListener, TicketBoardView.OnToggledListener, TambolaNumberListener {
    private final String TAG = "BoardFragment";
    private TicketBoardView[] ticketBoardView;
    private GridLayout myGridLayout;
    private ExpandableHeightGridView bubbleGrid;
    private TicketTextView[] textView;
    private Button takeScreenshot;
    private TextView ticketNum;
    private int[][] combinations, threeCombination, colWiseThreeToTwoCombNumbers;
    private Context mCtx;
    private boolean isActivityCurrent=false;
    Advertize banner_ad;

    private ImageView adBanner;

    private SharedPreferenceConfig sharedPreferenceConfig;

    private ScrollView mainScrollView;
    private RecyclerView recyclerView;
    private WinnerListAdapter mAdapter;

    private RequestQueue requestQueue;

    private String number = null;
    private int random;
    private Handler myHandler;

    private ArrayList<Integer> numbers = new ArrayList<>();

    private CGNowDB mDB;
    private TheCGNowApp publicAds;

    private BroadcastReceiver broadcastReceiver, broadcastReceiver2;
    private SimpleExoPlayerView simpleExoPlayerView;
    private SimpleExoPlayer simpleExoPlayer;
    private RoundKornerLinearLayout rkLayout;
    private LinearLayout linearLayoutAd;
    private PlayerControlView playerControlView;
    private TextView ticketId;
    private String dateStr;

    private final long startTime = System.currentTimeMillis();

    private OkHttpClient client;
    TambolaWebSocket listener;
    WebSocket ws;

    private CustomGridAdapter customGridAdapter;

    private static final int REQUEST_EXTERNAL_STORAGE = 1;
    private static String[] PERMISSIONS_STORAGE = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };

    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        final View view = inflater.inflate(R.layout.fragment_board, container, false);  //fragment_board

        initView(view);

        initDB();

        registerBroadcastReceiver();

        requestQueue = Volley.newRequestQueue(getActivity());

        sharedPreferenceConfig=new SharedPreferenceConfig(getActivity());
        setHandler();

        isActivityCurrent=true;

//        rkLayout.setVisibility(View.GONE);
        linearLayoutAd.setVisibility(View.GONE);
        ticketId.setText("Tambola Ticket #"+sharedPreferenceConfig.readResponseData(Config.ticketIdKey));
        setAdvertise();

        setClickEventForAds();

        initBubbleGrid();

        number = TheCGNowApp.currentTambolaNum;
        if (number!=null) {
            ticketNum.setText(number);
        }

        int numOfCol = myGridLayout.getColumnCount();
        int numOfRow = myGridLayout.getRowCount();
        ticketBoardView = new TicketBoardView[numOfCol*numOfRow];
        textView = new TicketTextView[numOfCol*numOfRow];

        for(int yPos=0; yPos<numOfRow; yPos++){
            for(int xPos=0; xPos<numOfCol; xPos++){
                TicketTextView tView = new TicketTextView(getActivity(), xPos, yPos);
                tView.setBackgroundColor(Color.WHITE);
                tView.setGravity(Gravity.CENTER);
                tView.setTextSize(18);
                tView.setTypeface(Typeface.DEFAULT_BOLD);
                tView.setTextColor(Color.parseColor("#FF424242"));
                tView.setTag(yPos*numOfCol + xPos);
                tView.setOnClickListener(this);
                textView[yPos*numOfCol + xPos] = tView;

                myGridLayout.addView(tView);
            }
        }

        generateNewTicket();
        myGridLayout.getViewTreeObserver().addOnGlobalLayoutListener(
                new ViewTreeObserver.OnGlobalLayoutListener(){

                    @Override
                    public void onGlobalLayout() {

                        final int MARGIN = 1;    // 5

                        int pLength;
                        int pWidth = myGridLayout.getWidth();
                        int pHeight = myGridLayout.getHeight();

                        //Set myGridLayout equal width and height
                        if(pWidth<=pHeight){
                            pLength = pHeight;
                        }else{
                            pLength = pWidth;
                        }

                        int numOfCol = myGridLayout.getColumnCount();
                        int numOfRow = myGridLayout.getRowCount();
//                        int w = pWidth/numOfCol;
//                        int h = pHeight/numOfRow;

                        int w = pLength/numOfCol;
                        int h = pLength/numOfCol;   //numOfRow

                        for(int yPos=0; yPos<numOfRow; yPos++){
                            for(int xPos=0; xPos<numOfCol; xPos++){

                                GridLayout.LayoutParams params = (GridLayout.LayoutParams)textView[yPos*numOfCol + xPos].getLayoutParams();
                                params.width = w - 2*MARGIN;
                                params.height = h - 2*MARGIN;
                                params.setMargins(MARGIN, MARGIN, MARGIN, MARGIN);
//                                ticketBoardView[yPos*numOfCol + xPos].setLayoutParams(params);
                                textView[yPos*numOfCol + xPos].setLayoutParams(params);
                            }
                        }

                    }});

        myGridLayout.setBackgroundResource(R.drawable.rounded_no_bg_box);

//        verifyStoragePermissions(getActivity());
        takeScreenshot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                verifyStoragePermissions(getActivity());
                takeShot(view);
//                takeShot();
            }
        });

        return view;
    }

    private void setClickEventForAds() {
            final int bannerAdPos = publicAds.adBannerItemPosition;
            adBanner.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                /*String url = publicAds.getAdvertizes().get(publicAds.adBannerItemPosition).getMediaUrl();
                Intent viewIntent =
                        new Intent("android.intent.action.VIEW",
                                Uri.parse(url));
                startActivity(viewIntent);*/

                    try {
                    String url = banner_ad.getAdUrl();
                    redirectToLink(url);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });

            simpleExoPlayerView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    try {
                        String url = publicAds.getAdvertizes().get(bannerAdPos).getMediaUrl();
                        redirectToLink(url);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
    }

    private void redirectToLink(String url) {
        Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        i.setPackage("com.android.chrome");
        try {
            startActivity(i);
        } catch (ActivityNotFoundException e) {
            // Chrome is probably not installed
            // Try with the default browser
            i.setPackage(null);
            startActivity(i);
        }
    }

    private static void verifyStoragePermissions(Activity activity) {
        // Check if we have write permission
        int permission = ActivityCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if (permission != PackageManager.PERMISSION_GRANTED) {
            // We don't have permission so prompt the user
            ActivityCompat.requestPermissions(
                    activity,
                    PERMISSIONS_STORAGE,
                    REQUEST_EXTERNAL_STORAGE
            );
        } else {
            Toast.makeText(activity, "Screenshot taken check your gallery", Toast.LENGTH_SHORT).show();
        }
    }

    private void takeShot(View view) {
        View v = view.getRootView();
        v.setDrawingCacheEnabled(true);
        Bitmap b = v.getDrawingCache();
//        String extr = Environment.getExternalStorageDirectory().toString();
        String extr = Environment.getExternalStorageDirectory().getPath()+getResources()+"/TheCGNow";

        File direct = new File(Environment.getExternalStorageDirectory() + "/TheCGNow");
        String fileName = "TICKET_SHOT_"+Calendar.getInstance().getTimeInMillis()+".jpg";

        if (!direct.exists()) {
            File wallpaperDirectory = new File("/sdcard/TheCGNow/");
            wallpaperDirectory.mkdirs();
        }

//        File myPath = new File(extr, fileName);  //getString(R.string.free_tiket)

        File file = new File("/sdcard/TheCGNow/", fileName);
        if (file.exists()) {
            file.delete();
        }

        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(file);
            b.compress(Bitmap.CompressFormat.JPEG, 100, fos);
            Log.d(TAG, "file:"+file.getPath());
            fos.flush();
            fos.close();
//            openScreenshot(file);
            showNotification(b, file);
            galleryAddPic(file);
//            MediaStore.Images.Media.insertImage( getActivity().getContentResolver(), b, "Screen", "screen");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void galleryAddPic(File file) {
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        File f = new File(file.getPath());
        Uri contentUri = Uri.fromFile(f);
        mediaScanIntent.setData(contentUri);
        getActivity().sendBroadcast(mediaScanIntent);
    }

    private void showNotification(Bitmap b, File file) {

        Intent activityIntent = new Intent(getActivity(), HousieBoardActivity.class);

        PendingIntent contentIntent = PendingIntent.getActivity(getActivity(), 0, activityIntent, 0);

        Intent broadcastIntent = new Intent(getActivity(), NotificationReceiver.class);
        Uri uri = Uri.fromFile(file);
        Log.d(TAG, "show notification imageurl:"+uri+" filepath:"+file.getPath());
//        broadcastIntent.putExtra("imageuri", uri.toString());
        broadcastIntent.putExtra("imageuri", file.getName());
        PendingIntent actionIntent = PendingIntent.getBroadcast(getActivity(), 0, broadcastIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationManager manager  = getActivity().getSystemService(NotificationManager.class);

        Notification notification = new NotificationCompat.Builder(getActivity())
                .setSmallIcon(R.drawable.app_icon)
                .setContentTitle("Screenshot Captured")
                .setContentText("this is text message")
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setCategory(NotificationCompat.CATEGORY_MESSAGE)
                .setColor(Color.BLUE)
                .setContentIntent(contentIntent)
                .setAutoCancel(true)
                .setOnlyAlertOnce(true)
                .addAction(R.drawable.ic_view_eye, "Open Gallery", actionIntent)
                .build();

                manager.notify(1, notification);
    }

    private void generateNewTicket() {
            if (mDB.tambolaDao().getTicket() == null) {
                generateTicket();
            } else {
//                int dayCount = getDayDiffrence();    // mDB.tambolaDao().getTicket().getTicketDate().equals(getCurrentDate())
                boolean isTodayTicket = mDB.tambolaDao().getTicket().getTicketDate().equals(getCurrentDate());
                Calendar calendar = Calendar.getInstance();

                Date date = calendar.getTime();

                if (!isTodayTicket) {
                    sharedPreferenceConfig.writeTicketGenerated(false);
                    Log.d(TAG, "fungentic: isTodayTicket="+isTodayTicket+" hour="+date.getHours());
                }

                if (sharedPreferenceConfig.readTicketGenerated()) {  //check if ticket is already generated
                    Log.d(TAG, "retrieve ticket from db");
                    retrieveTicketFromDB();
                } else {
//                    mDB.tambolaDao().deletePreviousTicket(getCurrentDate());
                    mDB.tambolaDao().deletePreviousTicket();
                    Log.d(TAG, "generate ticket sp:"+sharedPreferenceConfig.readTicketGenerated());
                    generateTicket();
                }
            }

            sharedPreferenceConfig.writeTicketGenerated(true);
            Log.d(TAG, "gen sp:"+sharedPreferenceConfig.readTicketGenerated());

    }

    private void showNotificationTicketGenerated() {
        Log.d(TAG, "ticket generated notification");
        Intent intent = new Intent(getActivity(), HousieBoardActivity.class);
//        intent.putExtra("fragToLoad", Config.POS_NOTI_FRAG);

        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        PendingIntent pendingIntent = PendingIntent.getActivity(getActivity(), 0 /* Request code */, intent,

                PendingIntent.FLAG_ONE_SHOT);



//        String channelId = getString(R.string.default_notification_channel_id);

        String channelId = "ch1";

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        Context context = getActivity();

        NotificationCompat.Builder notificationBuilder =

                new NotificationCompat.Builder(getActivity(), channelId)

                        .setSmallIcon(R.drawable.app_icon_noti, View.INVISIBLE)

                        .setLargeIcon(BitmapFactory.decodeResource(context.getResources(),
                                R.mipmap.ic_launcher))

                        .setContentTitle("Todays tambola ticket is generated")

                        .setContentText("Check what lucky no's you have got")

//                        .setAutoCancel(true)

                        .setSound(defaultSoundUri)

                        .setContentIntent(pendingIntent);



        NotificationManager notificationManager = (NotificationManager) getActivity().getSystemService(Context.NOTIFICATION_SERVICE);


        // Since android Oreo notification channel is needed.

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

            NotificationChannel channel = new NotificationChannel(channelId,

                    "Channel human readable title",

                    NotificationManager.IMPORTANCE_DEFAULT);

            notificationManager.createNotificationChannel(channel);

        }

        notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());
        Log.d(TAG, "ticket generated notification end");
    }

    private void initBubbleGrid() {

        customGridAdapter=new CustomGridAdapter(numbers, getActivity());
        bubbleGrid.setAdapter(customGridAdapter);

        String get_query = "?auth_key="+sharedPreferenceConfig.readFcmToken();
        Log.d(TAG, "urlquery="+Config.GET_TAMBOLA_NUMBERS_URL+get_query);
        StringRequest stringRequest = new StringRequest(com.android.volley.Request.Method.GET, Config.GET_TAMBOLA_NUMBERS_URL+get_query,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        numbers.clear();

                        Log.d(TAG,response);

                        try {
                            //Creating the json object from the response
                            JSONObject jsonResponse = new JSONObject(response);
                            JSONArray jsonArray = jsonResponse.getJSONArray("data");
                            Log.d(TAG,jsonArray.toString());

                            mDB.numberDao().deleteAllNumbers();
                            NumberRoom nr=new NumberRoom();
                            nr.setTicketDate(getCurrentDate());

                            for (int i=0; i<jsonArray.length(); i++) {
                                JSONObject json = jsonArray.getJSONObject(i);
                                numbers.add(json.getInt("number"));
                                nr.setTicketNum(json.getInt("number"));
                                mDB.numberDao().insertTambolaNumber(nr);
                            }

                            sortNumbers();
                            Log.d(TAG, "numbers from server:"+numbers.toString());
                            customGridAdapter=new CustomGridAdapter(numbers, getActivity());
                            bubbleGrid.setAdapter(customGridAdapter);

                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.d("mobileerror",""+e);

                        } catch (IndexOutOfBoundsException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
//                        Toast.makeText(mCtx, getResources().getString(R.string.couldnot_connect),Toast.LENGTH_LONG).show();
                        if (numbers.isEmpty()) {
                            mDB.numberDao().deletePrevSessionTambolaNumbers(getCurrentDate());
                            List<NumberRoom> nrs = mDB.numberDao().getTambolaNumbersByDate(getCurrentDate());
                            Log.d(TAG, "numbers before:" + numbers.toString());
                            for (NumberRoom numberRoom : nrs) {
//            if (!numbers.contains(numberRoom.getTicketNum()))
                                numbers.add(numberRoom.getTicketNum());
                            }

                            sortNumbers();
                            Log.d(TAG, "numbers:" + numbers.toString());
                            customGridAdapter = new CustomGridAdapter(numbers, getActivity());
                            bubbleGrid.setAdapter(customGridAdapter);
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                //Adding the parameters to the request
                params.put("auth_key", sharedPreferenceConfig.readFcmToken());
                return params;
            }

            @Override
            protected Response<String> parseNetworkResponse(NetworkResponse response) {
                try {
                    Cache.Entry cacheEntry = HttpHeaderParser.parseCacheHeaders(response);
                    if (cacheEntry == null) {
                        cacheEntry = new Cache.Entry();
                    }
                    final long cacheHitButRefreshed = 1 * 60 * 1000; // in 1 minutes cache will be hit, but also refreshed on background
                    final long cacheExpired = 24 * 60 * 60 * 1000; // in 24 hours this cache entry expires completely
                    long now = System.currentTimeMillis();
                    final long softExpire = now + cacheHitButRefreshed;
                    final long ttl = now + cacheExpired;
                    cacheEntry.data = response.data;
                    cacheEntry.softTtl = softExpire;
                    cacheEntry.ttl = ttl;
                    String headerValue;
                    headerValue = response.headers.get("Date");
                    if (headerValue != null) {
                        cacheEntry.serverDate = HttpHeaderParser.parseDateAsEpoch(headerValue);
                    }
                    headerValue = response.headers.get("Last-Modified");
                    if (headerValue != null) {
                        cacheEntry.lastModified = HttpHeaderParser.parseDateAsEpoch(headerValue);
                    }
                    cacheEntry.responseHeaders = response.headers;
                    final String jsonString = new String(response.data,
                            HttpHeaderParser.parseCharset(response.headers));
                    return Response.success(jsonString, cacheEntry);
                } catch (Exception e) {
                    return Response.error(new ParseError(e));
                }
            }
        };

        //Adding request the the queue
        requestQueue.add(stringRequest);
    }

    private void sortNumbers() {
        Collections.sort(numbers);
    }

    private void initBubbleGridInPusherThread(CustomGridAdapter customGridAdapter) {
        mDB.numberDao().deletePrevSessionTambolaNumbers(getCurrentDate());
        List<NumberRoom> nrs = mDB.numberDao().getTambolaNumbersByDate(getCurrentDate());
        for (NumberRoom numberRoom : nrs) {
            numbers.add(numberRoom.getTicketNum());
        }

        CustomGridAdapter cga=customGridAdapter;

        bubbleGrid.setAdapter(cga);

    }

    private void setAdvertise() {
       /* updateBannerAd();
        updateLabelAd();*/
        String get_query = "?latitude="+sharedPreferenceConfig.readLatitude()+"&longitude="+sharedPreferenceConfig.readLongitude()+"&ad_page="+sharedPreferenceConfig.readAdPage()+"&auth_key="+sharedPreferenceConfig.readFcmToken();
        Log.d(TAG, "urlquery="+Config.GET_ARTICLE_DESC_ADS+get_query);
        StringRequest stringRequest = new StringRequest(com.android.volley.Request.Method.GET, Config.GET_ARTICLE_DESC_ADS+get_query,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

//                        progressBar.setVisibility(View.GONE);

                        Log.d(TAG,"res="+response);
//                        Toast.makeText(AttendenceActivity.this, "attendance response"+response, Toast.LENGTH_SHORT).show();
                        try {
                            //Creating the json object from the response
                            JSONObject jsonResponse = new JSONObject(response);
                            JSONArray jsonArray = jsonResponse.getJSONArray("bannerdata");
                            JSONArray jsonArray2 = jsonResponse.getJSONArray("labeldata");
                            Log.d(TAG,"data:"+jsonArray.toString());
//                            Toast.makeText(AttendenceActivity.this, ""+jsonArray.toString(), Toast.LENGTH_SHORT).show();

                            for (int i=0; i<jsonArray.length(); i++) {
                                JSONObject jsonObject =jsonArray.getJSONObject(i);
                                banner_ad = new Advertize(jsonObject.getInt("advertise_id"),
                                        jsonObject.getInt("cat_id"),
                                        jsonObject.getInt("region_id"),
                                        jsonObject.getString("title"),
                                        Config.BASE_URL+jsonObject.getString("media_url"),
                                        jsonObject.getString("media_type"),
                                        jsonObject.getString("date"),
                                        jsonObject.getInt("ad_time"),
                                        jsonObject.getString("ad_url"));
                            }

                            /*for (int i=0; i<jsonArray2.length(); i++) {
                                JSONObject jsonObject =jsonArray2.getJSONObject(i);
                                labelAd = new Advertize(jsonObject.getInt("advertise_id"),
                                        jsonObject.getInt("cat_id"),
                                        jsonObject.getInt("region_id"),
                                        jsonObject.getString("title"),
                                        Config.BASE_URL+jsonObject.getString("media_url"),
                                        jsonObject.getString("media_type"),
                                        jsonObject.getString("date"),
                                        jsonObject.getInt("ad_time"),
                                        jsonObject.getString("ad_url"));
                            }*/

                            setAdvertiseUI();


                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.d(TAG,"mobileerror: "+e);
//                            Toast.makeText(getActivity(), ""+e.toString(), Toast.LENGTH_SHORT).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {


//                        loading.dismiss();
//                        progressBar.setVisibility(View.GONE);
                        Log.d(TAG, error.getMessage()+" tostr:"+error.toString());
                        Toast.makeText(mCtx, getResources().getString(R.string.couldnot_connect),Toast.LENGTH_LONG).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                //Adding the parameters to the request
//                params.put("id", id);
                params.put("auth_key", sharedPreferenceConfig.readFcmToken());
                return params;
            }


        };

        //Adding request the the queue
        requestQueue.add(stringRequest);
    }

    private void setAdvertiseUI() {
        Picasso.get().load(banner_ad.getMediaUrl()).placeholder(R.drawable.placeholder).into(adBanner);
    }

    private void updateLabelAd() {
        ArrayList<Advertize> templist=new ArrayList<>();
        for (Advertize ad : publicAds.getTambola_label_ads()) {
            if (ad.getRegionId()==sharedPreferenceConfig.readLocationId()) {
                templist.add(ad);
            }
        }
        ArrayList<Advertize> tambola_label_ads = templist;
        Random random = new Random();
        int adSize = tambola_label_ads.size();
        if (adSize>0) {
            int randomNumber1 = random.nextInt(adSize), randomNumber2 = random.nextInt(adSize);
        }
    }

    private void updateBannerAd() {
        ArrayList<Advertize> templist=new ArrayList<>();
        for (Advertize ad : publicAds.getTambola_banner_ads()) {
            if (ad.getRegionId()==sharedPreferenceConfig.readLocationId()) {
                templist.add(ad);
            }
        }
        ArrayList<Advertize> tambola_banner_ads = templist;
        Log.d(TAG, "bannerads="+tambola_banner_ads);
        Random random = new Random();
        int adSize = tambola_banner_ads.size();
        if (adSize>0) {
            int randomNumber = random.nextInt(adSize);
            banner_ad = tambola_banner_ads.get(randomNumber);
            switch (banner_ad.getMediaType()) {
                case "BIMG":
                    rkLayout.setVisibility(View.VISIBLE);
                    linearLayoutAd.setVisibility(View.GONE);
                    Picasso.get().load(banner_ad.getMediaUrl()).placeholder(R.drawable.placeholder).into(adBanner);
                    break;
                case "BGIF":
                    rkLayout.setVisibility(View.VISIBLE);
                    linearLayoutAd.setVisibility(View.GONE);
                    Glide.with(this)
                            .asGif()
                            .load(banner_ad.getMediaUrl())
                            .placeholder(R.drawable.placeholder)
                            .into(adBanner);
                    break;
                case "BVID":
                    rkLayout.setVisibility(View.GONE);
                    linearLayoutAd.setVisibility(View.VISIBLE);
                    Log.d(TAG, "video ad called");
                    try {
                        BandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
                        TrackSelector trackSelector = new DefaultTrackSelector(new AdaptiveTrackSelection.Factory(bandwidthMeter));
//                        TrackSelector trackSelector = null;
                        simpleExoPlayer = ExoPlayerFactory.newSimpleInstance(getActivity(), trackSelector);
                        DefaultHttpDataSourceFactory dataSourceFactory = new DefaultHttpDataSourceFactory("exoplayer_video");
                        ExtractorsFactory extractorsFactory = new DefaultExtractorsFactory();
                        MediaSource mediaSource = new ExtractorMediaSource(Uri.parse(banner_ad.getMediaUrl()), dataSourceFactory, extractorsFactory, null, null);
                        simpleExoPlayerView.setPlayer(simpleExoPlayer);
                        simpleExoPlayer.prepare(mediaSource);
                        simpleExoPlayer.setPlayWhenReady(true);
                        simpleExoPlayer.setRepeatMode(ExoPlayer.REPEAT_MODE_ALL);
                        simpleExoPlayer.setVolume(0f);

                        simpleExoPlayerView.addOnAttachStateChangeListener(new View.OnAttachStateChangeListener() {
                            @Override
                            public void onViewAttachedToWindow(View view) {

                            }

                            @Override
                            public void onViewDetachedFromWindow(View view) {
                                if (simpleExoPlayer != null) {
                                    simpleExoPlayer.setPlayWhenReady(false);
                                }
                            }
                        });
                    } catch (Exception e) {
                        Log.d(TAG, "exoplayer error: " + e.toString());
                    }
                    break;
            }
        }
    }

    private void registerBroadcastReceiver() {
        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                    refreshAds();
            }
        };
        getActivity().registerReceiver(broadcastReceiver, new IntentFilter("com.example.thecgnow.activity.adRefresh"));


        broadcastReceiver2 = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                myHandler.sendEmptyMessage(1);
                number = intent.getExtras().getString("com.example.thecgnow.tambola_number.number", "");
//                number="5";
            }
        };
        getActivity().registerReceiver(broadcastReceiver2, new IntentFilter("com.example.thecgnow.tambola_number.generated"));
    }

    private void refreshAds() {
            /*Picasso.with(this).load(publicAds.getLabelAdvertizes().get(publicAds.labelAdItemPosition).getMediaUrl()).into(adLabel);
            Picasso.with(this).load(publicAds.getAdvertizes().get(publicAds.adBannerItemPosition).getMediaUrl()).into(adBanner);*/
        setAdvertise();
    }

    private void retrieveTicketFromDB() {

        try {
//            List<TambolaRoom> data = mDB.tambolaDao().getTicketNumbers(curDate);
            String ticketGenerationDate = mDB.tambolaDao().getTicket().getTicketDate();
            Log.d(TAG, "ticket date:"+ticketGenerationDate);
            List<TambolaRoom> data = mDB.tambolaDao().getTicketNumbers(ticketGenerationDate);
            Gson gson = new Gson();
            Log.d(TAG, "tambola ticket:"+gson.toJson(data));
            for (int row = 0; row < 3; row++) {
                for (int col = 0; col < 9; col++) {
                    int ticketNum = data.get(row * 9 + col).getTicketNum();
                    if (ticketNum != 0)
                        textView[row * 9 + col].setText(ticketNum + "");
                    else
                        textView[row * 9 + col].setText(" ");

                    Log.d(TAG, "data check: " + data.get(row * 9 + col).getChecked());

                    if (mDB.tambolaDao().getChecked(ticketNum) == 1) {
                        textView[row * 9 + col].performClick();
                        Log.d(TAG, "pos: " + (row * 9 + col) + " clicked.");
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.d(TAG, "ticket error");
        }
    }

    private void initDB() {
        publicAds=(TheCGNowApp)getActivity().getApplicationContext();
        publicAds.initDB(getActivity().getApplicationContext());
        mDB=publicAds.getDB(getActivity().getApplicationContext());
    }

    private void focusOnView(){
        new Handler().post(new Runnable() {
            @Override
            public void run() {
//                mainScrollView.smoothScrollTo(0, ticketNum.getTop());
                ObjectAnimator.ofInt(mainScrollView, "scrollY",  ticketNum.getTop()).setDuration(1000).start();
            }
        });
    }

    private void setHandler() {
        myHandler = new Handler() {
            public void handleMessage(Message msg) {
                final int what = msg.what;
                if (!number.equals("null"))
                doUpdate();
                switch(what) {
                    case 1:
                        Log.d(TAG, "hi i am 1");
                        break;

                }
            }
        };
    }

    private void doUpdate() {
        try {
            focusOnView();
//            if (!number.equals("null") && !numbers.contains(Integer.parseInt(number))) {
            TheCGNowApp.currentTambolaNum = number;
                updateBubbleGrid(number);
//            }
            sharedPreferenceConfig.writeTicketNum(Integer.parseInt(number));
            ticketNum.setText(number);
            Log.d(TAG, "ticketnumber"+number+" "+ticketNum.getText().toString());
            playNotificationSound();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void playNotificationSound() {
        try {

            // find the sound from raw folder of this app
            Uri alarmSound = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE
                    + "://" + getActivity().getPackageName() + "/raw/notification");
            Ringtone r = RingtoneManager.getRingtone(getActivity(), alarmSound);
            r.play();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void updateBubbleGrid(final String number) {
        numbers.add(Integer.parseInt(number));
        sortNumbers();
//        initBubbleGridInPusherThread(customGridAdapter);

       /* Activity activity = (HousieBoardActivity)mCtx;
            if (mCtx instanceof HousieBoardActivity) {
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {*/
                        Log.d(TAG, "uiThreadRunning..."+numbers.toString());
//                        customGridAdapter.notifyDataSetChanged();
                        callback(number);
                        Log.d(TAG, "uiThreadRun complete");
              /*      }
                });
            } else {

            }
*/
//        callback(number);
        NumberRoom nr=new NumberRoom();
        nr.setTicketDate(getCurrentDate());

        nr.setTicketNum(Integer.parseInt(number));
        mDB.numberDao().insertTambolaNumber(nr);
        Log.d(TAG, "updated number="+number+" numbers are: "+numbers.toString());
    }

    private void generateTicket() {

        String get_query = "?latitude="+sharedPreferenceConfig.readLatitude()+"&longitude="+sharedPreferenceConfig.readLongitude()+"&auth_key="+sharedPreferenceConfig.readFcmToken();
        Log.d(TAG, "urlquery= "+Config.GENERATE_TAMBOLA_TICKET_URL+get_query);
        StringRequest stringRequest = new StringRequest(com.android.volley.Request.Method.GET, Config.GENERATE_TAMBOLA_TICKET_URL+get_query,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        Log.d(TAG,"response:"+response);
//                        Toast.makeText(AttendenceActivity.this, "attendance response"+response, Toast.LENGTH_SHORT).show();
                        try {
                            //Creating the json object from the response
                            JSONObject jsonResponse = new JSONObject(response);
                            JSONArray jsonArray = jsonResponse.getJSONArray("data");
                            dateStr = jsonResponse.getString("issuedDate");
                            Log.d("jsondatabatch",jsonArray.toString());
                            sharedPreferenceConfig.writeResponseData(Config.ticketIdKey, jsonResponse.getString("ticketId"));

                            updateTicket(jsonArray);

                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.d("mobileerror",""+e);
//                            Toast.makeText(getActivity(), ""+e.toString(), Toast.LENGTH_SHORT).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {

                        String message = null;
                        if (volleyError instanceof NetworkError) {
                            message = "Cannot connect to Internet...Please check your connection!";
                        } else if (volleyError instanceof ServerError) {
                            message = "The server could not be found. Please try again after some time!!";
                        } else if (volleyError instanceof AuthFailureError) {
                            message = "Cannot connect to Internet...Please check your connection!";
                        } else if (volleyError instanceof ParseError) {
                            message = "Parsing error! Please try again after some time!!";
                        } else if (volleyError instanceof NoConnectionError) {
                            message = "Cannot connect to Internet...Please check your connection!";
                        } else if (volleyError instanceof TimeoutError) {
                            message = "Connection TimeOut! Please check your internet connection.";
                        }

                        if (message!=null) {
                            try {
                                Toast.makeText(mCtx, getResources().getString(R.string.couldnot_connect),Toast.LENGTH_LONG).show();
                            } catch (IllegalStateException e) {
                                e.printStackTrace();
                            }
                        }
//                        Toast.makeText(mCtx, mCtx.getResources().getString(R.string.couldnot_connect),Toast.LENGTH_LONG).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                //Adding the parameters to the request
                params.put("auth_key", sharedPreferenceConfig.readFcmToken());
                return params;
            }

        };

        //Adding request the the queue
        requestQueue.add(stringRequest);

//        showNotificationTicketGenerated();
        //"data":[4,0,25,0,47,0,65,73,0,2,11,27,0,49,0,0,75,0,0,0,0,35,0,58,0,0,80],"issuedDate":"05-Nov-2020","ticketId":1604576195}
    }

    private void updateTicket(JSONArray jsonArray) {
        ticketId.setText("Tambola Ticket #"+sharedPreferenceConfig.readResponseData(Config.ticketIdKey));
        try {
            for (int i=0; i<jsonArray.length(); i++) {
                int num = jsonArray.getInt(i);
                TambolaRoom tambolaRoom = new TambolaRoom();
                tambolaRoom.setTicketNum(num);
                tambolaRoom.setTicketDate(dateStr);
                tambolaRoom.setChecked(0);
                mDB.tambolaDao().insertTicketNum(tambolaRoom);
            }

            retrieveTicketFromDB();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private String getCurrentDate() {
        Date c = Calendar.getInstance().getTime();
        System.out.println("Current time => " + c);

        Locale locale = new Locale("en");
        SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy", Locale.US);
        return df.format(c);
    }

    private void initView(View view) {
        myGridLayout=view.findViewById(R.id.ticket_grid);
        ticketNum=view.findViewById(R.id.ticket_num_txt);
        mainScrollView=view.findViewById(R.id.main_scrollView);
        adBanner=view.findViewById(R.id.ad_banner);
        bubbleGrid=view.findViewById(R.id.bubble_grid);
        bubbleGrid.setExpanded(true);
        takeScreenshot=view.findViewById(R.id.take_screenshot);
        simpleExoPlayerView=view.findViewById(R.id.ad_banner_video);
        rkLayout=view.findViewById(R.id.image_ad_banner_rkl);
        linearLayoutAd=view.findViewById(R.id.video_ad_banner_ll);
        ticketId=view.findViewById(R.id.ticket_id);
    }

    @Override
    public void OnToggled(TicketBoardView v, boolean touchOn) {

        //get the id string
        String idString = v.getIdX() + ":" + v.getIdY();

        Toast.makeText(getActivity(),
                "Toogled:\n" +
                        idString + "\n" +
                        touchOn,
                Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onClick(View view) {
        int tag = Integer.parseInt(view.getTag().toString());
        TicketTextView tv=(TicketTextView) view;
//        focusOnView();
//        Log.d(TAG, "onclick called");

        int numOfCol = myGridLayout.getColumnCount();
        int numOfRow = myGridLayout.getRowCount();
        int pos, x, y;
//        x=view.get
        x=tv.getIdX();
        y=tv.getIdY();
        pos=y*numOfCol+x;
//        tv.setText("hello");
        tv.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
        boolean touchOn=tv.isTouchOn();   // false
       /* if (!touchOn && !(tv.getText().toString().isEmpty() || tv.getText().toString().equals(" ") || ticketNum.getText().toString().isEmpty())) {
            if (Integer.parseInt(tv.getText().toString())==Integer.parseInt(ticketNum.getText().toString())) {
                tv.setBackgroundColor(Color.RED);
            }
        } else {
            tv.setBackgroundColor(Color.WHITE);
        }*/

//        Log.d(TAG, "onclick called22");
        try {
//            Log.d(TAG, "onclick called23");
            String date = mDB.tambolaDao().getTicket().getTicketDate();
            int ticketNum = mDB.tambolaDao().getTicketNumbers(date).get(pos).getTicketNum();
//            Log.d(TAG, "onclick called24");
            Log.d(TAG, "ticket num: " + ticketNum+" ticket text:"+tv.getText().toString()+" end");
            if (!touchOn && !tv.getText().toString().equals(" ")) {
                tv.setBackgroundColor(Color.GREEN);
                if (ticketNum != 0)
                    mDB.tambolaDao().setChecked(ticketNum, 1);
            } else {
                tv.setBackgroundColor(Color.WHITE);
                if (ticketNum != 0)
                    mDB.tambolaDao().setChecked(ticketNum, 0);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        tv.setTouchOn(!touchOn);

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        getActivity().unregisterReceiver(broadcastReceiver);
        isActivityCurrent=false;
    }

    @Override
    public void callback(String number) {
        bubbleGrid.setAdapter(customGridAdapter);
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        mCtx = context;
    }

    private final class TambolaWebSocket extends WebSocketListener {
        private static final int NORMAL_CLOSURE_STATUS=1000;

        @Override
        public void onOpen(@NotNull WebSocket webSocket, @NotNull okhttp3.Response response) {
            super.onOpen(webSocket, response);
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(getActivity(),"opened",Toast.LENGTH_LONG).show();
                }
            });
        }

        @Override
        public void onClosing(@NotNull WebSocket webSocket, int code, @NotNull String reason) {
            //super.onClosing(webSocket, code, reason);
            webSocket.close(NORMAL_CLOSURE_STATUS,null);
//        output("Closing : " + code + "/" + reason);
        }

        @Override
        public void onMessage(@NotNull WebSocket webSocket, @NotNull String text) {
            //super.onMessage(webSocket, text);
            Log.d("text",text);
            output(text);
        }

        @Override
        public void onFailure(@NotNull WebSocket webSocket, @NotNull Throwable t, @org.jetbrains.annotations.Nullable okhttp3.Response response) {
            super.onFailure(webSocket, t, response);
            t.printStackTrace();
        }

        @Override
        public void onMessage(@NotNull WebSocket webSocket, @NotNull ByteString bytes) {
            //super.onMessage(webSocket, bytes);
            Log.d("text",bytes.hex());
            output(bytes.hex());
        }
    }

    private void startWebSocket() {
        try {
            JSONObject websocketData = new JSONObject(sharedPreferenceConfig.readResponseData(Config.webSocketKey));
            Request request = null;
            Log.d("url",websocketData.getString("data"));
            request = new Request.Builder().url(websocketData.getString("data")).build();
            listener = new TambolaWebSocket();
            ws = client.newWebSocket(request,listener);
            client.dispatcher().executorService().shutdown();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    private void output(final String s){
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
//                callback(s);
                Toast.makeText(getActivity(),s.toString(),Toast.LENGTH_LONG).show();
                ticketNum.setText(s);
//                myHandler.sendEmptyMessage(1);
            }
        });
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            client = new OkHttpClient();
            sharedPreferenceConfig = new SharedPreferenceConfig(getContext());
            startWebSocket();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
