package com.newsapp.app.thecgnow2.model;

import com.google.gson.annotations.SerializedName;

public class JobList {
    @SerializedName("id")
    private int id;

    @SerializedName("region_id")
    int regionId;

    @SerializedName("job_category_id")
    int categoryId;

    @SerializedName("job_title")
    private String jobTitle;

    @SerializedName("contact_no")
    String contact;

    @SerializedName("job_description")
    String discription;

    @SerializedName("ad_url")
    String adUrl;

    public JobList(int id, int regionId, int categoryId, String jobTitle, String contact, String discription, String adUrl) {
        this.id = id;
        this.regionId = regionId;
        this.categoryId = categoryId;
        this.jobTitle = jobTitle;
        this.contact = contact;
        this.discription = discription;
        this.adUrl = adUrl;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getRegionId() {
        return regionId;
    }

    public void setRegionId(int regionId) {
        this.regionId = regionId;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    public String getJobTitle() {
        return jobTitle;
    }

    public void setJobTitle(String jobTitle) {
        this.jobTitle = jobTitle;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getDiscription() {
        return discription;
    }

    public void setDiscription(String discription) {
        this.discription = discription;
    }

    public String getAdUrl() {
        return adUrl;
    }

    public void setAdUrl(String adUrl) {
        this.adUrl = adUrl;
    }
}
