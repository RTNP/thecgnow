package com.newsapp.app.thecgnow2.activity.fragments.housie;

import android.app.DatePickerDialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.Cache;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.newsapp.app.thecgnow2.Config;
import com.newsapp.app.thecgnow2.R;
import com.newsapp.app.thecgnow2.SharedPreferenceConfig;
import com.newsapp.app.thecgnow2.adapter.WinnerListAdapter;
import com.newsapp.app.thecgnow2.model.Job;
import com.newsapp.app.thecgnow2.model.Winner;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class WinnersFragment extends Fragment implements View.OnClickListener {
    
    LinearLayout selectDate;
    TextView dateTxt;
    Calendar myCalendar;
    Context mCtx;

    String TAG = "WinnersFragment";

    ArrayList<Winner> winnerList = new ArrayList<>();
    private RequestQueue requestQueue;
    private SharedPreferenceConfig sharedPreferenceConfig;

    RecyclerView recyclerView;
    WinnerListAdapter mAdapter;

    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_winners, container, false);
        
        initView(view);


        selectDate.setOnClickListener(this);
        setUpRecyclerView();
        requestQueue = Volley.newRequestQueue(getActivity());
        sharedPreferenceConfig = new SharedPreferenceConfig(mCtx);
        fetchData("");
        return view;
    }

    private void fetchData(final String date) {
        String get_query = "?latitude="+sharedPreferenceConfig.readLatitude()+"&longitude="+sharedPreferenceConfig.readLongitude()+"&auth_key="+sharedPreferenceConfig.readFcmToken();
        if (!date.isEmpty()) {
            get_query+="&date="+date;
        }
        Log.d(TAG, "urlquery="+Config.GET_WINNER_LIST_URL+get_query);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, Config.GET_WINNER_LIST_URL+get_query,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        Log.d(TAG,response);
                        winnerList.clear();
//                        Toast.makeText(AttendenceActivity.this, "attendance response"+response, Toast.LENGTH_SHORT).show();
                        try {
                            //Creating the json object from the response
                            JSONObject jsonResponse = new JSONObject(response);
                            JSONArray jsonArray = jsonResponse.getJSONArray("data");
                            Log.d("TAG",jsonArray.toString());
//                            Toast.makeText(AttendenceActivity.this, ""+jsonArray.toString(), Toast.LENGTH_SHORT).show();

                            String sourceVal, articleId;
                            for (int i=0; i<jsonArray.length(); i++) {
                                JSONObject j =jsonArray.getJSONObject(i);

                                winnerList.add(new Winner(j.getInt("winner_id"),
                                        j.getInt("prize"),
                                        j.getString("name"),
                                        j.getString("date")));
                            }
                            mAdapter.notifyDataSetChanged();
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.d("mobileerror",""+e);
//                            Toast.makeText(getActivity(), ""+e.toString(), Toast.LENGTH_SHORT).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {

                        String message = null;
                        if (volleyError instanceof NetworkError) {
                            message = "Cannot connect to Internet...Please check your connection!";
                        } else if (volleyError instanceof ServerError) {
                            message = "The server could not be found. Please try again after some time!!";
                        } else if (volleyError instanceof AuthFailureError) {
                            message = "Cannot connect to Internet...Please check your connection!";
                        } else if (volleyError instanceof ParseError) {
                            message = "Parsing error! Please try again after some time!!";
                        } else if (volleyError instanceof NoConnectionError) {
                            message = "Cannot connect to Internet...Please check your connection!";
                        } else if (volleyError instanceof TimeoutError) {
                            message = "Connection TimeOut! Please check your internet connection.";
                        }

                        if (message!=null) {
//                            Toast.makeText(mCtx, mCtx.getResources().getString(R.string.couldnot_connect),Toast.LENGTH_LONG).show();
                        }
//                        Toast.makeText(mCtx, mCtx.getResources().getString(R.string.couldnot_connect),Toast.LENGTH_LONG).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                //Adding the parameters to the request
                params.put("auth_key", sharedPreferenceConfig.readFcmToken());
                if (!date.isEmpty()) {
                    params.put("date", date);
                    Log.d(TAG, "date is set");
                } else {
                    Log.d(TAG, "date is empty");
                }
                return params;
            }

           /* @Override
            protected Response<String> parseNetworkResponse(NetworkResponse response) {
                try {
                    Cache.Entry cacheEntry = HttpHeaderParser.parseCacheHeaders(response);
                    if (cacheEntry == null) {
                        cacheEntry = new Cache.Entry();
                    }
                    final long cacheHitButRefreshed =  2 * 60 * 1000; // 10 * 60 * 1000 in 10 minutes cache will be hit, but also refreshed on background
                    final long cacheExpired = 24 * 60 * 60 * 1000; // in 24 hours this cache entry expires completely
                    long now = System.currentTimeMillis();
                    final long softExpire = now + cacheHitButRefreshed;
                    final long ttl = now + cacheExpired;
                    cacheEntry.data = response.data;
                    cacheEntry.softTtl = softExpire;
                    cacheEntry.ttl = ttl;
                    String headerValue;
                    headerValue = response.headers.get("Date");
                    if (headerValue != null) {
                        cacheEntry.serverDate = HttpHeaderParser.parseDateAsEpoch(headerValue);
                    }
                    headerValue = response.headers.get("Last-Modified");
                    if (headerValue != null) {
                        cacheEntry.lastModified = HttpHeaderParser.parseDateAsEpoch(headerValue);
                    }
                    cacheEntry.responseHeaders = response.headers;
                    final String jsonString = new String(response.data,
                            HttpHeaderParser.parseCharset(response.headers));
                    return Response.success(jsonString, cacheEntry);
                } catch (Exception e) {
                    return Response.error(new ParseError(e));
                }
            }*/
        };

        //Adding request the the queue
        requestQueue.add(stringRequest);
    }

    private void setUpRecyclerView() {
        mAdapter = new WinnerListAdapter(winnerList, mCtx);
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(mCtx);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setAdapter(mAdapter);
    }

    private void initView(View view) {
        selectDate=view.findViewById(R.id.select_date);
        dateTxt=view.findViewById(R.id.date_txt);

        recyclerView=view.findViewById(R.id.winner_rv);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.select_date:
                final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear,
                                          int dayOfMonth) {
                        // TODO Auto-generated method stub
                        myCalendar.set(Calendar.YEAR, year);
                        myCalendar.set(Calendar.MONTH, monthOfYear);
                        myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                        String myFormat = "yyyy-MM-dd"; //In which you need put here
                        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
                        dateTxt.setText(sdf.format(myCalendar.getTime()));
                        fetchData(sdf.format(myCalendar.getTime()));
                    }

                };
                showDatePicker(date);
                break;
        }
    }


    private void showDatePicker(DatePickerDialog.OnDateSetListener date) {
        myCalendar = Calendar.getInstance();

                new DatePickerDialog(mCtx, date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();

    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        mCtx=context;
    }
}
