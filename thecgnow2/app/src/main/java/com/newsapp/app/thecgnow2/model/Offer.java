package com.newsapp.app.thecgnow2.model;

import com.google.gson.annotations.SerializedName;

public class Offer {
    @SerializedName("offer_id")
    private int id;
    @SerializedName("region_id")
    int regionId;
    @SerializedName("offer_category_id")
    int offerCatId;
    @SerializedName("title")
    private String title;
    @SerializedName("offer_image")
    String offerImage;

    public Offer(int id, int regionId, String title, int offerCatId, String offerImage) {
        this.id = id;
        this.regionId = regionId;
        this.title = title;
        this.offerCatId = offerCatId;
        this.offerImage = offerImage;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getRegionId() {
        return regionId;
    }

    public void setRegionId(int regionId) {
        this.regionId = regionId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getOfferCatId() {
        return offerCatId;
    }

    public void setOfferCatId(int offerCatId) {
        this.offerCatId = offerCatId;
    }

    public String getOfferImage() {
        return offerImage;
    }

    public void setOfferImage(String offerImage) {
        this.offerImage = offerImage;
    }
}
