package com.newsapp.app.thecgnow2.roomDB.like;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

@Dao
public interface LikeDao {

    @Insert
    public void like(LikeRoom articleId);

    @Query("select * from likes")
    public List<LikeRoom> getLikes();

    @Query("SELECT DISTINCT * FROM likes WHERE like_article_id =:articleId")
    public LikeRoom getLikeWithArticleId(String articleId);

    @Query("DELETE FROM likes WHERE like_article_id =:articleId")
    public void unlike(String articleId);
}
