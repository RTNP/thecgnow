package com.newsapp.app.thecgnow2.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.newsapp.app.thecgnow2.Config;
import com.newsapp.app.thecgnow2.MakeRequest;
import com.newsapp.app.thecgnow2.R;
import com.newsapp.app.thecgnow2.SharedPreferenceConfig;
import com.newsapp.app.thecgnow2.TheCGNowApp;
import com.newsapp.app.thecgnow2.adapter.JobAdapter;
import com.newsapp.app.thecgnow2.adapter.NotificationAdapter;
import com.newsapp.app.thecgnow2.model.Article;
import com.newsapp.app.thecgnow2.model.JobList;
import com.newsapp.app.thecgnow2.model.Notification;
import com.newsapp.app.thecgnow2.roomDB.CGNowDB;
import com.newsapp.app.thecgnow2.roomDB.notification.NotificationRoom;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class NotificationActivity extends AppCompatActivity {

    private ArrayList<Article> articles = new ArrayList<>();
    private RecyclerView recyclerView;
    private NotificationAdapter mAdapter;
    private RequestQueue requestQueue;
    BroadcastReceiver broadcastReceiver;

    CGNowDB mDB;
    TheCGNowApp publicAds;
    SharedPreferenceConfig sharedPreferenceConfig;

    public static final String TAG="NotificationActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.home);

        TextView toolTitle=findViewById(R.id.main_tool_title);
        toolTitle.setText(getResources().getString(R.string.notification));

        initView();

        requestQueue = Volley.newRequestQueue(this);
        sharedPreferenceConfig=new SharedPreferenceConfig(this);

        initDB();
        sharedPreferenceConfig.writeNotificationCount(0);
        registerBroadcastReceiver();
        fetchData();
        MainActivity.setNotificationBadgeCount(0);
    }

    private void fetchData() {

        Gson gson = new Gson();
        Type type = new TypeToken<ArrayList<Article>>(){}.getType();
        try {
            JSONObject jsonObject = new JSONObject(sharedPreferenceConfig.readResponseData(Config.notificationKey));
            articles = gson.fromJson(jsonObject.getJSONArray("data").toString(), type);
            Log.d(TAG, "notificationList="+gson.toJson(articles));
            Log.d(TAG, "imgurl="+articles.get(0).getMediaUrl()+", "+articles.get(0).getImg()+", "+articles.get(0).getSrcImage());

            mAdapter = new NotificationAdapter(articles, this);
            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
            recyclerView.setLayoutManager(mLayoutManager);
            recyclerView.setAdapter(mAdapter);
            Log.d(TAG, "notificationsize="+articles.size());
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void registerBroadcastReceiver() {
        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String title = intent.getStringExtra("com.example.thecgnow.notification.data.title");
                String image = intent.getStringExtra("com.example.thecgnow.notification.data.image");
                String id = intent.getStringExtra("com.example.thecgnow.notification.data.id");
                addNotification(id, title, image);
            }
        };
        registerReceiver(broadcastReceiver, new IntentFilter("com.example.thecgnow.notification_received"));
    }

    private void addNotification(String id, String title, String image) {

        String requestData = "?latitude="+sharedPreferenceConfig.readLatitude()+"&longitude="+sharedPreferenceConfig.readLongitude()+"&auth_key="+sharedPreferenceConfig.readFcmToken();
        MakeRequest.getRequestOnSharedPreference(Config.GET_NOTIFICATION_URL+requestData,this,Config.notificationKey);
        fetchData();

        /*NotificationRoom notificationRoom = new NotificationRoom();
        notificationRoom.setTitle(title);
        notificationRoom.setImageUrl(image);

        mDB.notificationDao().addNotification(notificationRoom);*/
        /*notifications.add(new Notification(id, title, image));
        mAdapter.notifyItemInserted(notifications.size()-1);
        sharedPreferenceConfig.writeNotificationCount(0);
        MainActivity.setNotificationBadgeCount(0);*/
    }

    private void initDB() {
        publicAds=(TheCGNowApp)getApplicationContext();
        publicAds.initDB(getApplicationContext());
        mDB=publicAds.getDB(getApplicationContext());
    }

    private void initView() {
        recyclerView=findViewById(R.id.notification_rv);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(broadcastReceiver);
    }
}