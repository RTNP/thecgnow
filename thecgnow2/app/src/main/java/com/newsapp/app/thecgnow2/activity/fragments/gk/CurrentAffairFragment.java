package com.newsapp.app.thecgnow2.activity.fragments.gk;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.newsapp.app.thecgnow2.Config;
import com.newsapp.app.thecgnow2.R;
import com.newsapp.app.thecgnow2.SharedPreferenceConfig;
import com.newsapp.app.thecgnow2.activity.MainActivity;
import com.newsapp.app.thecgnow2.activity.fragments.GKFragment;
import com.newsapp.app.thecgnow2.adapter.CurrentAffairAdapter;
import com.newsapp.app.thecgnow2.adapter.JobListAdapter;
import com.newsapp.app.thecgnow2.model.GK;
import com.newsapp.app.thecgnow2.model.JobList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class CurrentAffairFragment extends Fragment {

    private ArrayList<GK> gks = new ArrayList<>();
    private RecyclerView recyclerView;
    private CurrentAffairAdapter mAdapter;
    private RequestQueue requestQueue;
    private Context mCtx;
    private String TAG="CurrentAffairFragment";
    SharedPreferenceConfig sharedPreferenceConfig;
    NestedScrollView nestedScrollView;
    boolean isScrolling=false, isLoading=false;
    int page = 0, catId=0;
    ProgressBar loading_bottom;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view= inflater.inflate(R.layout.fragment_current_affair, container, false);

        initView(view);
        sharedPreferenceConfig=new SharedPreferenceConfig(getActivity());
        loading_bottom.setVisibility(View.GONE);
//        sharedPreferenceConfig.writeAlarmSet(false);

       setUpCurrentAffairRecyclerView();

        requestQueue = Volley.newRequestQueue(getActivity());

//        fetchData();

        initScrollListener();
        recyclerView.setAdapter(mAdapter);

        return view;
    }

    private void initScrollListener() {
        if (nestedScrollView!=null) {
            nestedScrollView.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
                @Override
                public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                    if (scrollY > oldScrollY) {
//                        Log.i(TAG, "Scroll DOWN");
                        isScrolling=true;
                    }
                    if (!isLoading && isScrolling && scrollY >= ( v.getChildAt(v.getChildCount()-1).getMeasuredHeight() - v.getMeasuredHeight() )) {
                        Log.i(TAG, "BOTTOM SCROLL");
                        isLoading=true;
                        isScrolling=false;
                        fetchData();
                    }
                }
            });
        }
    }

    private void setUpCurrentAffairRecyclerView() {


        Gson gson = new Gson();
        Type type = new TypeToken<ArrayList<GK>>(){}.getType();
        try {
            JSONObject jsonObject = new JSONObject(sharedPreferenceConfig.readResponseData(Config.currentAffairKey));
            gks = gson.fromJson(jsonObject.getJSONArray("data").toString(), type);
            Log.d(TAG, "currentaffairlist="+gson.toJson(gks));

            mAdapter = new CurrentAffairAdapter(gks,mCtx);
            recyclerView.setHasFixedSize(true);
            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(mCtx);
            recyclerView.setLayoutManager(mLayoutManager);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void fetchData() {

        loading_bottom.setVisibility(View.VISIBLE);

        String get_query = "?latitude="+sharedPreferenceConfig.readLatitude()+"&longitude="+sharedPreferenceConfig.readLongitude()+"&auth_key="+sharedPreferenceConfig.readFcmToken();
        Log.d(TAG, "currentaffairquery= "+Config.GET_CURRENT_AFFAIR_LIST_URL+get_query);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, Config.GET_CURRENT_AFFAIR_LIST_URL+get_query,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        Log.d(TAG,response);
                        gks.clear();
                        loading_bottom.setVisibility(View.GONE);
//                        Toast.makeText(AttendenceActivity.this, "attendance response"+response, Toast.LENGTH_SHORT).show();
                        try {
                            //Creating the json object from the response
                            JSONObject jsonResponse = new JSONObject(response);
                            JSONArray jsonArray = jsonResponse.getJSONArray("data");
                            Log.d("jsondatabatch",jsonArray.toString());
//                            Toast.makeText(AttendenceActivity.this, ""+jsonArray.toString(), Toast.LENGTH_SHORT).show();

                            String sourceVal, articleId;
                            for (int i=0; i<jsonArray.length(); i++) {
                                JSONObject j =jsonArray.getJSONObject(i);

                                gks.add(new GK(j.getInt("current_affair_id"),
                                        j.getString("title"),
                                        Config.BASE_URL+j.getString("image_url"),
                                        j.getString("description")));
                            }
                            mAdapter.notifyDataSetChanged();
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.d("mobileerror",""+e);
//                            Toast.makeText(getActivity(), ""+e.toString(), Toast.LENGTH_SHORT).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {

                        String message = null;
                        if (volleyError instanceof NetworkError) {
                            message = "Cannot connect to Internet...Please check your connection!";
                        } else if (volleyError instanceof ServerError) {
                            message = "The server could not be found. Please try again after some time!!";
                        } else if (volleyError instanceof AuthFailureError) {
                            message = "Cannot connect to Internet...Please check your connection!";
                        } else if (volleyError instanceof ParseError) {
                            message = "Parsing error! Please try again after some time!!";
                        } else if (volleyError instanceof NoConnectionError) {
                            message = "Cannot connect to Internet...Please check your connection!";
                        } else if (volleyError instanceof TimeoutError) {
                            message = "Connection TimeOut! Please check your internet connection.";
                        }

                        if (message!=null) {
                            try {
                                Toast.makeText(mCtx, getResources().getString(R.string.couldnot_connect),Toast.LENGTH_LONG).show();
                            } catch (IllegalStateException e) {
                                e.printStackTrace();
                            }
                        }
//                        Toast.makeText(mCtx, mCtx.getResources().getString(R.string.couldnot_connect),Toast.LENGTH_LONG).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                //Adding the parameters to the request
                params.put("auth_key", sharedPreferenceConfig.readFcmToken());
                return params;
            }

            /*@Override
            protected Response<String> parseNetworkResponse(NetworkResponse response) {
                try {
                    Cache.Entry cacheEntry = HttpHeaderParser.parseCacheHeaders(response);
                    if (cacheEntry == null) {
                        cacheEntry = new Cache.Entry();
                    }
                    final long cacheHitButRefreshed =  2 * 60 * 1000; // 10 * 60 * 1000 in 10 minutes cache will be hit, but also refreshed on background
                    final long cacheExpired = 24 * 60 * 60 * 1000; // in 24 hours this cache entry expires completely
                    long now = System.currentTimeMillis();
                    final long softExpire = now + cacheHitButRefreshed;
                    final long ttl = now + cacheExpired;
                    cacheEntry.data = response.data;
                    cacheEntry.softTtl = softExpire;
                    cacheEntry.ttl = ttl;
                    String headerValue;
                    headerValue = response.headers.get("Date");
                    if (headerValue != null) {
                        cacheEntry.serverDate = HttpHeaderParser.parseDateAsEpoch(headerValue);
                    }
                    headerValue = response.headers.get("Last-Modified");
                    if (headerValue != null) {
                        cacheEntry.lastModified = HttpHeaderParser.parseDateAsEpoch(headerValue);
                    }
                    cacheEntry.responseHeaders = response.headers;
                    final String jsonString = new String(response.data,
                            HttpHeaderParser.parseCharset(response.headers));
                    return Response.success(jsonString, cacheEntry);
                } catch (Exception e) {
                    return Response.error(new ParseError(e));
                }
            }*/
        };

        //Adding request the the queue
        requestQueue.add(stringRequest);
    }

    private void initView(View view) {
        recyclerView=view.findViewById(R.id.rv);
        nestedScrollView=view.findViewById(R.id.nested_sv);
        loading_bottom=view.findViewById(R.id.loading_bottom);
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        mCtx=context;
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
//        super.onCreateOptionsMenu(menu, inflater);
//        inflater.inflate(R.menu.fragment_menu, menu);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        int id = item.getItemId();
        if (id==R.id.action_refresh && GKFragment.tabs.getSelectedTabPosition()==0 && MainActivity.bottomBar.getSelectedItemId()==R.id.navigation_current_affairs) {
            Toast.makeText(mCtx, "current affair fragment", Toast.LENGTH_SHORT).show();
            page=0;
            fetchData();
        }

        return super.onOptionsItemSelected(item);
    }
}
