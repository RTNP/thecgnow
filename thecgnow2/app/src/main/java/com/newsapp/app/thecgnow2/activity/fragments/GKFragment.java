package com.newsapp.app.thecgnow2.activity.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.newsapp.app.thecgnow2.R;
import com.newsapp.app.thecgnow2.activity.MainActivity;
import com.newsapp.app.thecgnow2.activity.ui.gk.SectionsPagerAdapter;
import com.google.android.material.tabs.TabLayout;

public class GKFragment extends Fragment {

    private static final String ARG_NAME = "arg_name";
    public static TabLayout tabs;

    public static GKFragment newInstance(String name) {
        Bundle bundle = new Bundle();
        bundle.putString(ARG_NAME, name);
        GKFragment dashboardFragment = new GKFragment();
        dashboardFragment.setArguments(bundle);
        return dashboardFragment;
    }

    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view= inflater.inflate(R.layout.fragment_gk, container, false);

        initView(view);

        SectionsPagerAdapter sectionsPagerAdapter = new SectionsPagerAdapter(getActivity(), getChildFragmentManager());
        ViewPager viewPager = view.findViewById(R.id.view_pager);
        viewPager.setAdapter(sectionsPagerAdapter);
//        viewPager.setCurrentItem(1);
        tabs = view.findViewById(R.id.tabs);
        tabs.setupWithViewPager(viewPager);

        return view;
    }

    private void initView(View view) {
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        if(tabs.getSelectedTabPosition()==0)
        {
            inflater.inflate(R.menu.fragment_menu,menu);
        }
        super.onCreateOptionsMenu(menu, inflater);
    }
}
