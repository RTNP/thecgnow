package com.newsapp.app.thecgnow2.roomDB.follow;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

@Dao
public interface FollowDao {
    @Insert
    public void followTo(FollowRoom srcName);

    @Query("select * from followings")
    public List<FollowRoom> getFollowings();

    @Query("SELECT DISTINCT * FROM followings WHERE follow_source_name =:srcName")
    public FollowRoom getFollowingWithSrcName(String srcName);

    @Query("DELETE FROM followings WHERE follow_source_name =:srcName")
    public void unFollowTo(String srcName);
}
