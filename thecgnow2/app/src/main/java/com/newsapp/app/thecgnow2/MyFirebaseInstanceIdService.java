package com.newsapp.app.thecgnow2;

import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

import static com.newsapp.app.thecgnow2.activity.NotificationActivity.TAG;

public class MyFirebaseInstanceIdService extends FirebaseInstanceIdService {
    SharedPreferenceConfig sharedPreferenceConfig;

    @Override
    public void onTokenRefresh() {
        // Get updated InstanceID token.
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.d(TAG, "Refreshed token: " + refreshedToken);
        sharedPreferenceConfig = new SharedPreferenceConfig(this);
        sharedPreferenceConfig.writeFcmToken(refreshedToken);
        // If you want to send messages to this application instance or
        // manage this apps subscriptions on the server side, send the
        // Instance ID token to your app server.
    }
}
