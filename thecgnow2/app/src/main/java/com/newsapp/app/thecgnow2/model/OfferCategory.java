package com.newsapp.app.thecgnow2.model;

import androidx.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

public class OfferCategory {
    @SerializedName("offer_category_id")
    int id;

    @SerializedName("category_name")
    String name;

    String keyName;

    public OfferCategory(int id, String name, String keyName) {
        this.id = id;
        this.name = name;
        this.keyName=keyName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getKeyName() {
        return keyName;
    }

    public void setKeyName(String keyName) {
        this.keyName = keyName;
    }

    @NonNull
    @Override
    public String toString() {
        return name;
    }
}
