package com.newsapp.app.thecgnow2;

public class Config {
    public static final int ARTICLE_ITEM_IMAGE = 1;
    public static final int ARTICLE_ITEM_VIDEO = 2;
    public static final int ARTICLE_ITEM_AD = 3;
    public static final int LOADING_VIEW = -1;
//    public static final int ARTICLE_ITEM_SUGGEST_TOPIC = 4;

    public static final int SUGGEST_RECT = 5;
    public static final int SUGGEST_CIRCLE = 6;

    public static final int ARTICLE_ITEM_AD_VIDEO = 7;
    public static final int ARTICLE_ITEM_AD_GIF = 8;

    public static int AD_VISIBILITY_COUNTER = 0;
    public static int TIME_COUNTER = 0;

    public static int POS_NOTI_FRAG = 3;

    //http://thecgnow.com/apis/get-everything-in-app.php
    public static final String BASE_URL = "http://news.thecgnow.com";
    public static final String BASE_URL1 = "http://news.thecgnow.com/api/";
    public static final String WEB_HOSE_API_URL = BASE_URL1+"get-everything-in-app";
    public static final String GET_SHARED_ARTICLE_URL = BASE_URL+"get-shared-article.php";
    public static final String GET_NOTIFICATION_ARTICLE_URL = BASE_URL1+"get-notification-article";
    public static final String GET_ARTICLE_DESC_ADS = BASE_URL1+"get-article-desc-ads-in-app";
    public static final String GET_ADS_URL = BASE_URL1+"get-ads-in-app";
    public static final String GET_TAMBOLA_NUMBERS_URL = BASE_URL1+"get-tambola-numbers-in-app";
    public static final String GET_REGIONS_URL = BASE_URL1+"get-regions-in-app";
    public static final String GET_TAMBOLA_IN_APP = BASE_URL1+"get-tambola-in-app";
    public static final String GET_CATEGORY_URL = BASE_URL1+"get-categories-in-app";
//    public static final String GET_JOB_CATEGORIES_URL = BASE_URL+"get-job-categories-in-app.php";
    public static final String GET_JOB_LIST_URL = BASE_URL1+"get-job-list-in-app";
    public static final String GET_WINNER_LIST_URL = BASE_URL1+"get-winner-list-in-app";
    public static final String GET_OFFER_LIST_URL = BASE_URL1+"get-offer-in-app";
    public static final String GET_TOP_STORIES_URL = BASE_URL1+"get-top-stories-in-app";
    public static final String GET_CURRENT_AFFAIR_LIST_URL = BASE_URL1+"get-current-affairs-in-app";
    public static final String GET_OFFER_CATEGORIES_URL = BASE_URL1+"get-offer-categories-in-app";
    public static final String REGISTER_TOKEN_URL = BASE_URL1+"register-user-fcm-token";
    public static final String GENERATE_TAMBOLA_TICKET_URL = BASE_URL1+"generate-my-ticket-in-app";
    public static final String GET_NOTIFICATION_URL = BASE_URL1+"my-notification-in-app";
    public static final String GET_WEB_SOCKET_URL = BASE_URL1+"my-websocket-in-app";

    // master key
    public static final String MASTER_KEY = "zL8Vep02yyDpy5QAPu81BdVOUj0N1bKI29vjzt3B/x8=";

    //broadcast configuration
    public static final String NOTIFICATION_RECEIVED = "com.example.thecgnow.notification_received";
    public static final String NOTIFICATION_KEY_STRING = "com.example.thecgnow.notification_rec_key";
    public static final int NOTIFICATION_KEY_VALUE = 1;
    public static final String TOKEN_GENERATED = "com.example.thecgnow.token_generated";
    public static final String TOKEN_GENERATED_KEY = "com.example.thecgnow.token_generated_key";
    public static final int TOKEN_GENERATED_KEY_VALUE = 2;

    public static final String categoryListKey = "category_list_key";
    public static final String jobListKey = "job_list_key";
    public static final String offerListKey = "offer_list_key";
    public static final String currentAffairKey = "current_affair_key";
    public static final String tambolaKey = "tambola_key";
    public static final String offerCategoryKey = "offer_category_key";
    public static final String ticketIdKey = "ticked_id_key";
    public static final String webSocketKey = "web_socket_key";
    public static final String notificationKey = "notification_key";

//    public static final String webSocketUrl = "ws://31.220.21.50:8080";
}
