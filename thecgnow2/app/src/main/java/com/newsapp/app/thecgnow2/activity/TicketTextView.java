package com.newsapp.app.thecgnow2.activity;

import android.content.Context;
import android.graphics.Canvas;

import androidx.appcompat.widget.AppCompatTextView;

public class TicketTextView extends AppCompatTextView {

    public interface OnToggledListener {
        void OnToggled(TicketBoardView v, boolean touchOn);
    }

    boolean touchOn;
    boolean mDownTouch = false;
    private OnToggledListener toggledListener;
    int idX = 0; //default
    int idY = 0; //default

    public TicketTextView(Context context, int x, int y) {
        super(context);
        idX = x;
        idY = y;
        init();
    }

    private void init() {
        touchOn = false;
    }

    public void setText(String text) {
        super.setText(text);
    }

    public CharSequence setText() {
        return super.getText();
    }

    @Override
    protected void onDraw (Canvas canvas) {
        super.onDraw(canvas);
    }

    public boolean isTouchOn() {
        return touchOn;
    }

    public void setTouchOn(boolean touchOn) {
        this.touchOn = touchOn;
    }

    public int getIdX(){
        return idX;
    }

    public int getIdY(){
        return idY;
    }

    @Override
    public boolean performClick() {
        super.performClick();
        return true;
    }

    public void setOnToggledListener(OnToggledListener listener){
        toggledListener = listener;
    }
}
