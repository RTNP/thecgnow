package com.newsapp.app.thecgnow2.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.NinePatch;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;

import com.newsapp.app.thecgnow2.R;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Transformation;

import java.util.ArrayList;

import jp.wasabeef.picasso.transformations.CropCircleTransformation;
import jp.wasabeef.picasso.transformations.RoundedCornersTransformation;

public class BannerAdapter extends PagerAdapter {
    private Context context;
    private ArrayList<String> imageUrls;
    private ArrayList<String> linkUrls;
    public BannerAdapter(Context context, ArrayList<String> imageUrls, ArrayList<String> linkUrls)
    {
        this.context=context;
        this.imageUrls=imageUrls;
        this.linkUrls=linkUrls;
    }
    @Override
    public int getCount() {
        return imageUrls.size();
    }
    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view==object;
    }
    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, final int position) {
        ImageView imageView = new ImageView(context);
//        imageView.setBackgroundResource(R.drawable.rounded_stroke_box);
//        imageView.setScaleType(ImageView.ScaleType.FIT_XY);

        Picasso.get()
                .load(imageUrls.get(position))
                .error(R.drawable.placeholder)
                .transform(new RoundedCornersTransformation(10, 0))
                .into(imageView, new Callback() {
                    @Override
                    public void onSuccess() {
                    }
                    @Override
                    public void onError(Exception ex) {
                        //Toast.makeText(context,e.getMessage().toString(),Toast.LENGTH_LONG).show();
                    }
                });

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Uri uri = Uri.parse(linkUrls.get(position));
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
            }
        });
        container.addView(imageView);
//        return super.instantiateItem(container, position);
        return imageView;
    }
    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((View) object);
    }
}
