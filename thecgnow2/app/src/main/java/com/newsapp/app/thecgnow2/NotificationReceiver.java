package com.newsapp.app.thecgnow2;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.util.Log;
import android.widget.Toast;

import androidx.core.content.FileProvider;

import java.io.File;

public class NotificationReceiver extends BroadcastReceiver {

    Context mCtx;
    public String TAG = "NotificationReceiver";

    @Override
    public void onReceive(Context context, Intent intent) {
        mCtx = context;
        String imageUri = intent.getStringExtra("imageuri");
        Toast.makeText(context, "uri:"+imageUri, Toast.LENGTH_SHORT).show();
//        shareApp();
        Log.d(TAG, "received");
        openScreenshot(imageUri);
//        showImage(imageUri);
//        Intent i = new Intent(mCtx, HousieBoardActivity.class);
       /* i.putExtra("shareuri", imageUri);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        mCtx.startActivity(i);*/
    }

    private void shareApp() {
        Log.d(TAG, "share app called");
        try {
            Intent shareIntent = new Intent(Intent.ACTION_SEND);
            shareIntent.setType("text/plain");
            shareIntent.putExtra(Intent.EXTRA_SUBJECT, "CG More");
            String shareMessage= "Let me recommend you this application\n";
            shareMessage = shareMessage + "https://play.google.com/store/apps/details?id=" + BuildConfig.APPLICATION_ID ;
            shareIntent.putExtra(Intent.EXTRA_TEXT, shareMessage);
            mCtx.startActivity(Intent.createChooser(shareIntent, "choose one"));
            Log.d(TAG, "share app called inside try block");
        } catch(Exception e) {
            //e.toString();
            e.printStackTrace();
        }
    }

    private void openScreenshot(String imageUri) {
        Log.d(TAG, "openscreenshot");
        File file = new File(imageUri);
        Intent intent = new Intent();
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.setAction(Intent.ACTION_VIEW);
        Uri uri = Uri.parse(imageUri);
        intent.setDataAndType(uri, "image/*");
//        intent.setDataAndType(, "image/*");
        final Intent i = new Intent(Intent.ACTION_VIEW);//
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                /*i.setDataAndType(Build.VERSION.SDK_INT >= Build.VERSION_CODES.N ?
                                FileProvider.getUriForFile(mCtx,mCtx.getPackageName() + ".provider", file) : Uri.fromFile(file),
                        "image/*").addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);*/
                i.setDataAndType(Build.VERSION.SDK_INT >= Build.VERSION_CODES.N ?
                        FileProvider.getUriForFile(mCtx,
                                BuildConfig.APPLICATION_ID + ".provider", createImageFile(imageUri)) : Uri.fromFile(file),
                        "image/*").addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        /*Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse(file.getPath()));
//        i.setAction(Intent.ACTION_VIEW,Uri.fromFile(file).toString());*/
//        i.setType("image/*");
//        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        mCtx.startActivity(i);
        String tempUri = FileProvider.getUriForFile(mCtx,
                BuildConfig.APPLICATION_ID + ".provider", createImageFile(imageUri)).toString();
        Log.d(TAG, "openscreenshot end uri:"+tempUri);
    }

    private void showImage(String imageUri) {
        Uri photoURI = FileProvider.getUriForFile(mCtx,
                BuildConfig.APPLICATION_ID + ".provider",
                createImageFile(imageUri));
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_VIEW);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        /*Uri photoURI = FileProvider.getUriForFile(mCtx,
                BuildConfig.APPLICATION_ID + ".provider",
                new File(mCtx.getFilesDir() + File.separator + "DATA" + File.separator + "TITLE"+ File.separator + imageUri));*/
        intent.setDataAndType(photoURI,"image/*");
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
//        mCtx.startActivity(Intent.createChooser(intent, "View using"));
        mCtx.startActivity(intent);
    }

    private File createImageFile(String imageUri) {
        File file = new File("/sdcard/TheCGNow/", imageUri);
        return file;
    }
}
