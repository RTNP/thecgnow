package com.newsapp.app.thecgnow2.activity.fragments;

import android.animation.ObjectAnimator;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.drawable.AnimationDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.viewpager.widget.ViewPager;

import com.android.volley.AuthFailureError;
import com.android.volley.Cache;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.newsapp.app.thecgnow2.Config;
import com.newsapp.app.thecgnow2.FragmentInterface;
import com.newsapp.app.thecgnow2.PagerInterface;
import com.newsapp.app.thecgnow2.R;
import com.newsapp.app.thecgnow2.SharedPreferenceConfig;
import com.newsapp.app.thecgnow2.TheCGNowApp;
import com.newsapp.app.thecgnow2.activity.MainActivity;
import com.newsapp.app.thecgnow2.adapter.ArticleHomeAdapter;
import com.newsapp.app.thecgnow2.adapter.BannerAdapter;
import com.newsapp.app.thecgnow2.adapter.HorizontalRVAdapter;
import com.newsapp.app.thecgnow2.adapter.SliderBannerAdapter;
import com.newsapp.app.thecgnow2.model.Advertize;
import com.newsapp.app.thecgnow2.model.Article;
import com.newsapp.app.thecgnow2.model.Topic;
import com.newsapp.app.thecgnow2.roomDB.CGNowDB;
import com.newsapp.app.thecgnow2.roomDB.ad.AdRoom;
import com.google.gson.Gson;
import com.smarteist.autoimageslider.IndicatorView.animation.type.IndicatorAnimationType;
import com.smarteist.autoimageslider.SliderAnimations;
import com.smarteist.autoimageslider.SliderView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

import static com.newsapp.app.thecgnow2.activity.MainActivity.publicAds;

public class TabFragment extends Fragment implements View.OnClickListener {

    private static final String ARG_SECTION_NUMBER = "section_number";
    private static final String ARG_CATEGORY_ID = "category_id";
    private static final String ARG_CATEGORY_NAME = "category_name";
    LinearLayout linearLoadingLayout;
    ProgressBar loading_bottom;
    private int sectionNumber;
    int visibleItemCount, totalItemCount, pastVisiblesItems;
    private int categoryId, page=0, ad_page=0;
    private String categoryName;
    LinearLayout refreshll, loading;
    Button refreshBtn;
    LinearLayout cardView;
    boolean isLoading = false;
    NestedScrollView nestedScrollView;
    int adInterval=6;   // adInterval = defines after how many no. of news the advertise will be shown

    ArrayList<Advertize> advertizes = new ArrayList<>();

    private ArrayList<Article> articles = new ArrayList<>();
    private ArrayList<Article> topArticles = new ArrayList<>();
    private RecyclerView recyclerView;
    RecyclerView.LayoutManager mLayoutManager;
    private ArticleHomeAdapter mAdapter;
    private HorizontalRVAdapter mHorizAdapter;
    private RequestQueue requestQueue;
//    SwipeRefreshLayout pullToRefresh;
    TextView updateMsg;
    ProgressBar progressBar;
    ArrayList<String> defaultCategories=new ArrayList<>();
    ArrayList<Integer> adPositions=new ArrayList<>();    // it defines the positions of ads in recyclerview used for identify and refresh all ads
    private View currentFocusedLayout, oldFocusedLayout;
    SharedPreferenceConfig sharedPreferenceConfig;

    /* Modification 14-Dec-2020 */
    private boolean isLoaded =false,isVisibleToUser;

    TheCGNowApp cgNowApp;
    CGNowDB mDB;

    Context mCtx;
    public static final String TAG="TabFragment";
    BroadcastReceiver minuteUpdateReceiver, broadcastReceiver;
    private int num=0;
    boolean isScrolling=false;
    SliderView sliderView;

//    @BindView(R.id.bannerPager)
//    ViewPager viewPager;

    public TabFragment() {
        // Required empty public constructor
    }

    public static TabFragment newInstance(int sectionNumber, int categoryId, String categoryName) {
        TabFragment fragment = new TabFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        args.putInt(ARG_CATEGORY_ID, categoryId);
        args.putString(ARG_CATEGORY_NAME, categoryName);
        fragment.setArguments(args);
        Log.d(TAG, "query: "+categoryName+" section no."+sectionNumber);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_tab, container, false);

        return rootView;

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        sectionNumber = getArguments().getInt(ARG_SECTION_NUMBER);
        categoryId = getArguments().getInt(ARG_CATEGORY_ID);
        categoryName = getArguments().getString(ARG_CATEGORY_NAME);

        initView(view);

        cgNowApp=(TheCGNowApp)mCtx.getApplicationContext();
        cgNowApp.initDB(mCtx);
        mDB=cgNowApp.getDB(mCtx);

        sharedPreferenceConfig = new SharedPreferenceConfig(mCtx);

        startAnimation(view);

        refreshBtn.setOnClickListener(this);

        updateMsg.setVisibility(View.GONE);
        requestQueue= Volley.newRequestQueue(mCtx);

        setUpRecyclerView();
        registerBroadcastReceiver();

        if(isVisibleToUser && (!isLoaded)){
            fetchData();
            isLoaded=true;
        }
        initScrollListener();

    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        this.isVisibleToUser=isVisibleToUser;
        if(isVisibleToUser && isAdded() ){
            fetchData();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if(!isLoaded) {
            fetchData();
        }
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    private void startAnimation(View view) {
        ImageView[] img = new ImageView[8];
        img[0] = view.findViewById(R.id.img1);
        img[1] = view.findViewById(R.id.img2);
        img[2] = view.findViewById(R.id.img3);
        img[3] = view.findViewById(R.id.img4);
        img[4] = view.findViewById(R.id.img5);
        img[5] = view.findViewById(R.id.img6);
        img[6] = view.findViewById(R.id.img7);
        img[7] = view.findViewById(R.id.img8);
        for(int i=0; i<img.length;i++) {
            AnimationDrawable animationDrawable1 = (AnimationDrawable) img[i].getBackground();
            animationDrawable1.setEnterFadeDuration(100);
            animationDrawable1.setExitFadeDuration(500);
            animationDrawable1.start();
        }
    }

    private void setUpAutoScrollViewPager() {

        ArrayList<String> test = new ArrayList<String>();
        Integer size = 4;
        if (topArticles.size()>=size) {
            for (int i = 0; i < size; i++) {
                test.add(topArticles.get(i).getImg());
            }
            ArrayList<String> test1 = new ArrayList<String>();
            for (int i = 0; i < size; i++) {
                test1.add(topArticles.get(i).getUrl());
            }
            ArrayList<String> test2 = new ArrayList<String>();
            for (int i = 0; i < size; i++) {
                test2.add(topArticles.get(i).getTitle());
            }

            sliderView.setSliderAdapter(new SliderBannerAdapter(mCtx, topArticles));

            sliderView.setIndicatorAnimation(IndicatorAnimationType.DROP);
            sliderView.setSliderTransformAnimation(SliderAnimations.CUBEINDEPTHTRANSFORMATION);
            sliderView.startAutoCycle();
        }
    }

    private void setAdvertiseList() {

        int catId = mDB.categoryDao().getCatIdWithName(categoryName);
        if (defaultCategories.contains(categoryName)) {
            advertizes = publicAds.getNews_banner_ads();
            Log.d(TAG, "setAdvertise size if: "+advertizes.size()+" articleSize:"+articles.size()+" publicAdSize:"+publicAds.getNews_banner_ads().size()+" address:"+publicAds);
        } else {
            advertizes.clear();
            for (Advertize ad : publicAds.getNews_banner_ads()) {
                Log.d(TAG, "catId="+catId+", adregion="+ad.getRegionId()+", addata="+ad.toString());
                if (ad.getRegionId()==catId) {
                    advertizes.add(ad);
                }
            }
            Log.d(TAG, "setAdvertise size else: "+advertizes.size()+" categoryname:"+categoryName+" articleSize:"+articles.size()+" catid:"+catId+" publicAdSize:"+publicAds.getNews_banner_ads().size()+" address:"+publicAds);
        }
        Collections.shuffle(advertizes);

        Gson gson = new Gson();
        Log.d(TAG, "catid: "+catId+" ad refresh called "+gson.toJson(publicAds.getNews_banner_ads()));

       if (advertizes.size()>0)
        updateAdvertize();
    }

    private void updateAdvertize() {
        Random random = new Random();
        int index;
        int adSize = advertizes.size();
        Log.d(TAG, "updateAdvertize sectionnum:"+sectionNumber+" loopTime:"+(articles.size()/adInterval)+" articleSize:"+articles.toString()+" interval:"+adInterval);
        for (int i=1; i<=(articles.size()/adInterval); i++) {

            index = i*adInterval;
//            int randomNumber = random.nextInt(advertizes.size());
            Advertize advertize = advertizes.get(i%adSize);
            Log.d(TAG, "updateAdvertize called index="+(i%adSize)+" url:"+advertize.getMediaUrl()+" i="+i+" index="+index+" adsize="+adSize);
            if (advertize.getMediaType().equals("BIMG")) {
                Log.d(TAG, "bimg called type:" + advertize.getMediaType() + " section:" + sectionNumber);
//                articles.set(index, new Article("", "title changed", advertize.getMediaUrl(), "", advertize.getAdUrl(), Config.ARTICLE_ITEM_AD));
            } else if (advertize.getMediaType().equals("BVID")) {
                Log.d(TAG, "bvid called type:" + advertize.getMediaType() + " section:" + sectionNumber);
//                articles.set(index, new Article("", "title changed", advertize.getMediaUrl(), "", advertize.getAdUrl(), Config.ARTICLE_ITEM_AD_VIDEO));
            } else if (advertize.getMediaType().equals("BGIF")) {
                Log.d(TAG, "bgif called type:" + advertize.getMediaType() + " section:" + sectionNumber);
//                articles.set(index, new Article("", "title changed", advertize.getMediaUrl(), "", advertize.getAdUrl(), Config.ARTICLE_ITEM_AD_GIF));
            }
            mAdapter.notifyItemChanged(index);
        }
    }

    private void registerBroadcastReceiver() {
        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                refreshAds();
            }
        };
        mCtx.registerReceiver(broadcastReceiver, new IntentFilter("com.example.thecgnow.activity.adRefresh"));
    }

    public void refreshAds() {

        Log.d(TAG, "refreshAds function called. refreshing ads...");
        setAdvertiseList();
    }

    public void addAd(String title, String url, String adUrl, int adType, int index) {
        switch (adType) {
            case Config.ARTICLE_ITEM_AD:
//                articles.add(index, new Article("", title, url, "",adUrl, adType));
                break;
            case Config.ARTICLE_ITEM_AD_VIDEO:
//                articles.add(index, new Article("", title, "", url,adUrl, adType));
                break;
            case Config.ARTICLE_ITEM_AD_GIF:
//                url = "https://s3.amazonaws.com/mailbakery/wp-content/uploads/2015/06/26160357/jack-spade.gif";
//                articles.add(index, new Article("", title, url, "",adUrl, adType));
                break;
        }
    }

    public void addAd(int index, int adType) {
//        articles.add(index, new Article("", "", "", "", "", adType));
    }

    private void initScrollListener() {
        if (nestedScrollView!=null) {
            nestedScrollView.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
                @Override
                public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {

                    if (scrollY > oldScrollY) {
                        isScrolling=true;
                    }
                    if (!isLoading && isScrolling && scrollY >= ( v.getChildAt(v.getChildCount()-1).getMeasuredHeight() - v.getMeasuredHeight() )) {
                        Log.i(TAG, "BOTTOM SCROLL");
                        isLoading=true;
                        isScrolling=false;
                        fetchData();
                    }
                }
            });
        }
    }

    private void  fetchData() {
        setTryAgainView(false);
        if(articles.size()==0) {
            loading.setVisibility(View.VISIBLE);
        }else{
            loading_bottom.setVisibility(View.VISIBLE);
        }
//        updateMsg.setVisibility(View.VISIBLE);
        Log.d(TAG, "articlesize="+articles.size());

        String get_query = "?region="+categoryName+"&q="+categoryName+"&page="+page+"&latitude="+sharedPreferenceConfig.readLatitude()+"&longitude="+sharedPreferenceConfig.readLongitude()+"&ad_page="+ad_page+"&auth_key="+sharedPreferenceConfig.readFcmToken();
        Log.d(TAG, "query_"+categoryName+"="+Config.WEB_HOSE_API_URL+get_query);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, Config.WEB_HOSE_API_URL+get_query,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        isLoading = false;
//                        pullToRefresh.setRefreshing(false);

//                        updateMsg.setVisibility(View.GONE);
                        loading.setVisibility(View.GONE);
                        loading_bottom.setVisibility(View.GONE);
                        Log.d(TAG,categoryName+sectionNumber+" "+response);
                            try {
                                //Creating the json object from the response
                                JSONObject jsonResponse = new JSONObject(response);
                                JSONArray jsonArray = jsonResponse.getJSONArray("articles");
                                renderTab(jsonArray, jsonResponse);
                                sharedPreferenceConfig.writeResponseData(Config.WEB_HOSE_API_URL+get_query,response);
                            } catch (JSONException e) {
                                e.printStackTrace();
                                try {
                                    JSONObject jsonResponse = new JSONObject(response);
                                    if (jsonResponse.getBoolean("status") && !sharedPreferenceConfig.readFcmToken().isEmpty()) {
                                        registerFcmToken();
                                    }
                                    if(sharedPreferenceConfig.readResponseData(Config.WEB_HOSE_API_URL+get_query) != ""){
                                        jsonResponse = new JSONObject(sharedPreferenceConfig.readResponseData(Config.WEB_HOSE_API_URL+get_query));
                                        JSONArray jsonArray = jsonResponse.getJSONArray("articles");
                                        renderTab(jsonArray, jsonResponse);
                                    }else{
                                        fetchData();
                                    }
                                } catch (JSONException ex) {
                                    ex.printStackTrace();
                                }

                                Log.d(TAG, "mobileerror:" + e);
                                String temp = "http://thecgnow.com/apis/get-everything-in-app.php";
                                temp += "q" + "=" + categoryName;
                                requestQueue.getCache().invalidate(temp, true);
//                            Log.d("mobileerror",""+e);
//                                pullToRefresh.setRefreshing(false);
                                updateMsg.setVisibility(View.GONE);
                                loading.setVisibility(View.GONE);
                                setTryAgainView(true);
                                isLoading = false;
//                            Toast.makeText(getActivity(), ""+e.toString(), Toast.LENGTH_SHORT).show();
                            }
                        isLoaded = true;
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {

                        String message = null;
                        if (volleyError instanceof NetworkError) {
                            message = "Cannot connect to Internet...Please check your connection!";
                        } else if (volleyError instanceof ServerError) {
                            message = "The server could not be found. Please try again after some time!!";
                        } else if (volleyError instanceof AuthFailureError) {
                            message = "Cannot connect to Internet....Please check your connection!";
                        } else if (volleyError instanceof ParseError) {
                            message = "Parsing error! Please try again after some time!!";
                        } else if (volleyError instanceof NoConnectionError) {
                            message = "Cannot connect to Internet.....Please check your connection!";
                        } else if (volleyError instanceof TimeoutError) {
                            message = "Connection TimeOut! Please check your internet connection.";
                        }

                        if (message!=null) {
//                            pullToRefresh.setRefreshing(false);
                            updateMsg.setVisibility(View.GONE);
                            setTryAgainView(true);
                            isLoading = false;
                            loading.setVisibility(View.GONE);
                            Log.d(TAG, "message="+message+" volleyError:"+volleyError);
                        }
//                        Toast.makeText(mCtx, mCtx.getResources().getString(R.string.couldnot_connect),Toast.LENGTH_LONG).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                //Adding the parameters to the request
                Log.d(TAG, "queryvolley: "+categoryName+" section no."+sectionNumber+" pagenum: "+page);
                if(!defaultCategories.contains(categoryName))
                params.put("region", categoryName);
                params.put("q", categoryName);
                params.put("page", page+"");
                params.put("auth_key", sharedPreferenceConfig.readFcmToken());
                return params;
            }

        };

        //Adding request the the queue
        requestQueue.add(stringRequest);
    }

    private void setTryAgainView(boolean b) {
        if (b) {
            if (articles.size() == 0) {
                /*cardView.setVisibility(View.GONE);
                refreshll.setVisibility(View.VISIBLE);*/
            }
        } else {
            cardView.setVisibility(View.VISIBLE);
            refreshll.setVisibility(View.GONE);
        }

        Log.d(TAG, "tryagainb="+b+" articlesize="+articles.size());
    }


    private final class ReceiverThread extends AsyncTask<Void, Void, String> {

        @Override
        protected String doInBackground(Void... params) {

            final Runnable runnable=new Runnable() {
                @Override
                public void run() {
                    Log.d(TAG, "in handler refreshing...");
                    mAdapter.notifyDataSetChanged();
                    setUpAutoScrollViewPager();
                }
            };


            return "Executed";
        }

        @Override
        protected void onPostExecute(String result) {

        }
    }

    public void renderTab(JSONArray jsonArray, JSONObject jsonResponse){
        try {
            page = jsonResponse.getInt("page");
            ad_page = jsonResponse.getInt("ad_page");
            sharedPreferenceConfig.writeAdPage(ad_page);

            Log.d(TAG, jsonArray.toString());

            int prev_is_ad = 0;
            int mediaType = 0;
            int topArticleSize = 4;
            ArrayList<Article> adArticles = new ArrayList<>();
            Gson gson = new Gson();
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject articleJson = jsonArray.getJSONObject(i);

                if (prev_is_ad == 1 && articleJson.getInt("is_news") == 1) {
                    Article article = new Article();
                    ArrayList<Article> temp = new ArrayList<>();
                    temp.addAll(adArticles);
                    article.setAdArticles(temp);
                    article.setAdMediaType(mediaType);
                    Log.d(TAG, "mediatype=" + mediaType + ", is_news=" + articleJson.getInt("is_news") + ", article=" + gson.toJson(article));
                    articles.add(article);
                    int size = articles.size();
                    Log.d(TAG, "adarticle_size=" + articles.get(size - 1).getAdArticles().size());
                    prev_is_ad = 0;
                    adArticles.clear();
                    Log.d(TAG, "adarticle_size=" + articles.get(size - 1).getAdArticles().size());
                }

                if (articleJson.getInt("is_news") == 1) {
                    articles.add(new Article(articleJson.getString("id"),
                            articleJson.getString("title"),
                            articleJson.getString("urlToImage"),
                            "",
                            articleJson.getString("name"),
                            "img",
                            articleJson.getString("description"),
                            articleJson.getString("content"),
                            articleJson.getString("url"),
                            articleJson.getString("publishedAt"),
                            Config.ARTICLE_ITEM_IMAGE,
                            articleJson.getInt("category_id")));


                    if (topArticles.size() <= topArticleSize) {
                        topArticles.add(new Article(articleJson.getString("id"),
                                articleJson.getString("title"),
                                articleJson.getString("urlToImage"),
                                "",
                                articleJson.getString("name"),
                                "img",
                                articleJson.getString("description"),
                                articleJson.getString("content"),
                                articleJson.getString("url"),
                                articleJson.getString("publishedAt"),
                                Config.ARTICLE_ITEM_IMAGE,
                                articleJson.getInt("category_id")));
                    }
                    mAdapter.notifyDataSetChanged();
                } else {
                    String adType = articleJson.getString("media_type");
                    if (adType.equals("BIMG") || adType.equals("BVID") || adType.equals("BGIF")) {
                        mediaType = 0;
                        if (adType.equals("BIMG") || adType.equals("BGIF")) {
                            mediaType = Config.ARTICLE_ITEM_AD;
                        } else if (adType.equals("BVID")) {
                            mediaType = Config.ARTICLE_ITEM_AD_VIDEO;
                        }

                        if (mediaType != 0) {
                            adArticles.add(new Article(articleJson.getString("advertise_id"),
                                    articleJson.getString("title"),
                                    "http://news.thecgnow.com" + articleJson.getString("media_url"),
                                    articleJson.getString("ad_url"),
                                    mediaType));

                            prev_is_ad = 1;
                        }
                    }
                    Log.d(TAG, "mediatype=" + mediaType);
                }

            }
            setUpAutoScrollViewPager();
            Log.d(TAG + "tks", "srticle size:" + articles.size() + " ref no." + num + "section no." + sectionNumber);
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

    private void registerFcmToken() {

        String get_query = "?token="+sharedPreferenceConfig.readFcmToken()+"&master_key="+Config.MASTER_KEY;
        StringRequest stringRequest = new StringRequest(Request.Method.GET, Config.REGISTER_TOKEN_URL+get_query,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d(TAG, "token response:"+response);
                        if (response.contains("success")) {
                            Log.d(TAG, "token register success");
//                            sharedPreferenceConfig.writeFcmToken(token);
//                            notifyTokenGenerated();
                            fetchData();
                        } else {
                            Log.d(TAG, "token register failed");
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
//                        Toast.makeText(MainActivity.this, getResources().getString(R.string.couldnot_connect),Toast.LENGTH_LONG).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                //Adding the parameters to the request
                params.put("token", sharedPreferenceConfig.readFcmToken()); //sharedPreferenceConfig.readFcmToken()
                params.put("master_key", Config.MASTER_KEY);
//                params.put("pre_token", sharedPreferenceConfig.readFcmToken());
                return params;
            }
        };

        //Adding request the the queue
        requestQueue.add(stringRequest);
    }

    private void shuffleArticles() {
       /* int n = articles.size();
        Random random = new Random();
        random.nextInt();
        for (int i = 0; i < n; i++) {
            int change = i + random.nextInt(n - i);
            swap(i, change);
        }*/

        addOtherContentsInArticle(sectionNumber);
        if (advertizes.size()>0) {
            addAdvertisePos();
        }

    }

    private void addAdvertisePos() {
        Random random = new Random();
        int index;
        for (int i=1; i<=(articles.size()/adInterval); i++) {
            int randomNumber = random.nextInt(advertizes.size());
            Advertize advertize = advertizes.get(randomNumber);
            index = i*adInterval;
            if (advertize.getMediaType().equals("BIMG")) {
                Log.d(TAG, "bimg called type:" + advertize.getMediaType() + " section:" + sectionNumber);
                addAd(advertize.getTitle(), advertize.getMediaUrl(), advertize.getAdUrl(), Config.ARTICLE_ITEM_AD, index);
//                articles.set(index, new Article("", "title changed", advertize.getMediaUrl(), "", advertize.getAdUrl(), Config.ARTICLE_ITEM_AD));
            } else if (advertize.getMediaType().equals("BVID")) {
                Log.d(TAG, "bvid called type:" + advertize.getMediaType() + " section:" + sectionNumber);
                addAd(advertize.getTitle(), advertize.getMediaUrl(), advertize.getAdUrl(), Config.ARTICLE_ITEM_AD_VIDEO, index);
//                articles.set(index, new Article("", "title changed", advertize.getMediaUrl(), "", advertize.getAdUrl(), Config.ARTICLE_ITEM_AD_VIDEO));
            } else if (advertize.getMediaType().equals("BGIF")) {
                Log.d(TAG, "bgif called type:" + advertize.getMediaType() + " section:" + sectionNumber);
                addAd(advertize.getTitle(), advertize.getMediaUrl(), advertize.getAdUrl(), Config.ARTICLE_ITEM_AD_GIF, index);
//                articles.set(index, new Article("", "title changed", advertize.getMediaUrl(), "", advertize.getAdUrl(), Config.ARTICLE_ITEM_AD_GIF));
            }
        }
    }

    public void swap(int i, int change) {
        Article helper = articles.get(i);
        articles.set(i, articles.get(change));
        articles.set(change, helper);
    }

    private void addOtherContentsInArticle(int sn) {
        int random = new Random().nextInt(5) + 10;
        Log.d(TAG+"asize","size:"+articles.size()+" sn: "+sn);
        adPositions.clear();

        int adCount=0, topicCount=0, articleSize=articles.size();

        int catId = mDB.categoryDao().getCatIdWithName(categoryName);
        int currentAdPosition = Integer.parseInt(recyclerView.getTag().toString());
        int count=0;
        for (Advertize ad : publicAds.getAdvertizes()) {
            if ((ad.getRegionId() == catId && count++ > currentAdPosition) || defaultCategories.contains(categoryName)) {
                for (int i = 0; i < articles.size(); i++) {
                    if (sn == sectionNumber) {
                        if (i % 6 == 0 && i != 0) {
                            try {
//                                Advertize advertize = publicAds.getAdvertizes().get(0);
                                if (ad.getMediaType().equals("BIMG")) {
                                    addAd("Advertise",
                                            ad.getMediaUrl(),
                                            ad.getAdUrl(),
                                            Config.ARTICLE_ITEM_AD,
                                            i);
                                    num++;
                                    adCount++;
                                    adPositions.add(i);
                                } else if (ad.getMediaType().equals("BVID")) {
                                    addAd("Advertise",
                                            ad.getMediaUrl(),
                                            ad.getAdUrl(),
                                            Config.ARTICLE_ITEM_AD_VIDEO,
                                            i);
                                    num++;
                                    adCount++;
                                    adPositions.add(i);
                                } else if (ad.getMediaType().equals("BGIF")) {
                                    addAd("Advertise",
                                            ad.getMediaUrl(),
                                            ad.getAdUrl(),
                                            Config.ARTICLE_ITEM_AD_GIF,
                                            i);
                                    num++;
                                    adCount++;
                                    adPositions.add(i);
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }


                       /* Log.d(TAG, "adposval: " + adPositions.get(adPositions.size() - 1));
                        Log.d(TAG, "size: " + articles.size() + " sizeadpos: " + adPositions.size() + " i=" + i);*/
                        }

                   /* if (i % 11 == 0 &i!=0) {
                        articles.add(i, new Article("", Config.SUGGEST_CIRCLE, "Topics you should follow", "", topics));
                        random = new Random().nextInt(5) + 10; // [0, 5] + 20 => [10, 15]
                        Log.d(TAG, "topic sec." + sectionNumber + " pos: " + i);
                        topicCount++;
                    }*/
                    }
                }

                break;
            }
        }

            Log.d(TAG, "section: "+sectionNumber+" adCount: "+adCount+" topicCount: "+topicCount+" noadArticleSize: "+articleSize+ " articleCount: "+articles.size());
            printArticlesInLog(sn);
            topicCount=0; adCount=0; articleSize=0;

    }

    private void printArticlesInLog(int sn) {
        ArrayList<String> temp=new ArrayList<>();
        try {
            for (Article article : articles) {
                temp.add(article.getTitle());
            }
            Gson gson = new Gson();
            String articleArray = gson.toJson(temp);
            Log.d(TAG + "prt", " asize:"+articles.size()+" sn."+sectionNumber+" data: " + articleArray);
        } catch (Exception e) {
            e.printStackTrace();
        }
        num=0;
    }

    private void showAdPosValues() {
        for (int i:adPositions) {
            Log.d(TAG, "adpos: "+i);
        }
    }

    private void checkAdVisibility() {
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

                if (newState == AbsListView.OnScrollListener.SCROLL_STATE_IDLE) {
                    //get the recyclerview position which is completely visible and first
                    int positionView = ((LinearLayoutManager) recyclerView.getLayoutManager()).findFirstCompletelyVisibleItemPosition();
                    Log.i("VISISBLE", positionView + "");
                    if (positionView >= 0) {
                        if (oldFocusedLayout != null) {

                        }

                        currentFocusedLayout = ((LinearLayoutManager) recyclerView.getLayoutManager()).findViewByPosition(positionView);
                        Log.d(TAG, "currentpos: "+positionView);
//                        View currntView = mLayoutManager.findViewByPosition(positionView);
                        focusOnView(currentFocusedLayout);
                        if (adPositions.contains(positionView) && oldFocusedLayout!=currentFocusedLayout) {
                            Config.AD_VISIBILITY_COUNTER++;
                            Log.d(TAG, "ad viewed count: " + Config.AD_VISIBILITY_COUNTER);
                        }
                        oldFocusedLayout = currentFocusedLayout;
                    }
                }
            }
        });
    }

    private void focusOnView(final View view){
        new Handler().post(new Runnable() {
            @Override
            public void run() {
//                mainScrollView.smoothScrollTo(0, ticketNum.getTop());
                ObjectAnimator.ofInt(recyclerView, "scrollY",  view.getTop()).setDuration(1000).start();
            }
        });
    }

    private void setUpRecyclerView() {
        mAdapter = new ArticleHomeAdapter(articles, mCtx, getActivity().getSupportFragmentManager());
        mLayoutManager = new LinearLayoutManager(mCtx);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemViewCacheSize(20);
        recyclerView.setAdapter(mAdapter);
    }

    private void initView(View view) {
        recyclerView=view.findViewById(R.id.home_foryou_rv);
        updateMsg=view.findViewById(R.id.update_txt);
        progressBar=view.findViewById(R.id.progress_circular); // suspected
        refreshll=view.findViewById(R.id.refresh_ll);
        refreshBtn=view.findViewById(R.id.refresh_btn);
        cardView=view.findViewById(R.id.cv);
        loading=view.findViewById(R.id.loading_anim);
        loading_bottom = view.findViewById(R.id.loading_bottom);

        nestedScrollView=view.findViewById(R.id.nested_sv);
        sliderView = view.findViewById(R.id.imageSlider);
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        mCtx=context;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        articles.clear();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mCtx.unregisterReceiver(broadcastReceiver);
    }

    @Override
    public void onClick(View view) {
        if (view.getId()==R.id.refresh_btn) {
            articles.clear();
            page=0;
            fetchData();
        }
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
//        inflater.inflate(R.menu.fragment_menu, menu);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }
}
