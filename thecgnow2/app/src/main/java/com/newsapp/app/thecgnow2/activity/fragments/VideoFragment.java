package com.newsapp.app.thecgnow2.activity.fragments;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.newsapp.app.thecgnow2.R;
import com.newsapp.app.thecgnow2.activity.ui.video.SectionsPagerAdapter;
import com.google.android.material.tabs.TabLayout;

public class VideoFragment extends Fragment {

    Context mCtx;
    public final String TAG="VideoFragment";

    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view= inflater.inflate(R.layout.fragment_video, container, false);

        Log.d(TAG, "fragment video");
       SectionsPagerAdapter sectionsPagerAdapter = new SectionsPagerAdapter(getActivity(),getChildFragmentManager());
        ViewPager viewPager = view.findViewById(R.id.view_pager_video);
        viewPager.setAdapter(sectionsPagerAdapter);
        TabLayout tabs = view.findViewById(R.id.tabs_video);
        tabs.setupWithViewPager(viewPager);

        return view;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        mCtx=context;
    }
}
