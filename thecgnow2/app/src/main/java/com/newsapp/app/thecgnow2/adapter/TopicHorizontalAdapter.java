package com.newsapp.app.thecgnow2.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import androidx.room.Room;

import com.newsapp.app.thecgnow2.Config;
import com.newsapp.app.thecgnow2.R;
import com.newsapp.app.thecgnow2.model.Topic;
import com.newsapp.app.thecgnow2.roomDB.CGNowDB;
import com.newsapp.app.thecgnow2.roomDB.follow.FollowRoom;

import java.util.ArrayList;

public class TopicHorizontalAdapter extends RecyclerView.Adapter<TopicHorizontalAdapter.HorizontalViewHolder> {

    ArrayList<Topic> topics;
    Context mCtx;
    static CGNowDB mDB;
    int viewType;

    public TopicHorizontalAdapter(ArrayList<Topic> topics, Context mCtx, int type) {
        this.topics = topics;
        this.mCtx = mCtx;
        this.viewType = type;
        mDB = Room.databaseBuilder(mCtx, CGNowDB.class,  mCtx.getResources().getString(R.string.db_string)).allowMainThreadQueries().build();
    }

    @Override
    public int getItemViewType(int position) {
        switch (viewType) {
            case Config.SUGGEST_RECT:
                return Config.SUGGEST_RECT;
            case Config.SUGGEST_CIRCLE:
                return Config.SUGGEST_CIRCLE;
            default:
                return 0;
        }
    }

    @NonNull
    @Override
    public HorizontalViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView;
        switch (viewType) {
            case Config.SUGGEST_RECT:
                itemView = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.article_suggest_topic_one, parent, false);
                return new HorizontalViewHolder(itemView);
            case Config.SUGGEST_CIRCLE:
                itemView = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.article_suggest_topic_two, parent, false);
                return new HorizontalViewHolder(itemView);
        }

        return null;
    }

    @Override
    public void onBindViewHolder(@NonNull HorizontalViewHolder holder, int position) {

        ((HorizontalViewHolder) holder).suggestTopics(topics.get(position), mCtx);

    }

    @Override
    public int getItemCount() {
        return topics.size();
    }

    public static class HorizontalViewHolder extends RecyclerView.ViewHolder {
        TextView srcName, followTxt, profileTxt;
        public HorizontalViewHolder(@NonNull View itemView) {
            super(itemView);
            srcName=itemView.findViewById(R.id.source_name);
            followTxt=itemView.findViewById(R.id.follow_txt);
            profileTxt=itemView.findViewById(R.id.profile_txt);
        }

        public void suggestTopics(final Topic topic, final Context mCtx) {
            if (mDB.followDao().getFollowingWithSrcName(topic.getSrcName())==null) {
                followTxt.setText(mCtx.getResources().getString(R.string.follow_btn));
            } else {
                followTxt.setText(mCtx.getResources().getString(R.string.following_btn));
            }

            profileTxt.setText(topic.getSrcName().charAt(0)+"");

            followTxt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    FollowRoom data = new FollowRoom();
                    data.setSourceName(topic.getSrcName());
                    if (mDB.followDao().getFollowingWithSrcName(topic.getSrcName())==null) {
                        mDB.followDao().followTo(data);
                        followTxt.setText((mCtx.getResources().getString(R.string.following_btn)));
//                        Toast.makeText(mCtx, "followed to " + topic.getSrcName(), Toast.LENGTH_SHORT).show();
                    } else {
                        mDB.followDao().unFollowTo(data.getSourceName());
                        followTxt.setText(mCtx.getResources().getString(R.string.follow_btn));
//                        Toast.makeText(mCtx, "unfollowed to " + topic.getSrcName(), Toast.LENGTH_SHORT).show();
                    }
                }
            });

            srcName.setText(topic.getSrcName());
        }
    }
}
