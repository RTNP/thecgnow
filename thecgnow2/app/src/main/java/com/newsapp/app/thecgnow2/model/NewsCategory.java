package com.newsapp.app.thecgnow2.model;

import com.google.gson.annotations.SerializedName;
import com.newsapp.app.thecgnow2.TheCGNowApp;

public class NewsCategory {

    @SerializedName("id")
    int id;

    @SerializedName("active")
    int active;

    @SerializedName("category_order")
    int catOrder;

    @SerializedName("region_name_hi")
    String name;

    @SerializedName("region_name")
    String keyName;

    public NewsCategory(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public NewsCategory(int id, String name, String keyName) {
        this.id = id;
        this.name = name;        // used for display name
        this.keyName = keyName;  // used for keyword
    }

    public NewsCategory(int id, int active, String name, String keyName, int catOrder) {
        this.id = id;
        this.active = active;
        this.name = name;
        this.keyName = keyName;
        this.catOrder = catOrder;
    }

    public NewsCategory(int id, String name, String keyName, int order) {
        this.id = id;
        this.name = name;        // used for display name
        this.keyName = keyName;  // used for keyword
        this.catOrder = order;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getKeyName() {
        return keyName;
    }

    public void setKeyName(String keyName) {
        this.keyName = keyName;
    }

    public int getCatOrder() {
        return catOrder;
    }

    public void setCatOrder(int catOrder) {
        this.catOrder = catOrder;
    }

    public int getActive() {
        return active;
    }

    public void setActive(int active) {
        this.active = active;
    }

    @Override
    public String toString() {
        String lang;
        try {
            lang = TheCGNowApp.locale;
        } catch (Exception e) {
            e.printStackTrace();
            lang = "en";
        }
//        Log.d("NewsCategory", "lang: "+lang);
        try {
            if (lang.equalsIgnoreCase("hi")) {
                return name;
            } else {
                return keyName;
            }
        } catch (Exception e) {
            return keyName;
        }

    }
}
