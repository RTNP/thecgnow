package com.newsapp.app.thecgnow2.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.newsapp.app.thecgnow2.Config;
import com.newsapp.app.thecgnow2.GpsTracker;
import com.newsapp.app.thecgnow2.MakeRequest;
import com.newsapp.app.thecgnow2.R;
import com.newsapp.app.thecgnow2.SharedPreferenceConfig;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;
import okhttp3.logging.HttpLoggingInterceptor;

public class SplashScreenActivity extends AppCompatActivity {

    SharedPreferenceConfig sharedPreferenceConfig;
    private GpsTracker gpsTracker;
    Handler handler = new Handler();
    Runnable runnable;

    String TAG = "SplashScreenActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_splash_screen);

        sharedPreferenceConfig = new SharedPreferenceConfig(this);

        setContentView(R.layout.activity_splash_screen);
        if (ContextCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED ) {
            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, 101);
        }
        getLocation();
        checkLocation();
        Log.i("fcm",sharedPreferenceConfig.readFcmToken());
        Log.i("data",sharedPreferenceConfig.readFcmToken().equalsIgnoreCase("")+" - "+sharedPreferenceConfig.readLatitude().equalsIgnoreCase("")+" - "+sharedPreferenceConfig.readLongitude().equalsIgnoreCase(""));
    }

    private void checkLocation() {
        runnable = new Runnable() {
            @Override
            public void run() {
                if(!sharedPreferenceConfig.readFcmToken().equalsIgnoreCase("")
                        && !sharedPreferenceConfig.readLatitude().equalsIgnoreCase("")
                        && !sharedPreferenceConfig.readLongitude().equalsIgnoreCase("")) {
                    validateServer();
                }else{
                    handler.postDelayed(runnable,2000);
                }
            }
        };

        if(!sharedPreferenceConfig.readFcmToken().equalsIgnoreCase("")
                && !sharedPreferenceConfig.readLatitude().equalsIgnoreCase("")
                && !sharedPreferenceConfig.readLongitude().equalsIgnoreCase("")){
            validateServer();
        }else{
            if(sharedPreferenceConfig.readFcmToken().equalsIgnoreCase("")){
                FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(SplashScreenActivity.this, new OnSuccessListener<InstanceIdResult>() {
                    @Override
                    public void onSuccess(InstanceIdResult instanceIdResult) {
                        String token = instanceIdResult.getToken();
                        Log.i("FCM Token", token);
                        SharedPreferenceConfig sharedPreferenceConfig = new SharedPreferenceConfig(SplashScreenActivity.this);
                        sharedPreferenceConfig.writeFcmToken(token);
                    }
                });
            }
            handler.postDelayed(runnable,2000);
        }
    }
    private void validateServer() {
        if(sharedPreferenceConfig.readFcmTokenRegistered()){
//            Toast.makeText(getApplicationContext(),"Toast",Toast.LENGTH_LONG).show();
            initializeData();
            Log.i("Server Status","Registered to Server");
        }else{
            requestServer();
            Log.i("Server Status","Not Registered to Server");
        }
    }
    private void launchMainActivity() {
        Intent intent = new Intent(this,MainActivity.class);
        startActivity(intent);
        finish();
    }
    public void getLocation(){
        gpsTracker = new GpsTracker(SplashScreenActivity.this);
        if(gpsTracker.canGetLocation()){
            double latitude = gpsTracker.getLatitude();
            double longitude = gpsTracker.getLongitude();
            Log.i("latitude",latitude+"");
            Log.i("longitude",longitude+"");
            sharedPreferenceConfig.writeLatitude(latitude+"");
            sharedPreferenceConfig.writeLongitude(longitude+"");
        }else{
            gpsTracker.showSettingsAlert();
        }
    }
    public void requestServer(){
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BASIC);
        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(logging)
                .build();
        String requestString = "?master_key="+ Config.MASTER_KEY +"&token="+ sharedPreferenceConfig.readFcmToken();
        Request newReq = new Request.Builder()
                .url(Config.REGISTER_TOKEN_URL+requestString)
                .build();
        client.newCall(newReq).enqueue(new Callback() {
            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {
                ((Activity) SplashScreenActivity.this).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getApplicationContext(),"Error : " + e.getMessage(),Toast.LENGTH_LONG).show();
                    }
                });
            }
            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {
                try (ResponseBody responseBody = response.body()) {
                    if (!response.isSuccessful()) throw new IOException("Unexpected code " + response);
                    String myResponse = response.body().string();
                    ((Activity) SplashScreenActivity.this).runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if(myResponse.equalsIgnoreCase("success")){
                                sharedPreferenceConfig.writeFcmTokenRegistered(true);
                                initializeData();
                            }else{
                                sharedPreferenceConfig.writeFcmTokenRegistered(false);
                            }
                        }
                    });
                }catch (Exception ex){
                    ex.printStackTrace();
                }
            }
        });
    }
    public void initializeData(){
        String requestData = "?latitude="+sharedPreferenceConfig.readLatitude()+"&longitude="+sharedPreferenceConfig.readLongitude()+"&auth_key="+sharedPreferenceConfig.readFcmToken();
        MakeRequest.getRequestOnSharedPreference(Config.GET_CATEGORY_URL+requestData,SplashScreenActivity.this,Config.categoryListKey);
        MakeRequest.getRequestOnSharedPreference(Config.GET_JOB_LIST_URL+requestData,SplashScreenActivity.this,Config.jobListKey);
        MakeRequest.getRequestOnSharedPreference(Config.GET_OFFER_LIST_URL+requestData,SplashScreenActivity.this,Config.offerListKey);
        MakeRequest.getRequestOnSharedPreference(Config.GET_OFFER_CATEGORIES_URL+requestData,SplashScreenActivity.this,Config.offerCategoryKey);
        MakeRequest.getRequestOnSharedPreference(Config.GET_CURRENT_AFFAIR_LIST_URL+requestData,SplashScreenActivity.this,Config.currentAffairKey);
        MakeRequest.getRequestOnSharedPreference(Config.GET_TAMBOLA_IN_APP+requestData,SplashScreenActivity.this,Config.tambolaKey);
        MakeRequest.getRequestOnSharedPreference(Config.GET_NOTIFICATION_URL+requestData,SplashScreenActivity.this,Config.notificationKey);
        MakeRequest.getRequestOnSharedPreference(Config.GET_WEB_SOCKET_URL+requestData,SplashScreenActivity.this,Config.webSocketKey);
        Log.e(TAG,"onSplashScreen: ."+Config.GET_TAMBOLA_IN_APP+requestData);
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                launchMainActivity();
            }
        },5000);
    }

}
