package com.newsapp.app.thecgnow2.activity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.newsapp.app.thecgnow2.Config;
import com.newsapp.app.thecgnow2.R;
import com.newsapp.app.thecgnow2.SharedPreferenceConfig;
import com.newsapp.app.thecgnow2.adapter.ArticleHomeAdapter;
import com.newsapp.app.thecgnow2.model.Article;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class SearchArticleActivity extends AppCompatActivity {

    private ArrayList<Article> articles = new ArrayList<>();
    private RecyclerView recyclerView;
    private ArticleHomeAdapter mAdapter;
    ProgressBar loading;
    EditText search;
    ImageView searchImg, backImg;
    private RequestQueue requestQueue;
    boolean isLoading = false;
    String query="";
    int page = 0;
    SharedPreferenceConfig sharedPreferenceConfig;
    String TAG = "SearchArticleActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_article);

        initView();
        setUpRecyclerView();
        initScrollListener();
        sharedPreferenceConfig = new SharedPreferenceConfig(this);
        requestQueue= Volley.newRequestQueue(this);
        searchImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                searchImg.setImageResource(R.drawable.ic_search_purple);
//                searchImg.setColorFilter(Col);
                query = search.getText().toString();
                articles.clear();
                recyclerView.setVisibility(View.GONE);
                fetchData(query);
            }
        });

        backImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    private void initScrollListener() {
            recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                    super.onScrollStateChanged(recyclerView, newState);
                    Log.d(TAG, "scrollstatechanged");
                }

                @Override
                public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);

                    Log.d(TAG, "onscrolled");

                    LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();

                    if (!isLoading) {
                        if (linearLayoutManager != null && linearLayoutManager.findLastCompletelyVisibleItemPosition() == articles.size() - 1) {
                            //bottom of list!
                            isLoading = true;

                            loadMore();
                        }
                    }
                }
            });
    }

    private void loadMore() {
        page++;
        if (articles.size()>0)
            articles.remove(articles.size()-1);
        fetchData(query);
    }

    private void fetchData(String query) {

        if (articles.size()==0)
            loading.setVisibility(View.VISIBLE);
        String get_query = "?&q="+query+"&page="+page+"&auth_key="+sharedPreferenceConfig.readFcmToken();
        Log.d(TAG, "query:"+Config.WEB_HOSE_API_URL+get_query);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, Config.WEB_HOSE_API_URL+get_query,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        loading.setVisibility(View.GONE);
                        isLoading = false;
                        recyclerView.setVisibility(View.VISIBLE);
//                        searchImg.setImageResource(R.drawable.ic_search_black);
                        Log.d(TAG,"response="+query+" "+response);

//                            articles.clear();
//                        Toast.makeText(AttendenceActivity.this, "attendance response"+response, Toast.LENGTH_SHORT).show();
                        try {
                            //Creating the json object from the response
                            JSONObject jsonResponse = new JSONObject(response);
                            JSONArray jsonArray = jsonResponse.getJSONArray("articles");
                            Log.d(TAG, jsonArray.toString());
//                            Toast.makeText(AttendenceActivity.this, ""+jsonArray.toString(), Toast.LENGTH_SHORT).show();

                            String sourceVal, articleId;
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject articleJson = jsonArray.getJSONObject(i);

                                   /* articleId = articleJson.getJSONObject("source").getString("name")
                                            + articleJson.getString("publishedAt");
                                    sourceVal = articleJson.getJSONObject("source").getString("name");*/

                                articles.add(new Article(articleJson.getString("id"),
                                        articleJson.getString("title"),
                                        articleJson.getString("urlToImage"),
                                        "",
                                        articleJson.getString("name"),
                                        "img",
                                        articleJson.getString("description"),
                                        articleJson.getString("content"),
                                        articleJson.getString("url"),
                                        articleJson.getString("publishedAt"),
                                        Config.ARTICLE_ITEM_IMAGE));
//                                articles.add(new Article(i,
//                                        jresponse.getString("batch_code")))

                            }

                            articles.add(new Article(Config.LOADING_VIEW));

                            mAdapter.notifyDataSetChanged();
                            Log.d(TAG + "tks", "srticle size:" + articles.size());

//                            jsonResponse.getString(Config.TAG_RESPONSE).equalsIgnoreCase("Success");

                        } catch (JSONException e) {
                            e.printStackTrace();
                            if (response.contains("Access Denied") && !sharedPreferenceConfig.readFcmToken().isEmpty()) {
//                                registerFcmToken();
                            }
                            Log.d(TAG, "mobileerror:" + e);
                            loading.setVisibility(View.GONE);
                            isLoading = false;
                            recyclerView.setVisibility(View.VISIBLE);
//                            searchImg.setImageResource(R.drawable.ic_search_black);
//                            Toast.makeText(getActivity(), ""+e.toString(), Toast.LENGTH_SHORT).show();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {

                        String message = null;
                        if (volleyError instanceof NetworkError) {
                            message = "Cannot connect to Internet...Please check your connection!";
                        } else if (volleyError instanceof ServerError) {
                            message = "The server could not be found. Please try again after some time!!";
                        } else if (volleyError instanceof AuthFailureError) {
                            message = "Cannot connect to Internet....Please check your connection!";
                        } else if (volleyError instanceof ParseError) {
                            message = "Parsing error! Please try again after some time!!";
                        } else if (volleyError instanceof NoConnectionError) {
                            message = "Cannot connect to Internet.....Please check your connection!";
                        } else if (volleyError instanceof TimeoutError) {
                            message = "Connection TimeOut! Please check your internet connection.";
                        }

                        if (message!=null) {
                            loading.setVisibility(View.GONE);
                            isLoading = false;
                            recyclerView.setVisibility(View.VISIBLE);
//                            searchImg.setImageResource(R.drawable.ic_search_black);
                            Log.d(TAG, "message="+message+" volleyError:"+volleyError);
                        }
//                        Toast.makeText(mCtx, mCtx.getResources().getString(R.string.couldnot_connect),Toast.LENGTH_LONG).show();
                    }
                }) {
        };

        //Adding request the the queue
        requestQueue.add(stringRequest);
    }

    private void setUpRecyclerView() {
        mAdapter = new ArticleHomeAdapter(articles, this, getSupportFragmentManager());
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(mAdapter);
    }

    private void initView() {
        recyclerView = findViewById(R.id.article_rv);
        loading = findViewById(R.id.loading);
        search = findViewById(R.id.search_et);
        searchImg = findViewById(R.id.search_img);
        backImg = findViewById(R.id.back_img);
    }
}