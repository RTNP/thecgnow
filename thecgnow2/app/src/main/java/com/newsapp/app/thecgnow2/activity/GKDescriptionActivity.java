package com.newsapp.app.thecgnow2.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.newsapp.app.thecgnow2.Config;
import com.newsapp.app.thecgnow2.R;
import com.newsapp.app.thecgnow2.SharedPreferenceConfig;
import com.newsapp.app.thecgnow2.TheCGNowApp;
import com.newsapp.app.thecgnow2.model.Advertize;
import com.newsapp.app.thecgnow2.model.GK;
import com.google.android.exoplayer2.ExoPlayer;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory;
import com.google.android.exoplayer2.extractor.ExtractorsFactory;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelector;
import com.google.android.exoplayer2.ui.SimpleExoPlayerView;
import com.google.android.exoplayer2.upstream.BandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultHttpDataSourceFactory;
import com.google.gson.Gson;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import static com.newsapp.app.thecgnow2.activity.MainActivity.publicAds;

public class GKDescriptionActivity extends AppCompatActivity implements View.OnClickListener {

    TextView title, description;
    ImageView image, ad_label, ad_banner_img, mute;
    CardView videoCard, imageCard;
    TheCGNowApp cgNowApp;
    public String TAG = "GKDescription";
    BroadcastReceiver broadcastReceiver;
    SimpleExoPlayerView simpleExoPlayerView;
    SimpleExoPlayer simpleExoPlayer;
    RequestQueue requestQueue;
    SharedPreferenceConfig sharedPreferenceConfig;
    Advertize bannerAd, labelAd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gkdescription);

        initView();

        requestQueue = Volley.newRequestQueue(this);
        sharedPreferenceConfig = new SharedPreferenceConfig(this);

        registerBroadcastReceiver();

        setUI();
        getAdvertises();
        refreshAds();
        mute.setOnClickListener(this);
        videoCard.setOnClickListener(this);
        imageCard.setOnClickListener(this);
        ad_label.setOnClickListener(this);
    }

    private void registerBroadcastReceiver() {
        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
//                refreshAds();
            }
        };
        registerReceiver(broadcastReceiver, new IntentFilter("com.example.thecgnow.activity.adRefresh"));
    }

    private void getAdvertises() {
        String get_query = "?latitude="+sharedPreferenceConfig.readLatitude()+"&longitude="+sharedPreferenceConfig.readLongitude()+"&ad_page="+sharedPreferenceConfig.readAdPage()+"&auth_key="+sharedPreferenceConfig.readFcmToken();
        Log.d(TAG, "urlquery="+ Config.GET_ARTICLE_DESC_ADS+get_query);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, Config.GET_ARTICLE_DESC_ADS+get_query,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

//                        progressBar.setVisibility(View.GONE);

                        Log.d(TAG,"res="+response);
//                        Toast.makeText(AttendenceActivity.this, "attendance response"+response, Toast.LENGTH_SHORT).show();
                        try {
                            //Creating the json object from the response
                            JSONObject jsonResponse = new JSONObject(response);
                            JSONArray jsonArray = jsonResponse.getJSONArray("bannerdata");
                            JSONArray jsonArray2 = jsonResponse.getJSONArray("labeldata");
                            Log.d(TAG,"data:"+jsonArray.toString());
//                            Toast.makeText(AttendenceActivity.this, ""+jsonArray.toString(), Toast.LENGTH_SHORT).show();

                            for (int i=0; i<jsonArray.length(); i++) {
                                JSONObject jsonObject =jsonArray.getJSONObject(i);
                                bannerAd = new Advertize(jsonObject.getInt("advertise_id"),
                                        jsonObject.getInt("cat_id"),
                                        jsonObject.getInt("region_id"),
                                        jsonObject.getString("title"),
                                        Config.BASE_URL+jsonObject.getString("media_url"),
                                        jsonObject.getString("media_type"),
                                        jsonObject.getString("date"),
                                        jsonObject.getInt("ad_time"),
                                        jsonObject.getString("ad_url"));
                            }

                            for (int i=0; i<jsonArray2.length(); i++) {
                                JSONObject jsonObject =jsonArray2.getJSONObject(i);
                                labelAd = new Advertize(jsonObject.getInt("advertise_id"),
                                        jsonObject.getInt("cat_id"),
                                        jsonObject.getInt("region_id"),
                                        jsonObject.getString("title"),
                                        Config.BASE_URL+jsonObject.getString("media_url"),
                                        jsonObject.getString("media_type"),
                                        jsonObject.getString("date"),
                                        jsonObject.getInt("ad_time"),
                                        jsonObject.getString("ad_url"));
                            }

                            setAdvertiseUI();


                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.d(TAG,"mobileerror: "+e);
//                            Toast.makeText(getActivity(), ""+e.toString(), Toast.LENGTH_SHORT).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {


//                        loading.dismiss();
//                        progressBar.setVisibility(View.GONE);
                        Log.d(TAG, error.getMessage()+" tostr:"+error.toString());
                        Toast.makeText(GKDescriptionActivity.this, getResources().getString(R.string.couldnot_connect),Toast.LENGTH_LONG).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                //Adding the parameters to the request
//                params.put("id", id);
                params.put("auth_key", sharedPreferenceConfig.readFcmToken());
                return params;
            }


        };

        //Adding request the the queue
        requestQueue.add(stringRequest);
    }

    private void setAdvertiseUI() {
        Picasso.get().load(bannerAd.getMediaUrl()).placeholder(R.drawable.placeholder).into(ad_banner_img);
        Picasso.get().load(labelAd.getMediaUrl()).placeholder(R.drawable.labelph).into(ad_label);
    }


    public void refreshAds() {
        try {
            Picasso.get().load(publicAds.getLabelAdvertizes().get(publicAds.labelAdItemPosition).getMediaUrl()).into(ad_label);
            String adtype=publicAds.getAdvertizes().get(publicAds.adBannerItemPosition).getMediaType();
            if (adtype.equals("BIMG")) {
                imageCard.setVisibility(View.VISIBLE);
                videoCard.setVisibility(View.GONE);
                Picasso.get().load(publicAds.getAdvertizes().get(publicAds.adBannerItemPosition).getMediaUrl()).into(ad_banner_img);
            } else if (adtype.equals("BVID")) {
                imageCard.setVisibility(View.GONE);
                videoCard.setVisibility(View.VISIBLE);
                try {
                    BandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
                    TrackSelector trackSelector = new DefaultTrackSelector(new AdaptiveTrackSelection.Factory(bandwidthMeter));
                    simpleExoPlayer = ExoPlayerFactory.newSimpleInstance(this, trackSelector);
                    DefaultHttpDataSourceFactory dataSourceFactory = new DefaultHttpDataSourceFactory("exoplayer_video");
                    ExtractorsFactory extractorsFactory = new DefaultExtractorsFactory();
                    MediaSource mediaSource = new ExtractorMediaSource(Uri.parse(publicAds.getAdvertizes().get(publicAds.adBannerItemPosition).getMediaUrl()), dataSourceFactory, extractorsFactory, null, null);
                    simpleExoPlayerView.setPlayer(simpleExoPlayer);
                    simpleExoPlayer.prepare(mediaSource);
                    simpleExoPlayer.setPlayWhenReady(true);
                    simpleExoPlayer.setRepeatMode(ExoPlayer.REPEAT_MODE_ALL);
                } catch (Exception e) {
                    Log.d(TAG, "exoplayer error: "+e.toString());
                }
            } else if (adtype.equals("BGIF")) {
                imageCard.setVisibility(View.VISIBLE);
                videoCard.setVisibility(View.GONE);
                Glide.with(this)
                        .asGif()
                        .load(publicAds.getAdvertizes().get(publicAds.adBannerItemPosition).getMediaUrl())
                        .into(ad_banner_img);
//                Picasso.with(this).load(publicAds.getAdvertizes().get(publicAds.adBannerItemPosition).getMediaUrl()).into(ad_banner);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setUI() {
//        image.setVisibility(View.GONE);
        try {
            String gkArray = getIntent().getExtras().getString("gk");
            Gson gson = new Gson();
            GK gk=gson.fromJson(gkArray, GK.class);
            title.setText(gk.getTitle());
            description.setText(Html.fromHtml(gk.getDescription()));
            Picasso.get().load(gk.getImageUrl()).placeholder(R.drawable.placeholder).into(image, new Callback() {
                @Override
                public void onSuccess() {
                    // image.setVisibility(View.VISIBLE);
                }

                @Override
                public void onError(Exception e) {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void initView() {
        title=findViewById(R.id.title);
        description=findViewById(R.id.description);
        image=findViewById(R.id.image);
        videoCard=findViewById(R.id.video_ad_cv);
        imageCard=findViewById(R.id.image_ad_cv);
        ad_banner_img=findViewById(R.id.ad_banner_img);
        simpleExoPlayerView=findViewById(R.id.ad_banner_video);
        ad_label=findViewById(R.id.ad_label);
        mute=findViewById(R.id.mute);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(broadcastReceiver);
    }

    @Override
    public void onClick(View view) {
        if (view.getId()==R.id.mute) {
            float currentvolume = simpleExoPlayer.getVolume();
            Log.d(TAG, "currentVol: "+currentvolume);
            if (Float.compare(currentvolume, 0f)==0) {
                simpleExoPlayer.setVolume(1f);
                mute.setImageResource(R.drawable.unmute);
            } else {
                simpleExoPlayer.setVolume(0f);
                mute.setImageResource(R.drawable.mute);
            }
        }

        if (view.getId()==R.id.video_ad_cv) {
            String url=publicAds.getAdvertizes().get(publicAds.adBannerItemPosition).getAdUrl();
            redirectToUrl(url);
        }

        if (view.getId()==R.id.image_ad_cv) {
            String url=publicAds.getAdvertizes().get(publicAds.adBannerItemPosition).getAdUrl();
            redirectToUrl(url);
        }

        if (view.getId()==ad_label.getId()) {
            String url=publicAds.getLabelAdvertizes().get(publicAds.labelAdItemPosition).getAdUrl();
            redirectToUrl(url);
        }
    }

    private void redirectToUrl(String url) {   // 0 = video cardview, 1 = image cardview
        try {
            Intent viewIntent =
                    new Intent("android.intent.action.VIEW",
                            Uri.parse(url));
            viewIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            viewIntent.setPackage("com.android.chrome");
            try {
                startActivity(viewIntent);
            } catch (ActivityNotFoundException e) {
                // Chrome is probably not installed
                // Try with the default browser
                viewIntent.setPackage(null);
                startActivity(viewIntent);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
