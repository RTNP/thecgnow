package com.newsapp.app.thecgnow2.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;

import com.bumptech.glide.Glide;
import com.newsapp.app.thecgnow2.R;

import java.util.ArrayList;

public class CustomPagerAdapter extends PagerAdapter {

    private Context context;
    private ArrayList<String> imageUrls;
    private ArrayList<String> linkUrls;
    private ArrayList<String> titles;
    private LayoutInflater layoutInflater;

    public CustomPagerAdapter(Context context, ArrayList<String> imageUrls, ArrayList<String> linkUrls, ArrayList<String> titles) {
        this.context = context;
        this.imageUrls = imageUrls;
        this.linkUrls = linkUrls;
        this.titles = titles;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        String imageUrl = imageUrls.get(position);
        String linkUrl = linkUrls.get(position);
        String title = titles.get(position);

        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View view = layoutInflater.inflate(R.layout.image_slider_layout_item, null);

        ImageView imageView = view.findViewById(R.id.iv_auto_image_slider);
        Glide.with(context)
                .load(imageUrl)
                .fitCenter()
                .into(imageView);

        container.addView(view);

        return view;
    }

    @Override
    public int getCount() {
        return imageUrls.size();
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View)object);
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return (View)object == view;
    }

}
