package com.newsapp.app.thecgnow2.roomDB.article;

import androidx.room.Dao;
import androidx.room.Insert;

@Dao
public interface ArticleDao {
    @Insert
    public void addArticle(ArticleRoom article);
}
