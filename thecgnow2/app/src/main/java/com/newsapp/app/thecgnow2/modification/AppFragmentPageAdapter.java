package com.newsapp.app.thecgnow2.modification;

import com.newsapp.app.thecgnow2.SharedPreferenceConfig;
import com.newsapp.app.thecgnow2.activity.MainActivity;
import com.newsapp.app.thecgnow2.activity.fragments.GKFragment;
import com.newsapp.app.thecgnow2.activity.fragments.HomeFragment;
import com.newsapp.app.thecgnow2.activity.fragments.MyCityFragment;
import com.newsapp.app.thecgnow2.activity.fragments.PlayGameFragment;
import com.newsapp.app.thecgnow2.activity.fragments.housieboard.ComingSoonFragment;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import java.util.ArrayList;
import java.util.List;

public class AppFragmentPageAdapter extends FragmentPagerAdapter {

    private final List<Fragment> mFragmentList = new ArrayList<>();

    public AppFragmentPageAdapter(FragmentManager fm) {
        super(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        return mFragmentList.get(position);
    }

    @Override
    public int getCount() {
        return mFragmentList.size();
    }

    public void addFragment(Fragment fragment, int position) {
        mFragmentList.add(position, fragment);
    }

    public void removeFragment(Fragment fragment, int position) {
        mFragmentList.remove(position);
    }

}
