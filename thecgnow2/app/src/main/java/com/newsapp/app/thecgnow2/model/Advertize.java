package com.newsapp.app.thecgnow2.model;

public class Advertize {
    int id, catId, regionId, adTime;
    String title, mediaUrl, mediaType, date, adUrl;

    public Advertize() {
    }

    public Advertize(int id, int catId, int regionId, String title, String mediaUrl, String mediaType, String date, int adTime, String adUrl) {
        this.id = id;
        this.catId = catId;
        this.regionId = regionId;
        this.title = title;
        this.mediaUrl = mediaUrl;
        this.mediaType = mediaType;
        this.date = date;
        this.adTime = adTime;
        this.adUrl = adUrl;
    }

    public int getId() {
        return id;
    }

    public int getCatId() {
        return catId;
    }

    public int getRegionId() {
        return regionId;
    }

    public String getTitle() {
        return title;
    }

    public String getMediaUrl() {
        return mediaUrl;
    }

    public String getMediaType() {
        return mediaType;
    }

    public String getDate() {
        return date;
    }

    public int getAdTime() {
        return adTime;
    }

    public String getAdUrl() {
        return adUrl;
    }
}
