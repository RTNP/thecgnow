package com.newsapp.app.thecgnow2.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.newsapp.app.thecgnow2.R;
import com.newsapp.app.thecgnow2.model.Article;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class HorizontalRVAdapter extends RecyclerView.Adapter<HorizontalRVAdapter.HorizViewHolder> {
    ArrayList<Article> articles;
    Context mCtx;

    public HorizontalRVAdapter(ArrayList<Article> articles, Context mCtx) {
        this.articles = articles;
        this.mCtx = mCtx;
    }

    @NonNull
    @Override
    public HorizViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView;

        itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.article_banner, parent, false);

        return new HorizViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(HorizViewHolder holder, int position) {
        Article article = articles.get(position);
        Picasso.get().load(article.getImg()).placeholder(R.drawable.placeholder).into(holder.imageView);
    }

    @Override
    public int getItemCount() {
        return articles.size();
    }

    public class HorizViewHolder extends RecyclerView.ViewHolder {
        ImageView imageView;

        public HorizViewHolder(@NonNull View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.banner);
        }
    }
}
