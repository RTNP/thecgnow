package com.newsapp.app.thecgnow2.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class Article {
    int type;
    @SerializedName("category_id")
    int categoryId;
    int adId;
    @SerializedName("id")
    String id;
    @SerializedName("title")
    String title;
    @SerializedName("urlToImage")
    String img;
    String video;
    String srcName;
    String srcImage;
    @SerializedName("url")
    String url;
    @SerializedName("description")
    String description;
    @SerializedName("content")
    String content;
    @SerializedName("publishedAt")
    String publishedAt;
    String mediaType, date;
    ArrayList<Topic> topics;
    ArrayList<Article> adArticles;

    // use for main article
    public Article(String id, String title, String img, String video,String srcName, String srcImage, String description, String content, String url, String publishedAt, int type) {
        this.id = id;
        this.title = title;
        this.img = img;
        this.video = video;
        this.srcName = srcName;
        this.srcImage=srcImage;
        this.description=description;
        this.content=content;
        this.url=url;
        this.publishedAt=publishedAt;
        this.type=type;
    }

    public Article(String id, String title, String img, String video,String srcName, String srcImage, String description, String content, String url, String publishedAt, int type, int categoryId) {
        this.id = id;
        this.title = title;
        this.img = img;
        this.video = video;
        this.srcName = srcName;
        this.srcImage=srcImage;
        this.description=description;
        this.content=content;
        this.url=url;
        this.publishedAt=publishedAt;
        this.type=type;
        this.categoryId = categoryId;
    }

    /* Advertisement item */
    public Article(int id, int categoryId, String title, String mediaUrl, String mediaType, String date, String adUrl) {
        this.adId = id;
        this.categoryId = categoryId;
        this.title = title;
        this.img = mediaUrl;
        this.mediaType = mediaType;
        this.date = date;
        this.url = adUrl;
    }

    // use for suggest topic
    public Article(String id, int type, String title, String url, ArrayList<Topic> topics) {
        this.id = id;
        this.type = type;
        this.title = title;
        this.srcName = srcName;
        this.url = url;
        this.topics = topics;
    }

    // use for advertise
    public Article(String id, String title, String img, String url, int type) {
        this.id = id;
        this.title = title;
        this.img = img;
        this.srcName=srcName;
        this.url=url;
        this.type=type;
    }

    public Article() {

    }

    public ArrayList<Article> getAdArticles() {
        return adArticles;
    }

    public void setAdArticles(ArrayList<Article> adArticles) {
        this.adArticles = adArticles;
    }

    public void setAdId(int id) {
        this.adId = id;
    }

    public int getAdId() {
        return adId;
    }

    public void setAdTitle(String title) {
        this.title = title;
    }

    public String getAdTitle() {
        return title;
    }

    public void setMediaUrl(String mediaUrl) {
        this.img = mediaUrl;
    }

    public String getMediaUrl() {
        return img;
    }

    public void setAdUrl(String adUrl) {
        this.url = adUrl;
    }

    public String getAdUrl() {
        return url;
    }

    public void setAdMediaType(int mediaType) {
        this.type = mediaType;
    }

    public Article(int type) {
        this.type = type;
    }

    public String getId() {
        return id;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    public String getTitle() {
        return title;
    }

    public String getImg() {
        return img;
    }

    public String getVideo() {
        return video;
    }

    public int getType() {
        return type;
    }

    public ArrayList<Topic> getTopics() {
        return topics;
    }

    public String getSrcName() {
        return srcName;
    }

    public String getSrcImage() {
        return srcImage;
    }

    public String getDescription() {
        return description;
    }

    public String getUrl() {
        return url;
    }

    public String getContent() {
        return content;
    }

    public String getPublishedAt() {
        return publishedAt;
    }

    public void setType(int type) {
        this.type = type;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public void setVideo(String video) {
        this.video = video;
    }

    public void setSrcName(String srcName) {
        this.srcName = srcName;
    }

    public void setSrcImage(String srcImage) {
        this.srcImage = srcImage;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public void setPublishedAt(String publishedAt) {
        this.publishedAt = publishedAt;
    }

    public void setTopics(ArrayList<Topic> topics) {
        this.topics = topics;
    }

    @Override
    public String toString() {
        return srcName;
    }
}
