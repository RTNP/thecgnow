package com.newsapp.app.thecgnow2.roomDB.category;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

@Dao
public interface CategoryDao {

    @Insert
    public void addCategory(CategoryRoom category);

    @Query("select * from categories")
    public List<CategoryRoom> getCategories();

    @Query("SELECT DISTINCT * FROM categories WHERE category_id =:id")
    public CategoryRoom getCategoryWithCatId(int id);

    @Query("SELECT DISTINCT category_id FROM categories WHERE category_key_name =:catName")
    public int getCatIdWithName(String catName);

    @Query("select * from categories order by category_order")
    public List<CategoryRoom> getCategoriesInOrder();

    @Query("DELETE FROM categories WHERE category_id =:id")
    public void removeCategory(int id);
}
